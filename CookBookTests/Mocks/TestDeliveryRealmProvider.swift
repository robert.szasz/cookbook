//
//  TestDeliveryRealmProvider.swift
//  CookBookTests
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift
@testable import CookBook

struct TestDeliveryRealmProvider: RealmProvider {
    
    let configuration: Realm.Configuration
    var realm: Realm {
        return try! Realm(configuration: configuration)
    }
    
    private init(config: Realm.Configuration) {
        configuration = config
    }
    
    // MARK: - FOOD Realm in the memory
    private static func deliveryItemsOnDiskConfig() -> Realm.Configuration {
        return Realm.Configuration(inMemoryIdentifier: "deliveryRealm", schemaVersion: 0, deleteRealmIfMigrationNeeded: true, objectTypes: [Delivery.self])
    }
    
    static var itemsOnDisk: RealmProvider = {
        return TestDeliveryRealmProvider(config: TestDeliveryRealmProvider.deliveryItemsOnDiskConfig())
    }()
}
