//
//  UILabel-Extension.swift
//  CookBook
//
//  Created by Robert Szasz on 22/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit

extension UILabel {
    
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.text!.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: self.font!], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.text!.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: self.font!], context: nil)

        return ceil(boundingBox.width)
    }
}
