//
//  FoodPickerFilter.swift
//  CookBook
//
//  Created by Robert Szasz on 20/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import Then
import RxSwift
import RxCocoa

class FoodPickerFilterView: UIView {
    let viewModel: FoodListViewModel
    let charArray = ["🍜", "🥬", "🥩", "🍰", "A"]
    let numberOfCharacters = 5
    let buttonSize: CGFloat
    let horizontalSpacing: CGFloat
    let verticalSpacing: CGFloat
    
    private var stateArray: [Int] = [5]
    private var centralFilter = PublishSubject<[Int]>()
    private var bag = DisposeBag()
    
    // MARK: - View properties
    
    lazy var soupButton = makeCustomButton(text: charArray[0], color: #colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 1), buttonSize: buttonSize, withTag: 1)
    lazy var saladButton = makeCustomButton(text: charArray[1], color: #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1), buttonSize: buttonSize, withTag: 2)
    lazy var mainDishButton = makeCustomButton(text: charArray[2], color: #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), buttonSize: buttonSize, withTag: 3)
    lazy var dessertButton = makeCustomButton(text: charArray[3], color: #colorLiteral(red: 1, green: 0.1764705882, blue: 0.3333333333, alpha: 1), buttonSize: buttonSize, withTag: 4)
    lazy var allButton = makeCustomButton(text: charArray[4], color: #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5764705882, alpha: 1), buttonSize: buttonSize, withTag: 5).then {
        $0.setTitleColor(.black, for: .normal)
        $0.isSelected = true
        $0.layer.backgroundColor = UIColor(cgColor: $0.layer.backgroundColor!).withAlphaComponent(1).cgColor
    }
    

    // MARK: - Init
    private init(vm: FoodListViewModel, buttonSize size: CGFloat = 50, horizontalSpacing: CGFloat = 5, verticalSpacing: CGFloat = 10) {
        self.viewModel = vm
        self.buttonSize = size
        self.horizontalSpacing = horizontalSpacing
        self.verticalSpacing = verticalSpacing
        
        let frameWidth = (CGFloat(numberOfCharacters) - 1) * (buttonSize + horizontalSpacing) + (buttonSize + 2 * verticalSpacing)
        let frameHeight = buttonSize + 2 * verticalSpacing
        
        super.init(frame: CGRect(x: 0, y: 0, width: frameWidth, height: frameHeight))
        self.layer.cornerRadius = ( buttonSize + 2 * verticalSpacing ) / 2
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 5
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        
        setUpViews()
    }
    
    convenience init(viewModel: FoodListViewModel, buttonSize size: CGFloat, horizontalSpacing: CGFloat, verticalSpacing: CGFloat) {
        self.init(vm: viewModel, buttonSize: size, horizontalSpacing: horizontalSpacing, verticalSpacing: verticalSpacing)
    }
    
    convenience init(viewModel: FoodListViewModel) { self.init(vm: viewModel) }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Set up view

    private func setUpViews() {
        centralFilter.asObservable()
            .subscribe(onNext: { [weak self] intArr in
                self?.stateArray = intArr
            })
            .disposed(by: bag)
        
        centralFilter.asObservable()
            .bind(to: viewModel.filterInput)
            .disposed(by: bag)
        
        let buttonArray = [soupButton, saladButton, mainDishButton, dessertButton, allButton]
        buttonArray.enumerated().forEach { (index, button) in
            addButton(button, toPlace: CGFloat(index) * (buttonSize + horizontalSpacing) + verticalSpacing)
        }
        
    }
    
    // MARK: - Helper methods
    
    private func addButton(_ button: UIButton, toPlace horizontalSpace: CGFloat) {
        
        self.addSubview(button)
        button.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: verticalSpacing, paddingLeft: horizontalSpace, paddingBottom: 0, paddingRight: 0, width: buttonSize, height: buttonSize)
                
        centralFilter.asObservable()
            .subscribe(onNext: { [weak self] intArr in
                for number in 1...5 {
                    guard let btn = self?.viewWithTag(number) as? UIButton else { continue }
                    
                    if intArr.contains(number) {
                        if !btn.isSelected {
                            btn.isSelected.toggle()
                            UIView.animate(withDuration: 0.5) {
                                btn.layer.backgroundColor = UIColor(cgColor: btn.layer.backgroundColor!).withAlphaComponent(1).cgColor
                            }
                        }
                    } else if btn.isSelected {
                        btn.isSelected.toggle()
                        UIView.animate(withDuration: 0.5) {
                            btn.layer.backgroundColor = UIColor(cgColor: btn.layer.backgroundColor!).withAlphaComponent(0).cgColor
                        }
                    }
                }
            })
            .disposed(by: bag)
        
        button.rx.tap
            .filter { 1 ... 4 ~= button.tag  }
            .subscribe { [weak self] _ in
                
                if button.isSelected {
                    guard let stateArr = self?.stateArray, stateArr.contains(button.tag), stateArr.count > 1 else { return }
                    
                    let newState = stateArr.filter { $0 != (button.tag) }
                    self?.centralFilter.onNext(newState)
                } else {
                    guard let stateArr = self?.stateArray, !stateArr.contains(button.tag), !button.isSelected, let allButton = self?.viewWithTag(5) as? UIButton else { return }
                    
                    if allButton.isSelected {
                        self?.centralFilter.onNext([button.tag])
                    } else {
                        var newState = stateArr
                        newState.append(button.tag)
                        newState.sort()
                        self?.centralFilter.onNext(newState)
                    }
                }
                
            }.disposed(by: bag)
        
        button.rx.tap
            .filter { button.tag == 5 }
            .subscribe(onNext: { [weak self] _ in
                
                guard let allButton = self?.viewWithTag(5) as? UIButton, !allButton.isSelected else { return }
                
                let newState = [5]
                self?.centralFilter.onNext(newState)
            })
            .disposed(by: bag)
    }
    
    private func makeCustomButton(text: String, color: UIColor, buttonSize: CGFloat, withTag tag: Int) -> UIButton {
        return UIButton(type: .custom).then {
            $0.isSelected = false
            $0.setTitle(text, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: 30)
            $0.layer.backgroundColor = color.withAlphaComponent(0).cgColor
            $0.layer.cornerRadius = buttonSize / 2
            $0.clipsToBounds = true
            $0.tag = tag
        }
    }
    
}
