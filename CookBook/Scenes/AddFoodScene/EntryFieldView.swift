//
//  EntryFieldView.swift
//  CookBook
//
//  Created by Robert Szasz on 25/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import Then

class EntryFieldView: UIView {

    private let name: String
    
    // MARK: - View properties
    
    lazy var descriptor: UILabel = UILabel().then {
        $0.text = name
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .heavy)
    }
    
    lazy var textfield: UITextField = UITextField().then {
        $0.placeholder = name
        $0.borderStyle = .roundedRect
    }
    
    lazy var textview: UITextView = UITextView().then {
        $0.layer.cornerRadius = 3
        $0.layer.masksToBounds = true
        $0.layer.borderColor = UIColor.lightGray.cgColor
        $0.layer.borderWidth = 1
        $0.font = .systemFont(ofSize: 18)
    }
    
    
    // MARK: - Init
    
    convenience init(entryTextviewName: String) {
        self.init(frame: .zero, name: entryTextviewName)
        setUpView(isTextView: true)
    }
    
    convenience init(entryfieldName: String) {
        self.init(frame: .zero, name: entryfieldName)
        setUpView(isTextView: false)
    }
    
    init(frame: CGRect, name: String) {
        self.name = name
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Set up view
    
    private func setUpView(isTextView: Bool) {
        
        addSubview(descriptor)
        descriptor.anchor(top: topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: descriptor.frame.width, height: descriptor.frame.height)
        
        if !isTextView {
            addSubview(textfield)
            textfield.anchor(top: descriptor.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: -10, paddingRight: -10, width: 0, height: 0)
            descriptor.leftAnchor.constraint(equalTo: textfield.leftAnchor, constant: 5).isActive = true
        }
        
        if isTextView {
            addSubview(textview)
            textview.anchor(top: descriptor.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: -10, paddingRight: -10, width: 0, height: 0)
            descriptor.leftAnchor.constraint(equalTo: textview.leftAnchor, constant: 5).isActive = true
        }
        
    }
    

}
