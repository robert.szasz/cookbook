//
//  AddFoodViewModel.swift
//  CookBook
//
//  Created by Robert Szasz on 21/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxCocoa
import Action


struct AddFoodViewModel {
    let sceneCoordinator: SceneCoordinatorType
    let foodService: FoodServiceType
    let foodToEdit: FoodItem?
    
    init(coordinator: SceneCoordinatorType, service: FoodServiceType, foodToEdit: FoodItem?) {
        self.sceneCoordinator = coordinator
        self.foodService = service
        self.foodToEdit = foodToEdit

        if let foodToEdit = self.foodToEdit { configureSelf(withFood: foodToEdit) }
    }
    
    private var ffpl: FullFoodPropertyList {
        FullFoodPropertyList(uid: (foodToEdit != nil) ? foodToEdit!.uid : nil,
                             name: name.value,
                             overview: overview.value,
                             recipe: recipe.value,
                             prepTime: prepTime.value,
                             ingredients: ingredients.value,
                             photo: photo.value,
                             isVegetarian: restrictions.value.contains("isVegetarian"),
                             isVegan: restrictions.value.contains("isVegan"),
                             isLactosefree: restrictions.value.contains("isLactosefree"),
                             isGlutenfree: restrictions.value.contains("isGlutenfree"),
                             isSugarfree: restrictions.value.contains("isSugarfree"),
                             foodKind: foodKind.value,
                             difficulty: difficulty.value)
    }
    
    // MARK: - Input & Output
        
    let photo: BehaviorRelay<String> = BehaviorRelay<String>(value: "")

    let name: BehaviorRelay<String> = BehaviorRelay<String>(value: "")
    
    let overview: BehaviorRelay<String> = BehaviorRelay<String>(value: "")

    let foodKind: BehaviorRelay<FoodKind> = BehaviorRelay<FoodKind>(value: .MainDish)

    let recipe: BehaviorRelay<String> = BehaviorRelay<String>(value: "")
    
    let ingredients: BehaviorRelay<[String]> = BehaviorRelay<[String]>(value: [])
    
    let prepTime: BehaviorRelay<String> = BehaviorRelay<String>(value: "")
    
    let difficulty: BehaviorRelay<DifficultyLevel> = BehaviorRelay<DifficultyLevel>(value: .MiddleLow)
    
    let restrictions: BehaviorRelay<[String]> = BehaviorRelay<[String]>(value : [])
    
    func onSaveTask() -> CocoaAction {
        return CocoaAction { _ in
            return self.foodService
                .createFood(properties: self.ffpl)
                .flatMap { _ -> Observable<Void> in
                    return self.sceneCoordinator
                        .pop(animated: true)
                        .asObservable()
                        .map { _ in }
                }
        }
    }
    
    func onEditTask() -> CocoaAction {
        return CocoaAction { _ in
            return self.foodService
                .update(food: self.foodToEdit!, properties: self.ffpl)
                .flatMap { _ -> Observable<Void> in
                    return self.sceneCoordinator
                        .pop(animated: true)
                        .asObservable()
                        .map { _ in }
                }
        }
    }

    // MARK: - Helper methods
    
    private func configureSelf(withFood foodToEdit: FoodItem) {
        photo.accept(foodToEdit.pictureURL)
        name.accept(foodToEdit.name)
        overview.accept(foodToEdit.overview)
        foodKind.accept(foodToEdit.enumFoodKind)
        recipe.accept(foodToEdit.recipe)
        ingredients.accept(foodToEdit.ingredients.toArray())
        prepTime.accept(foodToEdit.prepTime)
        difficulty.accept(foodToEdit.enumDifficultyLevel)
        restrictions.accept(foodToEdit.getFoodRestrictionArray())
    }
    
}

