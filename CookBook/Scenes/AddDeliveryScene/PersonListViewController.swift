//
//  PersonListViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 30/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import Then

class PersonListViewController: UIViewController {
    
    private let namesArray: [String]
    
    let bag = DisposeBag()
    let selectedName = PublishSubject<String>()
    
    // MARK: - Lifecycle
    
    init(names: [String]) {
        self.namesArray = names
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for (index, name) in namesArray.enumerated() {
            let nameLabel = makeNameCell(withName: name)
            
            view.addSubview(nameLabel)
            nameLabel.anchor(top: index == 0 ? view.topAnchor : view.subviews[index - 1].bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: index == 0 ? 20 : 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 200, height: 30)
        }
    }

    // MARK: - Helper methods
    
    private func makeNameCell(withName name: String) -> UIView {
        return UILabel().then { label in
            label.text = "  " + name
            label.textColor = .darkGray
            label.font = .systemFont(ofSize: 20, weight: .bold)
            label.isUserInteractionEnabled = true
            
            let tapGesture = UITapGestureRecognizer()
            tapGesture.numberOfTapsRequired = 1
            label.addGestureRecognizer(tapGesture)
            tapGesture.rx.event
                .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
                .subscribe({ [weak self] _ in
                    self?.selectedName.onNext(label.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                    self?.dismiss(animated: true, completion: nil)
                })
                .disposed(by: bag)
        }
    }
}
