//
//  DifficultySelectorView.swift
//  CookBook
//
//  Created by Robert Szasz on 25/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import Then
import RxSwift
import RxCocoa
import Action


class DifficultySelectorView: UIView {

    public var selectedButton: BehaviorRelay<Int>
    
    private var colorArray: [UIColor]
    
    // MARK: - Init
    
    convenience init(initialValue: Int, colorArray: [UIColor] = [#colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1), #colorLiteral(red: 0.1803921569, green: 0.8, blue: 0.4431372549, alpha: 1), #colorLiteral(red: 0.9450980392, green: 0.768627451, blue: 0.05882352941, alpha: 1), #colorLiteral(red: 0.9529411765, green: 0.6117647059, blue: 0.07058823529, alpha: 1), #colorLiteral(red: 0.8274509804, green: 0.3294117647, blue: 0, alpha: 1)]) {
        self.init(frame: .zero, value: initialValue, array: colorArray)
        setUpView()
        
        selectedButton.asObservable()
            .take(2)
            .subscribe { [weak self] in self?.setUpView() }
    }
    
    init(frame: CGRect, value: Int, array: [UIColor]) {
        self.selectedButton = BehaviorRelay(value: value)
        self.colorArray = array
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup view
    
    private func setUpView() {
        subviews.forEach { $0.removeFromSuperview() }
        var btnArray = [UIButton]()
        
        for (index, color) in colorArray.enumerated() {
            let tag = index + 1
            var btn = makeButton(ofColor: color, tag)
            
            btn.rx.action = CocoaAction { [weak self] _ in
                let object = PublishSubject<Void>()
                guard let self = self else { object.onCompleted(); return object.asObservable()}
                
                for viewTag in 1 ... self.colorArray.count {
                    if viewTag <= btn.tag {
                        let strongButton = self.viewWithTag(viewTag) as! UIButton
                        strongButton.alpha = 1
                    } else {
                        let weakButton = self.viewWithTag(viewTag) as! UIButton
                        weakButton.alpha = 0.1
                    }
                }
                self.selectedButton.accept(btn.tag)
                object.onCompleted()
                
                return object.asObservable()
            }
            
            btnArray.append(btn)
            
        }
        
        makeStack(withArray: btnArray)
        
    }
    
    // MARK: - Helper methods
    
    private func makeButton(ofColor color: UIColor, _ number: Int) -> UIButton {
        UIButton(type: .custom).then {
            $0.backgroundColor = color
            $0.alpha = selectedButton.value >= number ? 1 : 0.1
            $0.tag = number
            $0.layer.cornerRadius = 3
            $0.layer.masksToBounds = true
            $0.layer.borderColor = UIColor.white.cgColor
            $0.layer.borderWidth = 3
        }
    }
    
    private func makeStack(withArray array: [UIButton]) {
        let stack = UIStackView(arrangedSubviews: array)
        stack.alignment = .center
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        addSubview(stack)
        stack.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
}
