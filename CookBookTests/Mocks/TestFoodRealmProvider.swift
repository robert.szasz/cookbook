//
//  TestFoodRealmProvider.swift
//  CookBookTests
//
//  Created by Robert Szasz on 19/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift
@testable import CookBook

struct TestFoodRealmProvider: RealmProvider {
    
    let configuration: Realm.Configuration
    var realm: Realm {
        return try! Realm(configuration: configuration)
    }
    
    private init(config: Realm.Configuration) {
        configuration = config
    }
    
    // MARK: - FOOD Realm in the memory
    private static func foodItemsOnDiskConfig() -> Realm.Configuration {
        return Realm.Configuration(inMemoryIdentifier: "foodRealm", schemaVersion: 0, deleteRealmIfMigrationNeeded: true, objectTypes: [FoodItem.self])
    }
    
    static var itemsOnDisk: RealmProvider = {
        return TestFoodRealmProvider(config: TestFoodRealmProvider.foodItemsOnDiskConfig())
    }()
}
