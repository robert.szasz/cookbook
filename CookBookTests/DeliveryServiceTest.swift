//
//  DeliveryServiceTest.swift
//  CookBookTests
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm
import RxTest
import RxBlocking

@testable import CookBook

class DeliveryServiceTest: XCTestCase {
    var deliveryService: DeliveryService!
    var deliveryPropertyList: DeliveryPropertyList!
    var disposeBag: DisposeBag!
    
    private let deliveryRealm: Realm = TestDeliveryRealmProvider.itemsOnDisk.realm
    
    private func createTestDeliveries(_ number: Int) throws -> Maybe<[Delivery]> {
        var deliveries = [Delivery]()
        for i in 1...number {
            let properties: DeliveryPropertyList = (uid: (0 - i), date: Date(), toPerson: "TestPerson\(i)", soup: nil, salad: nil, mainDish: nil, dessert: nil)
            let delivery = Delivery(properties: properties)
            
            try deliveryRealm.write {
                deliveryRealm.add(delivery)
                deliveries.append(delivery)
            }
        }
        return Maybe.just(deliveries)
    }
    
    override func setUp() {
        super.setUp()
        
        deliveryService = DeliveryService(TestDeliveryRealmProvider.self)
        deliveryPropertyList = (uid: -1, date: Date(), toPerson: "TestPerson", soup: nil, salad: nil, mainDish: nil, dessert: nil)
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        do {
            try deliveryRealm.write {
                deliveryRealm.deleteAll()
            }
        } catch {
            fatalError( "Could not delete test objects from delivery realm")
        }
        
        super.tearDown()
    }
    
    // MARK: - Tests
    func testDeliveryIsAddedToRealm() throws {
        let result = deliveryService.createDelivery(properties: deliveryPropertyList)
        XCTAssertEqual(try result.toBlocking().first(), deliveryRealm.object(ofType: Delivery.self, forPrimaryKey: -1))
    }
    
    func testDeleteDelivery() throws {
        let _ = try createTestDeliveries(1).subscribe { maybe in
            switch maybe {
                case .success(let deliveries):
                    self.deliveryService.delete(delivery: deliveries.first!)
                        .subscribe(onNext: {
                            XCTAssertNil(self.deliveryRealm.objects(Delivery.self))
                        })
                        .disposed(by: self.disposeBag)
                default: XCTFail("Maybe<[Delivery]> should always contain at least one element in testing.")
            }
        }
    }
    
    func testDeliveriesFromRealm() throws {
        let _ = try createTestDeliveries(3).subscribe { maybe in
            switch maybe {
            case .success(let deliveries):
                let result = self.deliveryService.deliveries()
                XCTAssertNotNil(try? result.toBlocking(timeout: 1).first())
                XCTAssertEqual(try? result.toBlocking(timeout: 1).first()?.toArray().count, 3)
                XCTAssertEqual(try? result.toBlocking(timeout: 1).first()?.toArray(), deliveries)
                
            default: XCTFail("Maybe<[Delivery]> should always contain at least one element in testing.")
            }
        }
    }
}
