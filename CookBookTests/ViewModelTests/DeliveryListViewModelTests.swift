//
//  DeliveryListViewModelTests.swift
//  CookBookTests
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm
import RxTest
import RxBlocking
import Then

@testable import CookBook

class DeliveryListViewModelTests: XCTestCase {
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    
    private let formatter = DateFormatter().then { $0.dateFormat = "yyyy/MM/dd HH:mm" }
    private let calendar = Calendar.current
    
    private let deliveryRealm: Realm = TestDeliveryRealmProvider.itemsOnDisk.realm

    private lazy var may11thDelivery: Delivery = { deliveryRealm.object(ofType: Delivery.self, forPrimaryKey: -1)! }()
    private lazy var may12thDelivery: Delivery = { deliveryRealm.object(ofType: Delivery.self, forPrimaryKey: -2)! }()
    private lazy var may13thDelivery: Delivery = { deliveryRealm.object(ofType: Delivery.self, forPrimaryKey: -3)! }()
    private lazy var may14thDelivery: Delivery = { deliveryRealm.object(ofType: Delivery.self, forPrimaryKey: -4)! }()
    
    private func createViewModel() -> DeliveryListViewModel {
        DeliveryListViewModel(coordinator: SceneCoordinator(window: UIWindow()), deliveryService: DeliveryService(TestDeliveryRealmProvider.self))
    }
    
    private func createTestDeliveries(_ number: Int) throws -> Maybe<[Delivery]> {
        var deliveries = [Delivery]()
        let startDate = formatter.date(from: "2020/05/11 11:00")
        
        for i in 1...number {
            let properties: DeliveryPropertyList = (uid: (0 - i),
                                                    date: calendar.date(byAdding: .day, value: i - 1, to: startDate!)!,
                                                    toPerson: "TestPerson\(i % 2 == 0 ? 2 : 1)",
                                                    soup: nil,
                                                    salad: nil,
                                                    mainDish: nil,
                                                    dessert: nil)
            
            let delivery = Delivery(properties: properties)
            
            try deliveryRealm.write {
                deliveryRealm.add(delivery)
                deliveries.append(delivery)
            }
        }
        return Maybe.just(deliveries)
    }
    
    override func setUp() {
        super.setUp()
        
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        do {
            try deliveryRealm.write {
                deliveryRealm.deleteAll()
            }
        } catch {
            fatalError( "Could not delete test objects from delivery realm" )
        }
        
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func test_whenInitiated_storesInitParams() {
        let viewModel = createViewModel()
        
        XCTAssertNotNil(viewModel.deliveryService)
        XCTAssertNotNil(viewModel.sceneCoordinator)
    }
    
    func test_sectionedItemsEmit_whenInitialized() throws {
        let viewModel = createViewModel()
        let result = viewModel.sectionedItems
        
        XCTAssertNotNil(try? result.toBlocking(timeout: 1.0).first())
    }
    
    func test_sectionedItemsEmit_whenChanged() throws {
        let viewModel = createViewModel()
        let result = viewModel.sectionedItems
        
        XCTAssertNotNil(try? result.toBlocking(timeout: 1.0).first())
        
        let _ = try createTestDeliveries(4).subscribe { maybe in
            switch maybe {
            case .success(let deliveries):
                XCTAssertEqual(try? result.take(1).toBlocking().last()?.items.count, deliveries.count)
            default: XCTFail( "Was not able to add test object deliveries to realm." )
            }
        }
    }
    
    func test_sectionedItemsEmit_whenFilteringApplied() throws {
        let may11th = formatter.date(from: "2020/05/11 10:00")!
        let may12th = formatter.date(from: "2020/05/12 10:00")!
        //let may13th = formatter.date(from: "2020/05/13 10:00")!
        let may14th = formatter.date(from: "2020/05/14 10:00")!
        
        let viewModel = createViewModel()
        
        let _ = try createTestDeliveries(4)
        
        let filteredResults = scheduler.createObserver(DeliverySection.self)
        
        viewModel.sectionedItems
            .bind(to: filteredResults)
            .disposed(by: disposeBag)
        
        scheduler.createColdObservable([.next(10, ["TestPerson1"]),
                                        .next(20, ["TestPerson1", "TestPerson2"]),
                                        .next(30, ["TestPerson2"]),
                                        .next(40, [])])
            .bind(to: viewModel.personFilter)
            .disposed(by: disposeBag)
        
        scheduler.createColdObservable([.next(15, DateInterval(start: may12th, end: may14th)),
                                        .next(25, DateInterval(start: may11th, end: may12th)),
                                        .next(35, nil)])
            .bind(to: viewModel.calendarFilter)
            .disposed(by: disposeBag)
        
        scheduler.start()
        
        XCTAssertEqual(filteredResults.events, [.next(0, DeliverySection(model: "Deliveries", items: [may14thDelivery,
                                                                                                      may13thDelivery,
                                                                                                      may12thDelivery,
                                                                                                      may11thDelivery])),
                                                .next(10, DeliverySection(model: "Deliveries to: TestPerson1", items: [may13thDelivery,
                                                                                                                       may11thDelivery])),
                                                .next(15, DeliverySection(model: "Deliveries to: TestPerson1", items: [may13thDelivery])),
                                                .next(20, DeliverySection(model: "Deliveries to: TestPerson1, TestPerson2", items: [may13thDelivery,
                                                                                                                                    may12thDelivery])),
                                                .next(25, DeliverySection(model: "Deliveries to: TestPerson1, TestPerson2", items: [may11thDelivery])),
                                                .next(30, DeliverySection(model: "Deliveries to: TestPerson2", items: [])),
                                                .next(35, DeliverySection(model: "Deliveries to: TestPerson2", items: [may14thDelivery, may12thDelivery])),
                                                .next(40, DeliverySection(model: "Deliveries", items: [may14thDelivery,
                                                                                                       may13thDelivery,
                                                                                                       may12thDelivery,
                                                                                                       may11thDelivery]))])
    }
}
