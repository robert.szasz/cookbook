//
//  UIPresentationController-Rx.swift
//  CookBook
//
//  Created by Robert Szasz on 28/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension UIPresentationController: HasDelegate {
    public typealias Delegate = UIAdaptivePresentationControllerDelegate
}

class RxUIAdaptivePresentationControllerDelegateProxy: DelegateProxy<UIPresentationController, UIAdaptivePresentationControllerDelegate>, DelegateProxyType, UIAdaptivePresentationControllerDelegate {
    
    public weak private(set) var presentationController: UIPresentationController?
    
    public init(presentationController: ParentObject) {
        self.presentationController = presentationController
        super.init(parentObject: presentationController, delegateProxy: RxUIAdaptivePresentationControllerDelegateProxy.self)
    }
    
    static func registerKnownImplementations() {
        self.register { RxUIAdaptivePresentationControllerDelegateProxy(presentationController: $0) }
    }
}

@available(iOS 13.0, *)
extension Reactive where Base: UIPresentationController {
    
    public var delegate: DelegateProxy<UIPresentationController, UIAdaptivePresentationControllerDelegate> {
        return RxUIAdaptivePresentationControllerDelegateProxy.proxy(for: base)
    }
    
    public var didDismissController: ControlEvent<UIPresentationController> {
        let source = delegate
            .methodInvoked(#selector(UIAdaptivePresentationControllerDelegate.presentationControllerDidDismiss(_:)))
            .map { a in
                return a[0] as! UIPresentationController
            }
        return ControlEvent(events: source)
    }
    
}
