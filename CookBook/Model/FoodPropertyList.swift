//
//  FoodPropertyList.swift
//  CookBook
//
//  Created by Robert Szasz on 23/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation

typealias FoodPropertyList = (uid: Int?, name: String, overview: String, recipe: String, prepTime: String, ingredients: [String], foodKind: FoodKind, difficulty: DifficultyLevel)
typealias FullFoodPropertyList = (uid: Int?, name: String, overview: String, recipe: String, prepTime: String, ingredients: [String], photo: String, isVegetarian: Bool, isVegan: Bool, isLactosefree: Bool, isGlutenfree: Bool, isSugarfree: Bool, foodKind: FoodKind, difficulty: DifficultyLevel)


// Interoperability methods
func fullFoodPropertyList(from fpl: FoodPropertyList) -> FullFoodPropertyList {
    (uid: fpl.uid, name: fpl.name, overview: fpl.overview, recipe: fpl.recipe, prepTime: fpl.prepTime, ingredients: fpl.ingredients, photo: "", isVegetarian: false, isVegan: false, isLactosefree: false, isGlutenfree: false, isSugarfree: false, foodKind: fpl.foodKind, difficulty: fpl.difficulty)
}

func foodPropertyList(from ffpl: FullFoodPropertyList) -> FoodPropertyList {
    (uid: ffpl.uid, name: ffpl.name, overview: ffpl.overview, recipe: ffpl.recipe, prepTime: ffpl.prepTime, ingredients: ffpl.ingredients, foodKind: ffpl.foodKind, difficulty: ffpl.difficulty)
}
