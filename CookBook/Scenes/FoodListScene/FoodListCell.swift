//
//  FoodListCell.swift
//  CookBook
//
//  Created by Robert Szasz on 20/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import Action
import RxSwift
import Then

class FoodListCell: UITableViewCell {
    
    private var cellHeight: CGFloat = 250
    private var bag = DisposeBag()
    
    // MARK: - View properties
    
    let photoView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
        $0.layer.borderWidth = 1
        $0.layer.borderColor = UIColor.lightGray.cgColor
        $0.image = UIImage(named: "salad")
    }
    
    lazy var textContainer: UIView = {
        let container = UIView()
        
        let label = UILabel()
        label.backgroundColor = .white
        label.numberOfLines = 0
        label.attributedText = getAttributedTitle(title: NSLocalizedString("Food title", comment: "Food List Cell food title"), description: NSLocalizedString("Description", comment: "Food List Cell food description"))
        container.addSubview(label)
        label.anchor(top: container.topAnchor, left: container.leftAnchor, bottom: container.bottomAnchor, right: container.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: -50, paddingRight: 0, width: 0, height: 0)
        
        let iv1 = UIImageView(image: UIImage()).then {
            $0.contentMode = .scaleAspectFit
        }
        let iv2 = UIImageView(image: UIImage()).then {
            $0.contentMode = .scaleAspectFit
        }
        let iv3 = UIImageView(image: UIImage()).then {
            $0.contentMode = .scaleAspectFit
        }
        let iv4 = UIImageView(image: UIImage()).then {
            $0.contentMode = .scaleAspectFit
        }
        let iv5 = UIImageView(image: UIImage()).then {
            $0.contentMode = .scaleAspectFit
        }
        let ivArray = [iv1, iv2, iv3, iv4, iv5]
        
        ivArray.enumerated().forEach { (index, iv) in
            addImageView(iv, toPlace: CGFloat(index * (30 + 5)) + 10)
        }
        
        func addImageView(_ iv: UIImageView, toPlace xCoorinate: CGFloat) {
            container.addSubview(iv)
            iv.anchor(top: label.bottomAnchor, left: container.leftAnchor, bottom: nil, right: nil, paddingTop: 15, paddingLeft: xCoorinate, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        }
        
        return container
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(photoView)
        contentView.addSubview(textContainer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: cellHeight)
        
        photoView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: nil, paddingTop: 20, paddingLeft: 20, paddingBottom: -20, paddingRight: 0, width: 150, height: 0)
        textContainer.anchor(top: photoView.topAnchor, left: photoView.rightAnchor, bottom: photoView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: -20, width: 0, height: 0)
        textContainer.layer.cornerRadius = 10
        textContainer.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
    }
    
    // MARK: - Helper methods
    
    func configure(with item: FoodItem) {
        
        item.rx.observe(Int.self, "realmFoodKind")
            .filter { $0 != nil }
            .map { $0! }
            .subscribe(onNext: { [weak self] foodKind in
                switch foodKind {
                case 1: self?.photoView.image = UIImage(named: "soup")
                case 2: self?.photoView.image = UIImage(named: "salad")
                case 3: self?.photoView.image = UIImage(named: "mainDish")
                case 4: self?.photoView.image = UIImage(named: "dessert")
                default: fatalError("Invalid food kind integer")
                }
            }).disposed(by: bag)
        
        let name = item.rx.observe(String.self, "name")
            .filter { $0 != nil }
            .map { $0! }
        
        let overview = item.rx.observe(String.self, "overview")
            .filter { $0 != nil }
            .map { $0! }
        
        Observable.combineLatest(name, overview) { [weak self] in self?.getAttributedTitle(title: $0, description: $1) }
            .filter { $0 != nil }
            .map { $0! }
            .subscribe(onNext: { [weak self] attrStr in
                guard let label = self?.textContainer.subviews.first as? UILabel else { return }
                label.attributedText = attrStr
            })
            .disposed(by: bag)
        
        let photo = item.rx.observe(String.self, "pictureURL")
            .filter { $0! != "" }
            .map { $0! }
            .map { URL(string: $0)!.lastPathComponent }
            .map { imageFromFileName($0) }
            .bind(to: photoView.rx.image)
            .disposed(by: bag)
        
        let isVegan = item.rx.observe(Bool.self, "isVegan")
        let isVegetarian = item.rx.observe(Bool.self, "isVegetarian")
        let isLactosefree = item.rx.observe(Bool.self, "isLactosefree")
        let isGlutenfree = item.rx.observe(Bool.self, "isGlutenfree")
        let isSugarfree = item.rx.observe(Bool.self, "isSugarfree")
        
        Observable.combineLatest(isVegan, isVegetarian, isLactosefree, isGlutenfree, isSugarfree) { (vegan, vegetarian, lactosefree, glutenfree, sugarfree) -> [String] in
            let restrictionArr = [vegan, vegetarian, lactosefree, glutenfree, sugarfree].enumerated()
                .map { (index, element) -> String? in
                    if element! {
                        switch index {
                        case 0: return "isVegan"
                        case 1: return "isVegetarian"
                        case 2: return "isLactosefree"
                        case 3: return "isGlutenfree"
                        case 4: return "isSugarfree"
                        default: fatalError("Index out of range!")
                        }
                    } else {
                        return nil
                    }
                }.compactMap { $0 }
                return restrictionArr
            }
            .subscribe(onNext: { [weak self] stringArr in
                guard let self = self else { return }
                
                for subview in self.textContainer.subviews {
                    guard let imageView = subview as? UIImageView else { continue }
                    imageView.image = nil
                }
                
                stringArr.enumerated().forEach { (index, element) in
                    guard let imageView = self.textContainer.subviews[index + 1] as? UIImageView else { return }
                    imageView.image = UIImage(named: element)
                }
            })
            .disposed(by: bag)
    }
    
    // To empty the dispose bag between cell reuses
    override func prepareForReuse() {
        bag = DisposeBag()
    }
    
    // MARK: - Helper methods
    private func getAttributedTitle(title: String, description: String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: "\(title)\n", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 24)])
        attributedText.append(NSAttributedString(string: description, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.lightGray]))
        return attributedText
    }
}
