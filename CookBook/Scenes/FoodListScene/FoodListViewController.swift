//
//  FoodListViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Action
import RxRealm
import Then

fileprivate let cellIdentifier = "FoodCell"

class FoodListViewController: UIViewController, BindableType {
    
    var viewModel: FoodListViewModel!
    var dataSource: RxTableViewSectionedAnimatedDataSource<FoodSection>!
    let bag = DisposeBag()
    
    // MARK: - View Properties
    
    let searchBar = UISearchBar().then {
        $0.isTranslucent = false
        $0.placeholder = NSLocalizedString("Search for food...", comment: "Search bar in Food List")
        $0.barTintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.backgroundImage = UIImage()
        $0.showsCancelButton = true
        
        var searchTextField = $0.value(forKey: "searchField") as! UITextField
        searchTextField.backgroundColor = UIColor.init(white: 1, alpha: 0.5)
        
        let attributes:[NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.white,
            .font: UIFont.systemFont(ofSize: 17)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    }
    
    let tableView = UITableView().then {
        $0.backgroundColor = .white
        $0.separatorStyle = .none
        $0.register(FoodListCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    lazy var filterView = FoodPickerFilterView(viewModel: viewModel)

    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setUpNavBar()
        
        addSubviews()
        addLayout()
        
        configureDataSource()
        
        tableView.rx.setDelegate(self).disposed(by: bag)
    }
    
    // MARK: - Subviews & Layout
    
    private func addSubviews() {
        view.addSubview(searchBar)
        view.addSubview(filterView)
        view.addSubview(tableView)
    }
    
    private func addLayout() {
        searchBar.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 60)
        
        filterView.anchor(top: searchBar.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: filterView.frame.width, height: filterView.frame.height)
        filterView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        tableView.anchor(top: filterView.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 250
    }
    
    // MARK: - Bind ViewModel
    
    func bindViewModel() {
        viewModel.sectionedItems
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        searchBar.rx.searchButtonClicked
            .map { [weak self] _ in self?.searchBar.text }
            .filter { $0 != nil}
            .map { str -> String in str! }
            .filter { !$0.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty }
            .bind(to: viewModel.searchBarInput)
            .disposed(by: bag)
        
        searchBar.rx.searchButtonClicked
            .subscribe(onNext: { [weak self] _ in
                self?.searchBar.resignFirstResponder()
            })
            .disposed(by: bag)
        
        searchBar.rx.textDidEndEditing
            .map { [weak self] _ -> String in (self?.searchBar.text)! }
            .filter { $0 == "" }
            .bind(to: viewModel.searchBarInput)
            .disposed(by: bag)
        
        searchBar.rx.cancelButtonClicked
            .do(onNext: { [weak self] _ in
                self?.searchBar.resignFirstResponder()
                self?.searchBar.text = ""
            })
            .map { _ -> String in "" }
            .bind(to: viewModel.searchBarInput)
            .disposed(by: bag)
        
        tableView.rx.itemSelected
            .do(onNext: { [unowned self] indexPath in
                self.tableView.deselectRow(at: indexPath, animated: true)
            })
            .map { [unowned self] indexPath in
                try! self.dataSource.model(at: indexPath) as! FoodItem
            }
            .bind(to: viewModel.foodSelected.inputs)
            .disposed(by: bag)
        
        tableView.rx.itemDeleted
            .map { [unowned self] indexPath in
                try! self.dataSource.model(at: indexPath) as! FoodItem
            }
            .subscribe(onNext: { [weak self] in self?.viewModel.onDeleteAction().execute($0) })
            .disposed(by: bag)
        
        navigationItem.rightBarButtonItem?.rx.action = viewModel.onCreateFood()
    }
    
    // MARK: - Helper methods
    
    private func configureDataSource() {
        dataSource = RxTableViewSectionedAnimatedDataSource<FoodSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .automatic, reloadAnimation: .automatic, deleteAnimation: .left), configureCell: { dataSource, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FoodListCell
            cell.configure(with: item)
            return cell
        }, titleForHeaderInSection: { dataSource, index in
            dataSource.sectionModels[index].model
        }, canEditRowAtIndexPath: { _, _ in
            return true
        })
    }
    
    private func setUpNavBar() {
        navigationItem.title = NSLocalizedString("Food search", comment: "Food List controller navbar title")
        navigationController?.navigationBar.prefersLargeTitles = true
        
        if #available(iOS 13.0, *) {
            let navigationBar = navigationController?.navigationBar
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
            appearance.shadowColor = nil
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.titleTextAttributes = [.foregroundColor : UIColor.white]
            let btnAppearance = UIBarButtonItemAppearance(style: .plain)
            btnAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.white]
            appearance.buttonAppearance = btnAppearance
            UINavigationBar.appearance().tintColor = .white
            
            navigationBar?.standardAppearance = appearance
            navigationBar?.scrollEdgeAppearance = appearance
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus.app.fill"), style: .plain, target: nil, action: nil)
            return
        }
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
}

// MARK: - Table View Delegate

extension FoodListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
}
