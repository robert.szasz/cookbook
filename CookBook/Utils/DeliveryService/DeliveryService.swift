//
//  DeliveryService.swift
//  CookBook
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//


import Foundation
import RealmSwift
import RxSwift
import RxRealm


struct DeliveryService: DeliveryServiceType {
    private var realmProviderType: RealmProvider.Type
    
    init(_ realmProvider: RealmProvider.Type = DeliveryRealmProvider.self) {
        self.realmProviderType = realmProvider
    }
    
    private func withDeliveryRealm<T>(_ operation: String, action: (Realm) throws -> T) -> T? {
      do {
        let realm = realmProviderType.itemsOnDisk.realm
        return try action(realm)
      } catch let err {
        print("Failed \(operation) realm with error: \(err)")
        return nil
      }
    }
    
    /**
        This function creates, saves to Realm and returns an observable delivery object with the given properties.
     
        The `createDelivery` function takes a `DeliveryPropertyList` argument, which the delivery object is constructed from. The object gets automatically saved to the delivery `Realm` instance. At creation, the newly created object gets assigned an automatically increment unique integer id (to be uniquely identifiable). The `id`s of the objects are positive integer values, negative integer `id`s are used by testing objects.
     
        - Parameter properties: A `DeliveryPropertyList`, which is a **typealias** for a specific `tuple`
        - Returns: A `Observable<Delivery>` if the action succeeds or a `DeliveryServiceError.creationFailed` otherwise.
        - Warning: This function writes the generated object to a Realm specified by the `RealmProvider` of the `struct`
        - Note: This function has `@discardableResult`
     */
    @discardableResult
    func createDelivery(properties: DeliveryPropertyList) -> Observable<Delivery> {
        let result = withDeliveryRealm("creating") { realm -> Observable<Delivery> in
            let delivery = Delivery(properties: properties)
            try realm.write {
                if properties.uid == nil {delivery.uid = (realm.objects(Delivery.self).max(ofProperty: "uid") ?? 0) + 1 }
                realm.add(delivery)
            }
            return .just(delivery)
        }
        return result ?? .error(DeliveryServiceError.creationFailed)
    }
    
    /**
        This function deletes the specified object from the Realm and returns `Void` observable.
     
        The `delete` function takes a `Delivery` argument, which defines the to be deleted object. The object gets deleted from the `Realm`.
     
        - Parameter properties: A `Delivery` object
        - Returns: A `Observable<Void>` if the action succeeds or a `DeliveryServiceError.deletionFailed(delivery)` with the associated object otherwise.
        - Warning: This function writes the generated object to a Realm specified by the `RealmProvider` of the `struct`
        - Note: This function has `@discardableResult`
        - Todo: Change return type to Completable (or Maybe), which fits the purpose more
     */
    @discardableResult
    func delete(delivery: Delivery) -> Observable<Void> {
        let result = withDeliveryRealm("deleting") { realm -> Observable<Void> in
            try realm.write {
                realm.delete(delivery)
            }
            return .empty()
        }
        return result ?? .error(DeliveryServiceError.deletionFailed(delivery))
    }

    /**
        This function fetches the delivery objects from the `Realm`.
     
        The `deliveries` doesn not take an argument and returns the whole content of the delivery `Realm`.
     
        - Returns: A `Observable<Results<Delivery>>` - a observable of the Realm `Results` wrapper with the deliveries - if the action succeeds or a `empty` Observable otherwise.
        - Warning: This function fetches the objects from a Realm specified by the `RealmProvider` of the `struct`
     */
    func deliveries() -> Observable<Results<Delivery>> {
        let result = withDeliveryRealm("getting deliveries") { realm -> Observable<Results<Delivery>> in
            let deliveries = realm.objects(Delivery.self)
            return .collection(from: deliveries)
        }
        return result ?? .empty()
    }
}


struct DeliveryRealmProvider: RealmProvider {
    
    private static let libraryDir = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first ?? ""
    let configuration: Realm.Configuration
    var realm: Realm {
        return try! Realm(configuration: configuration)
    }
    
    private init(config: Realm.Configuration) {
        configuration = config
    }
    
    // MARK: - DELIVERY Realm on the disk
    private static func deliveryItemsOnDiskConfig() -> Realm.Configuration {
        let libraryUrl = URL(fileURLWithPath: libraryDir)
        let libraryUrlWithRealm = libraryUrl.appendingPathComponent("deliveries.realm")
        
        return Realm.Configuration(fileURL: libraryUrlWithRealm, schemaVersion: 0, deleteRealmIfMigrationNeeded: true, objectTypes: [Delivery.self])
    }
    
    static var itemsOnDisk: RealmProvider = {
        return DeliveryRealmProvider(config: DeliveryRealmProvider.deliveryItemsOnDiskConfig())
    }()
}
