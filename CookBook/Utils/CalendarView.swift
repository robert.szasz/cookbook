//
//  CalendarView.swift
//  CookBook
//
//  Created by Robert Szasz on 01/06/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Then
import Action


class CalendarView: UIView {
    public let choosenDay = PublishSubject<Date>()
    public let choosenInterval = PublishSubject<DateInterval>()
    
    private let singleDayPick: Bool
    private let days = [NSLocalizedString("Su", comment: "Sunday"),
                        NSLocalizedString("Mo", comment: "Monday"),
                        NSLocalizedString("Tu", comment: "Tuesday"),
                        NSLocalizedString("We", comment: "Wednesday"),
                        NSLocalizedString("Th", comment: "Thursday"),
                        NSLocalizedString("Fr", comment: "Friday"),
                        NSLocalizedString("Sa", comment: "Saturday")]
    private var currentMonth = BehaviorRelay<Date>(value: Date())
    private lazy var currentMonthDays = BehaviorRelay<Int>(value: calendar.range(of: .day, in: .month, for: Date())!.count)
    private let startDayAndEndOfPreviousMonth: BehaviorRelay<(startDay: Int, endOfPreviousMonth: Int)>
    private let startDateInterval = BehaviorRelay<Date?>(value: nil)
    private let startDateIntervalBuffer = BehaviorRelay<Date?>(value: nil)
    private let endDateInterval = BehaviorRelay<Date?>(value: nil)
    private let choosenDayBuffer = BehaviorRelay<Date>(value: Date())
    private let dateFormatter = DateFormatter().then { $0.dateFormat = "dd/MM/yyyy" }
    private let monthDateFormatter = DateFormatter().then { $0.dateFormat = "MMM yyyy" }
    private let calendar = Calendar.current
    private let bag = DisposeBag()
    private var dateBag = DisposeBag()
    
    private enum MonthDirection {
        case Previous, Next, Current
    }
    
    // MARK: - View properties
    
    lazy var monthLabel = UILabel().then {
        $0.text = monthDateFormatter.string(from: currentMonth.value)
        $0.font = .systemFont(ofSize: 22, weight: .bold)
    }
    
    var previousButton = UIButton(type: .custom).then {
        $0.setImage(UIImage(named: "previous_arrow")!.withRenderingMode(.alwaysTemplate), for: .normal)
        $0.tintColor = .darkGray
        $0.imageView?.contentMode = .scaleAspectFit
    }
    
    private lazy var monthAction = Action<MonthDirection, Void> { [weak self] direction in
        return Observable<Void>.create { [weak self] observer in
            guard let self = self else { observer.onCompleted(); return Disposables.create() }
            
            guard direction != .Current else {
                self.currentMonth.accept(Date())
                observer.onCompleted()
                return Disposables.create()
            }
            
            guard let month = self.calendar.date(byAdding: .month, value: direction == .Next ? 1 : -1, to: self.currentMonth.value) else {
                observer.onCompleted()
                return Disposables.create()
            }
            
            self.currentMonth.accept(month)
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    var currentButton = UIButton(type: .custom).then {
        $0.backgroundColor = .darkGray
    }
    
    var nextButton = UIButton(type: .custom).then {
        $0.setImage(UIImage(named: "next_arrow")!.withRenderingMode(.alwaysTemplate), for: .normal)
        $0.tintColor = .darkGray
        $0.imageView?.contentMode = .scaleAspectFit
    }
    
    // MARK: - Init
    
    public convenience init(singleDayPick: Bool = true) {
        self.init(frame: .zero, singleDayPick: singleDayPick)
        
        translatesAutoresizingMaskIntoConstraints = false
        singleDayPick ? choosenInterval.onCompleted() : choosenDay.onCompleted()
        
        self.startDayAndEndOfPreviousMonth.accept(findStartDay())
        
        makeDaysStack()
        
        for row in 1...6 { makeHorizontalStack(forRow: row) }
                
        setUpLayout()
        
        previousButton.rx.bind(to: monthAction, input: .Previous)
        currentButton.rx.bind(to: monthAction, input: .Current)
        nextButton.rx.bind(to: monthAction, input: .Next)
        
        currentMonth.asObservable()
            .map { [unowned self] in self.monthDateFormatter.string(from: $0) }
            .bind(to: monthLabel.rx.text)
            .disposed(by: bag)
        
        currentMonth.asObservable()
            .map { [unowned self] in self.calendar.range(of: .day, in: .month, for: $0)!.count }
            .bind(to: currentMonthDays)
            .disposed(by: bag)
        
        currentMonth.asObservable()
            .map { [unowned self] in self.findStartDay(forDate: $0) }
            .bind(to: startDayAndEndOfPreviousMonth)
            .disposed(by: bag)

        startDayAndEndOfPreviousMonth.asObservable()
            .skip(1)
            .do(onNext: { [unowned self] _ in self.updateStack() })
            .subscribe()
            .disposed(by: bag)
        
        if singleDayPick {
            choosenDay.asObservable()
                .bind(to: choosenDayBuffer)
                .disposed(by: bag)
            
            currentMonth.asObservable()
                .do(onNext: { [unowned self] _ in
                    let labels = self.getIntervalOfAllBlackLabels()
                    labels.forEach { label in self.changeLabelToUnselected(label) }
                })
                .do(onNext: { [unowned self] in
                    let currentMonthDateComponents = self.calendar.dateComponents([.year, .month], from: $0)
                    let dayDateComponents = self.calendar.dateComponents([.year, .month, .day], from: self.choosenDayBuffer.value)
                    if let year = currentMonthDateComponents.year, let month = currentMonthDateComponents.month, let dayYear = dayDateComponents.year, let dayMonth = dayDateComponents.month, year == dayYear, month == dayMonth {
                        let label = self.searchLabelForDay(dayDateComponents.day!)
                        self.changeLabelToSelected(label)
                    }
                })
                .subscribe()
                .disposed(by: bag)
        }
        
        if !singleDayPick {
            
            startDateIntervalBuffer.asObservable()
                .do(onNext: { [unowned self] _ in
                    let range = self.getIntervalOfAllBlackLabels()
                    range.forEach { label in self.changeLabelToUnselected(label) }
                })
                .do(onNext: { [unowned self] _ in
                    self.endDateInterval.accept(nil)
                })
                .bind(to: startDateInterval)
                .disposed(by: bag)
            
            startDateInterval.asObservable()
                .filter { [unowned self] in
                    guard let validDate = $0, let _ = self.calendar.dateComponents([.day], from: validDate).day else { return false }
                    return true
                }
                .do(onNext: { [unowned self] in
                    let dayInt = self.calendar.dateComponents([.day], from: $0!).day!
                    let label = self.searchLabelForDay(dayInt)
                    self.changeLabelToSelected(label)
                })
                .subscribe()
                .disposed(by: bag)
            
            endDateInterval.asObservable()
                .filter { [unowned self] in
                    guard let validDate = $0, let _ = self.calendar.dateComponents([.day], from: validDate).day else { return false }
                    return true
                }
                .do(onNext: { [unowned self] in
                    let dayInt = self.calendar.dateComponents([.day], from: $0!).day!
                    let label = self.searchLabelForDay(dayInt)
                    self.changeLabelToSelected(label)
                })
                .do(onNext: { [unowned self] _ in
                    let labels = self.getIntervalRangeOfLabels(starting: false)
                    labels.forEach { label in self.changeLabelToSelected(label) }
                })
                .subscribe()
                .disposed(by: bag)
            
            currentMonth.asObservable()
                .filter { [unowned self] _ in self.startDateInterval.value != nil }
                .do(onNext: { [unowned self] in
                    let currentMonthDateComponents = self.calendar.dateComponents([.year, .month], from: $0)
                    let startDateComponents = self.calendar.dateComponents([.year, .month, .day], from: self.startDateInterval.value!)
                    if let year = currentMonthDateComponents.year, let month = currentMonthDateComponents.month, let startYear = startDateComponents.year, let startMonth = startDateComponents.month, year == startYear, month == startMonth {
                        let label = self.searchLabelForDay(startDateComponents.day!)
                        self.changeLabelToSelected(label)
                        let labels = self.getIntervalRangeOfLabels(starting: true)
                        labels.forEach { label in self.changeLabelToSelected(label) }
                    }
                })
                .filter { [unowned self] _ in self.endDateInterval.value != nil }
                .do(onNext: { [unowned self] in
                    let currentMonthDateComponents = self.calendar.dateComponents([.year, .month], from: $0)
                    let endDateComponents = self.calendar.dateComponents([.year, .month], from: self.endDateInterval.value!)
                    if let year = currentMonthDateComponents.year, let month = currentMonthDateComponents.month, let endYear = endDateComponents.year, let endMonth = endDateComponents.month, year == endYear, month == endMonth {
                        let labels = self.getIntervalRangeOfLabels(starting: false)
                        labels.forEach { label in self.changeLabelToSelected(label) }
                    }
                })
                .filter { [unowned self] _ in self.calendar.dateComponents([.month], from: self.startDateInterval.value!).month! + 1 < self.calendar.dateComponents([ .month], from: self.endDateInterval.value!).month! }
                .do(onNext: { [unowned self] in
                    let currentMonthDateComponents = self.calendar.dateComponents([.year, .month], from: $0)
                    let startDateComponents = self.calendar.dateComponents([.year, .month], from: self.startDateInterval.value!)
                    let endDateComponents = self.calendar.dateComponents([.year, .month], from: self.endDateInterval.value!)
                    if let year = currentMonthDateComponents.year, let month = currentMonthDateComponents.month, let startYear = startDateComponents.year, let startMonth = startDateComponents.month, let endYear = endDateComponents.year, let endMonth = endDateComponents.month, startYear...endYear ~= year, (startMonth + 1)...(endMonth - 1) ~= month {
                        self.colorMonth(until: self.currentMonthDays.value)
                    }
                })
                .subscribe()
                .disposed(by: bag)
            
            endDateInterval.asObservable()
                .filter { $0 != nil }
                .map { [unowned self] in DateInterval(start: self.startDateInterval.value!, end: $0!) }
                .bind(to: choosenInterval)
                .disposed(by: bag)
        }
        
    }
    
    private init(frame: CGRect, singleDayPick: Bool) {
        self.singleDayPick = singleDayPick
        self.startDayAndEndOfPreviousMonth = BehaviorRelay(value: (0, 0))
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Set Up View
    
    private func setUpLayout() {
        addSubview(monthLabel)
        monthLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 30, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addSubview(nextButton)
        nextButton.anchor(top: nil, left: nil, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: -8, width: 30, height: 30)
        nextButton.centerYAnchor.constraint(equalTo: monthLabel.centerYAnchor).isActive = true
        
        addSubview(currentButton)
        currentButton.anchor(top: nil, left: nil, bottom: nil, right: nextButton.leftAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: -8, width: 20, height: 20)
        currentButton.centerYAnchor.constraint(equalTo: nextButton.centerYAnchor).isActive = true
        currentButton.roundCorners()
        
        addSubview(previousButton)
        previousButton.anchor(top: nil, left: nil, bottom: nil, right: currentButton.leftAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: -8, width: 30, height: 30)
        previousButton.centerYAnchor.constraint(equalTo: currentButton.centerYAnchor).isActive = true
        
        guard let daysStack = viewWithTag(10) as? UIStackView else { fatalError() }
        
        daysStack.arrangedSubviews.first!.layoutIfNeeded()
        daysStack.anchor(top: monthLabel.bottomAnchor, left: monthLabel.leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: -5, width: 0, height: ceil((daysStack.arrangedSubviews.first?.frame.height)!) + 10)
        
        let separatorView = UIView().then { $0.backgroundColor = .lightGray; $0.tag = 11 }
        addSubview(separatorView)
        separatorView.anchor(top: daysStack.bottomAnchor, left: daysStack.leftAnchor, bottom: nil, right: daysStack.rightAnchor, paddingTop: -8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 1)
        
        for tag in 1...6 {
            guard let stack = viewWithTag(tag) as? UIStackView else { continue }
            
            stack.arrangedSubviews.first!.layoutIfNeeded()
            stack.anchor(top: tag == 1 ? separatorView.bottomAnchor : viewWithTag(tag - 1)?.bottomAnchor, left: daysStack.leftAnchor, bottom: nil, right: daysStack.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: ceil((stack.arrangedSubviews.first?.frame.height)!) + 10)
        }
    }
    
    
    
    // MARK: - Helper methods
    
    private func findStartDay(forDate date: Date = Date()) -> (Int, Int) {
        let components = calendar.dateComponents([.year, .month], from: date)
        guard let startOfMonth = calendar.date(from: components) else { return (0, 0) }
        
        guard let firstDayOfMonth = calendar.dateComponents([.weekday], from: startOfMonth).weekday, firstDayOfMonth != 1 else { return (1, 1) }
        
        let daysOfPreviousMonth = Array(Array<Int>(calendar.range(of: .day, in: .month, for: calendar.date(byAdding: .day, value: -1, to: startOfMonth)!)!).reversed())
        return (daysOfPreviousMonth[firstDayOfMonth - 2], daysOfPreviousMonth[0])
    }
    
    private func makeDaysStack() {
        let stack = UIStackView()
        for day in days {
            let dayView = UILabel().then {
                $0.text = day
                $0.textAlignment = .center
                $0.font = .systemFont(ofSize: 18, weight: .bold)
                $0.textColor = .darkGray
                $0.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
            }
            stack.addArrangedSubview(dayView)
        }
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .equalSpacing
        stack.tag = 10

        addSubview(stack)
    }
    
    private func makeHorizontalStack(forRow row: Int) {
        switch row {
        case 1: makeStackForFirstRow()
        case 2...4: makeStackForMiddleRows(row)
        case 5...6: makeStackForLastRows(row)
        default: fatalError("There should be always 6 horizontal stacks!")
        }
    }
    
    private func makeStackForFirstRow() {
        let oldMonthArray = Array(startDayAndEndOfPreviousMonth.value.startDay ... startDayAndEndOfPreviousMonth.value.endOfPreviousMonth)
        let newMonthArray = Array(Array(1...7).prefix(7 - oldMonthArray.count))
        let array = oldMonthArray + newMonthArray
        
        let stack = UIStackView()
        
        for day in array {
            let dayView = UILabel().then { label in
                label.text = String(day)
                label.textAlignment = .center
                label.isUserInteractionEnabled = true
                label.layer.cornerRadius = 3
                label.layer.masksToBounds = true
                label.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
                stack.addArrangedSubview(label)
                if day > 8 { label.textColor = .lightGray }
                else { singleDayPick ? chooseDaySubscription(label) : chooseInterval(label) }
            }
        }
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .equalCentering
        stack.tag = 1

        addSubview(stack)
    }
    
    private func makeStackForMiddleRows(_ row: Int) {
        let startingDay = startDayAndEndOfPreviousMonth.value.startDay + 7 - startDayAndEndOfPreviousMonth.value.endOfPreviousMonth + 7 * (row - 2)
        let endingDay = startingDay + 7
        let array = Array(startingDay..<endingDay)
        
        let stack = UIStackView()
        for day in array {
            let dayView = UILabel().then { label in
                label.text = String(day)
                label.textAlignment = .center
                label.isUserInteractionEnabled = true
                label.layer.cornerRadius = 3
                label.layer.masksToBounds = true
                label.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
                stack.addArrangedSubview(label)
                singleDayPick ? chooseDaySubscription(label) : chooseInterval(label)
            }
        }
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .equalCentering
        stack.tag = row

        addSubview(stack)
        
    }
    
    private func makeStackForLastRows(_ row: Int) {
        let startingDay = startDayAndEndOfPreviousMonth.value.startDay + 7 - startDayAndEndOfPreviousMonth.value.endOfPreviousMonth + 7 * (row - 2)
        let endingDay = startingDay + 7
        let smallCurrentArray = Array(startingDay..<endingDay).filter { $0 <= currentMonthDays.value }
        let smallNewArray = Array(Array(1...7).prefix(7 - smallCurrentArray.count))
        var array = smallCurrentArray + smallNewArray
        if startingDay > currentMonthDays.value { array = Array((startingDay - currentMonthDays.value)..<(startingDay - currentMonthDays.value + 7)) }
        
        let stack = UIStackView()
        for day in array {
            let dayView = UILabel().then { label in
                label.text = String(day)
                label.textAlignment = .center
                label.isUserInteractionEnabled = true
                label.layer.cornerRadius = 3
                label.layer.masksToBounds = true
                label.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
                stack.addArrangedSubview(label)
                if day < 20 { label.textColor = .lightGray }
                else { singleDayPick ? chooseDaySubscription(label) : chooseInterval(label) }
            }
        }
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .equalCentering
        stack.tag = row
        
        addSubview(stack)
    }
    
    private func updateStack() {
        for tag in 1...6 { viewWithTag(tag)?.removeFromSuperview() }
        dateBag = DisposeBag()
        for row in 1...6 { makeHorizontalStack(forRow: row) }
        for tag in 1...6 {
            guard let stack = viewWithTag(tag) as? UIStackView else { continue }
            
            stack.arrangedSubviews.first!.layoutIfNeeded()
            stack.anchor(top: tag == 1 ? viewWithTag(11)!.bottomAnchor : viewWithTag(tag - 1)?.bottomAnchor, left: viewWithTag(10)!.leftAnchor, bottom: nil, right: viewWithTag(10)!.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: ceil((stack.arrangedSubviews.first?.frame.height)!) + 10)
        }
    }
    
    private func chooseDaySubscription(_ label: UILabel) {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        label.addGestureRecognizer(tapGesture)
        
        tapGesture.rx.event
            .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .do(onNext: { [unowned self] _ in
                for tag in 1...6 {
                    let stack = self.viewWithTag(tag) as! UIStackView
                    for label in stack.arrangedSubviews {
                        if label.layer.backgroundColor == #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).cgColor, let label = label as? UILabel {
                            label.layer.backgroundColor = UIColor.clear.cgColor
                            label.textColor = .black
                            label.font = .systemFont(ofSize: label.font.pointSize, weight: .regular)
                        }
                    }
                }
            })
            .do(onNext: { _ in
                label.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).cgColor
                label.textColor = .white
                label.font = .systemFont(ofSize: label.font.pointSize, weight: .bold)
            })
            .map { _ in Int(label.text!) }
            .map { [unowned self] day in
                var dateComponents: DateComponents? = self.calendar.dateComponents([.year, .month], from: self.currentMonth.value)

                dateComponents?.day = day
                dateComponents?.hour = 12
                dateComponents?.minute = 0
                dateComponents?.second = 0

                return self.calendar.date(from: dateComponents!)!
            }
            .bind(onNext: { [weak self] date in self?.choosenDay.onNext(date) })
            .disposed(by: dateBag)
    }
    
    private func chooseInterval(_ label: UILabel) {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        label.addGestureRecognizer(tapGesture)
        
        tapGesture.rx.event
            .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .map { [unowned self] _ -> Date in
                let day = Int(label.text!)!
                var dateComponents: DateComponents? = self.calendar.dateComponents([.year, .month], from: self.currentMonth.value)

                dateComponents?.day = day
                dateComponents?.hour = (self.endDateInterval.value == nil) ? 23 : 1
                dateComponents?.minute = 0
                dateComponents?.second = 0

                return self.calendar.date(from: dateComponents!)!
            }
            .filter { [unowned self] in
                if self.endDateInterval.value == nil, let startDate = self.startDateInterval.value {
                    return $0 > startDate
                }
                return true
            }
            .bind(onNext: { [weak self] date in (self?.endDateInterval.value == nil && self?.startDateInterval.value != nil) ? self?.endDateInterval.accept(date) : self?.startDateIntervalBuffer.accept(date) })
            .disposed(by: dateBag)
    }
    
    private func colorMonth(until number: Int) {
        for tag in 1...6 {
            let stack = self.viewWithTag(tag) as! UIStackView
            for label in stack.arrangedSubviews {
                let labelColor: UIColor
                if #available(iOS 13.0, *) { labelColor = UIColor.label } else { labelColor = .black }
                if let label = label as? UILabel, label.textColor == labelColor, Int(label.text!)! <= number {
                    label.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).cgColor
                    label.textColor = .white
                    label.font = .systemFont(ofSize: label.font.pointSize, weight: .bold)
                }
            }
        }
    }
    
    private func searchLabelForDay(_ day: Int) -> UILabel {
        for tag in 1...6 {
            let stack = self.viewWithTag(tag) as! UIStackView
            for label in stack.arrangedSubviews {
                let labelColor: UIColor
                if #available(iOS 13.0, *) { labelColor = UIColor.label } else { labelColor = .black }
                if let label = label as? UILabel, Int(label.text!)! == day, label.textColor == labelColor {
                    return label
                }
            }
        }
        return UILabel()
    }
    
    private func changeLabelToSelected(_ label: UILabel) {
        label.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).cgColor
        label.textColor = .white
        label.font = .systemFont(ofSize: label.font.pointSize, weight: .bold)
    }
    
    private func changeLabelToUnselected(_ label: UILabel) {
        let labelColor: UIColor
        if #available(iOS 13.0, *) { labelColor = UIColor.label } else { labelColor = .black }
        label.layer.backgroundColor = UIColor.clear.cgColor
        label.textColor = labelColor
        label.font = .systemFont(ofSize: label.font.pointSize, weight: .regular)
    }
    
    private func getIntervalOfAllBlackLabels() -> [UILabel] {
        var range = [UILabel]()
        
        let dateIntRange = 1...currentMonthDays.value
                
        for tag in 1...6 {
            let stack = self.viewWithTag(tag) as! UIStackView
            for label in stack.arrangedSubviews {
                if let label = label as? UILabel, dateIntRange ~= Int(label.text!)!, label.textColor != .lightGray {
                    range.append(label)
                }
            }
        }
        return range
    }
    
    private func getIntervalRangeOfLabels(starting: Bool) -> [UILabel] {
        var range = [UILabel]()
        guard let startDate = startDateInterval.value, let startDateInt = calendar.dateComponents([.day], from: startDate).day, let startDateMonth = calendar.dateComponents([.month], from: startDate).month, let startDateDaysNumber = calendar.range(of: .day, in: .month, for: startDate)?.count, let endDate = endDateInterval.value, let endDateInt = calendar.dateComponents([.day], from: endDate).day, let endDateMonth = calendar.dateComponents([.month], from: endDate).month else { return range }
        
        let dateIntRange: ClosedRange<Int>
        switch endDateMonth - startDateMonth {
        case 0: dateIntRange = startDateInt...endDateInt
        default: dateIntRange = starting ? startDateInt...startDateDaysNumber : 1...endDateInt
        }
                
        for tag in 1...6 {
            let stack = self.viewWithTag(tag) as! UIStackView
            for label in stack.arrangedSubviews {
                if let label = label as? UILabel, dateIntRange ~= Int(label.text!)!, label.textColor != .lightGray {
                    range.append(label)
                }
            }
        }
        return range
    }
}

class CalendarViewController: UIViewController {
    let singleDayPick: Bool
    lazy var calendar: CalendarView = CalendarView(singleDayPick: singleDayPick)
    
    init(singleDayPick: Bool) {
        self.singleDayPick = singleDayPick
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(calendar)
        calendar.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
    }
}
