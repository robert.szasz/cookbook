//
//  SceneCoordinator.swift
//  CookBook
//
//  Created by Robert Szasz on 17/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class SceneCoordinator: SceneCoordinatorType {
    
    var window: UIWindow
    var currentViewController: UIViewController!
    var viewControllerStack: [UIViewController] = []
    
    required init(window: UIWindow) {
        self.window = window
    }
    
    static func actualViewController(for viewController: UIViewController) -> UIViewController {
        if let navigationController = viewController as? UINavigationController {
            return navigationController.viewControllers.first!
        } else {
            return viewController
        }
    }

    @discardableResult
    func transition(to scene: Scene, type: SceneTransitionType) -> Maybe<UINavigationController> {
        let subject = PublishSubject<UINavigationController>()
        let viewController = scene.viewController()
        switch type {
        case .root:
            guard viewControllerStack.isEmpty else { fatalError("Can't be another view controller on stack before root view controller.") }
            currentViewController = SceneCoordinator.actualViewController(for: viewController)
            viewControllerStack.append(currentViewController)
            return Maybe<UINavigationController>.create { maybe in
                maybe(.success(viewController as! UINavigationController))
                return Disposables.create {}
            }

        case .push:
            guard let navigationController = currentViewController.navigationController else { fatalError("Can't push a view controller without a current navigation controller") }
            // one-off subscription to be notified when push complete
            _ = navigationController.rx.delegate
                .sentMessage(#selector(UINavigationControllerDelegate.navigationController(_:didShow:animated:)))
                .do(onNext: { _ in
                    subject.onCompleted()
                })
            navigationController.pushViewController(viewController, animated: true)
            currentViewController = viewController
            viewControllerStack.append(currentViewController)
            let vc = viewController as! ConfigurableType
            vc.configureSelfAfterPushedOnNavStack()

        case .modal:
            currentViewController.present(UINavigationController(rootViewController: viewController), animated: true) {
                subject.onCompleted()
            }
            currentViewController = viewController
            let vc = currentViewController as! ConfigurableType
            vc.configureSelfAfterPushedOnNavStack()
            viewControllerStack.append(currentViewController)
        }
        return subject.asMaybe()
    }

    @discardableResult
    func pop(animated: Bool) -> Completable {
        let subject = PublishSubject<Void>()
        if let _ = currentViewController.presentingViewController, viewControllerStack[viewControllerStack.count - 2].presentingViewController == nil {
            // dismiss a modal controller
            currentViewController.navigationController!.dismiss(animated: animated) { [unowned self] in
                let _ = self.viewControllerStack.popLast()
                self.currentViewController = self.viewControllerStack.last!
                subject.onCompleted()
            }
        } else if let navigationController = currentViewController.navigationController {
            // navigate up the stack
            // one-off subscription to be notified when pop complete
            _ = navigationController.rx.delegate
                .sentMessage(#selector(UINavigationControllerDelegate.navigationController(_:didShow:animated:)))
                .map { _ in }
                .bind(to: subject)
            guard navigationController.popViewController(animated: animated) != nil else { fatalError("can't navigate back from \(currentViewController!)") }
            let _ = viewControllerStack.popLast()
            currentViewController = viewControllerStack.last!
        } else {
            fatalError("Not a modal, no navigation controller: can't navigate back from \(currentViewController!)")
        }
        return subject.asObservable()
            .take(1)
            .ignoreElements()
    }
    
    @discardableResult
    func swipeDownDismiss(_ modalVC: UIViewController) -> Completable {
        guard modalVC == currentViewController, modalVC == viewControllerStack.last! else { fatalError() }
        let subject = PublishSubject<Void>()
        
        let _ = viewControllerStack.popLast()
        currentViewController = viewControllerStack.last!
        subject.onCompleted()
        
        return subject.asObservable()
            .take(1)
            .ignoreElements()
    }
}
