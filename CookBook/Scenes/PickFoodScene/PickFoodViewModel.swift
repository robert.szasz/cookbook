//
//  PickFoodViewModel.swift
//  CookBook
//
//  Created by Robert Szasz on 30/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action

struct PickFoodViewModel {
    let sceneCoordinator: SceneCoordinatorType
    let foodService: FoodServiceType
    let foodType: FoodKind
    
    let bag = DisposeBag()
    
    init(coordinator: SceneCoordinatorType, foodService: FoodServiceType, foodType: FoodKind) {
        self.sceneCoordinator = coordinator
        self.foodService = foodService
        self.foodType = foodType
    }
    
    // MARK: - Inputs
    
    let textFilter = BehaviorRelay<String>(value: "")
    
    // MARK: - Outputs
    
    let pickedFood = PublishSubject<FoodItem>()
    
    var sectionedItems: Observable<FoodSection> {
        return Observable.combineLatest(self.foodService.foodsOfType(foodType), textFilter.asObservable()) { (foods, textFilter) -> FoodSection in
                
                if textFilter.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                    return FoodSection(model: getNameForFoodKind(self.foodType), items: foods
                        .sorted(byKeyPath: "added", ascending: false)
                        .toArray())
                }
                
                let filteredFoods = foods
                    .filter(NSPredicate(format: "name CONTAINS[c] %@", textFilter))
                    .sorted(byKeyPath: "added", ascending: false)

                return FoodSection(model: getNameForFoodKind(self.foodType), items: filteredFoods.toArray())
        }
    }
    
    func onDismissSelf() -> CocoaAction {
        CocoaAction {
            return self.sceneCoordinator
                .pop(animated: true)
                .asObservable()
                .map { _ in }
        }
    }
}
