//
//  FoodServiceType.swift
//  CookBook
//
//  Created by Robert Szasz on 16/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

enum FoodServiceError: Error {
  case creationFailed
  case updateFailed(FoodItem)
  case deletionFailed(FoodItem)
}

protocol FoodServiceType {
    @discardableResult
    func createFood(properties: FoodPropertyList) -> Observable<FoodItem>
    
    @discardableResult
    func createFood(properties: FullFoodPropertyList) -> Observable<FoodItem>

    @discardableResult
    func delete(food: FoodItem) -> Observable<Void>

    @discardableResult
    func update(food: FoodItem, properties: FoodPropertyList) -> Observable<FoodItem>
    
    @discardableResult
    func update(food: FoodItem, properties: FullFoodPropertyList) -> Observable<FoodItem>

    func foods() -> Observable<Results<FoodItem>>
    
    func foodsOfType(_ type: FoodKind) -> Observable<Results<FoodItem>>
}
