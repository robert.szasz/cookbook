//
//  FoodItem.swift
//  CookBook
//
//  Created by Robert Szasz on 16/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift
import RxDataSources

final class FoodItem: Object {
    // Basic properties
    @objc dynamic var uid: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic var recipe: String = ""
    @objc dynamic var prepTime: String = ""
    
    // Food restriction categories
    @objc dynamic var isVegetarian: Bool = false
    @objc dynamic var isVegan: Bool = false
    @objc dynamic var isLactosefree: Bool = false
    @objc dynamic var isGlutenfree: Bool = false
    @objc dynamic var isSugarfree: Bool = false
    
    let ingredients = List<String>()
    @objc dynamic var pictureURL: String = ""
    @objc dynamic var added: Date = Date()
    
    // Enum properties
    @objc private dynamic var realmFoodKind: Int = FoodKind.MainDish.rawValue
    var enumFoodKind: FoodKind {
        get { return FoodKind(rawValue: realmFoodKind)! }
        set { realmFoodKind = newValue.rawValue }
    }
    
    @objc private dynamic var realmDifficultyLevel: Int = DifficultyLevel.Middle.rawValue
    var enumDifficultyLevel: DifficultyLevel {
        get { return DifficultyLevel(rawValue: realmDifficultyLevel)! }
        set { realmDifficultyLevel = newValue.rawValue }
    }
    
    // Overridden implementations
    override class func primaryKey() -> String? { "uid" }
    
    // Init
    convenience init(properties: FoodPropertyList) {
        self.init()
        if let uid = properties.uid { self.uid = uid }
        self.name = properties.name
        self.recipe = properties.recipe
        self.overview = properties.overview
        self.prepTime = properties.prepTime
        self.ingredients.append(objectsIn: properties.ingredients)
        self.enumFoodKind = properties.foodKind
        self.realmFoodKind = enumFoodKind.rawValue
        self.enumDifficultyLevel = properties.difficulty
        self.realmDifficultyLevel = enumDifficultyLevel.rawValue
    }
    
    convenience init(properties: FullFoodPropertyList) {
        let fpl: FoodPropertyList = (uid: properties.uid, name: properties.name, overview: properties.overview, recipe: properties.recipe, prepTime: properties.prepTime, ingredients: properties.ingredients, foodKind: properties.foodKind, difficulty: properties.difficulty)
        self.init(properties: fpl)
        
        self.pictureURL = properties.photo
        self.isVegetarian = properties.isVegetarian
        self.isVegan = properties.isVegan
        self.isLactosefree = properties.isLactosefree
        self.isGlutenfree = properties.isGlutenfree
        self.isSugarfree = properties.isSugarfree
    }
    
    private enum CodingKeys: String, CodingKey {
        case uid, name, overview, recipe, prepTime, isVegetarian, isVegan, isLactosefree, isGlutenfree, isSugarfree, ingredients, pictureURL, realmFoodKind, realmDifficultyLevel
    }
    
    // Helper methods
    func getFoodRestrictionArray() -> [String] {
        var arr = [String]()
        if self.isVegetarian { arr.append("isVegetarian") }
        if self.isVegan { arr.append("isVegan") }
        if self.isLactosefree { arr.append("isLactosefree") }
        if self.isGlutenfree { arr.append("isGlutenfree") }
        if self.isSugarfree { arr.append("isSugarfree") }
        return arr
    }
}

extension FoodItem: Decodable {
    convenience init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.init()
        uid = try values.decode(Int.self, forKey: .uid)
        name = try values.decode(String.self, forKey: .name)
        overview = try values.decode(String.self, forKey: .overview)
        recipe = try values.decode(String.self, forKey: .recipe)
        prepTime = try values.decode(String.self, forKey: .prepTime)
        isVegetarian = try values.decode(Bool.self, forKey: .isVegetarian)
        isVegan = try values.decode(Bool.self, forKey: .isVegan)
        isLactosefree = try values.decode(Bool.self, forKey: .isLactosefree)
        isGlutenfree = try values.decode(Bool.self, forKey: .isGlutenfree)
        isSugarfree = try values.decode(Bool.self, forKey: .isSugarfree)
        pictureURL = try values.decode(String.self, forKey: .pictureURL)
        realmFoodKind = try values.decode(Int.self, forKey: .realmFoodKind)
        realmDifficultyLevel = try values.decode(Int.self, forKey: .realmDifficultyLevel)

        let ingredientsArray = try values.decode([String].self, forKey: .ingredients)
        ingredients.append(objectsIn: ingredientsArray)
    }
}

extension FoodItem: IdentifiableType {
    var identity: Int {
        return self.isInvalidated ? 0 : uid
    }
}


enum FoodKind: Int { case Soup = 1, Salad, MainDish, Dessert }

enum DifficultyLevel: Int { case Easy = 1, MiddleLow, Middle, MiddleHigh, High }
