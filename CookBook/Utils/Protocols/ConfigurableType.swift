//
//  ConfigurableType.swift
//  CookBook
//
//  Created by Robert Szasz on 28/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift

protocol ConfigurableType: AnyObject {
  
    // configure self after loaded onto the nav stack
    func configureSelfAfterPushedOnNavStack()
}
