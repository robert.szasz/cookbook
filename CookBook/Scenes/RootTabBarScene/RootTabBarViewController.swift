//
//  RootTabBarViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift

class RootTabBarViewController: UITabBarController {

    private var window: UIWindow
    private var disposeBag = DisposeBag()
    private var viewControllersArray = [UIViewController]()
    
    // MARK: - Init
    
    static func initRootTabBar(window: UIWindow) {
        let _ = RootTabBarViewController(window: window)
    }
    
    private init(window: UIWindow) {
        self.window = window
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setAsRootController()
        makeFoodVC()
        makeDeliveryVC()

        setViewControllers(viewControllersArray, animated: true)
        tabBar.barTintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        tabBar.tintColor = .white
    }
    
    private func setAsRootController() {
        guard window.rootViewController == nil else { fatalError("Root view controller already set") }
        
        window.rootViewController = self
        window.makeKeyAndVisible()
    }
    
    // MARK: - Making Tab VCs

    private func makeFoodVC() {
        let service = FoodService()
        let sceneCoordinator = SceneCoordinator(window: window)

        let foodViewModel = FoodListViewModel(coordinator: sceneCoordinator, foodService: service)
        let firstScene = FoodScene.foodList(foodViewModel)
        sceneCoordinator.transition(to: firstScene, type: .root)
            .subscribe { [unowned self] maybe in
                switch maybe {
                case .success(let navController):
                    navController.tabBarItem = UITabBarItem(title: NSLocalizedString("Recipes", comment: "Tab Bar Controller title"), image: UIImage(named: "search_icon"), tag: 0)
                    self.viewControllersArray.append(navController)
                default: fatalError("Should get back a nav controller")
                }
            }
            .disposed(by: disposeBag)
    }
    
    private func makeDeliveryVC() {
        let service = DeliveryService()
        let sceneCoordinator = SceneCoordinator(window: window)

        let deliveryViewModel = DeliveryListViewModel(coordinator: sceneCoordinator, deliveryService: service)
        let firstScene = DeliveryScene.deliveryList(deliveryViewModel)
        sceneCoordinator.transition(to: firstScene, type: .root)
            .subscribe { [unowned self] maybe in
                switch maybe {
                case .success(let navController):
                    navController.tabBarItem = UITabBarItem(title: NSLocalizedString("Delivery", comment: "Tab Bar Controller title"), image: UIImage(named: "list_icon"), tag: 1)
                    self.viewControllersArray.append(navController)
                default: fatalError("Should get back a nav controller")
                }
            }
            .disposed(by: disposeBag)
    }
}
