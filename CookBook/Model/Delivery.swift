//
//  Delivery.swift
//  CookBook
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift
import RxDataSources

final class Delivery: Object {
    @objc dynamic var uid: Int = 0
    @objc dynamic var date: Date = Date()
    @objc dynamic var dateAdded: Date = Date()
    @objc dynamic var toPerson: String = ""
    let menu = List<String>()
    
    override class func primaryKey() -> String? { "uid" }
    
    override class func indexedProperties() -> [String] { ["date"] }
    
    // MARK: - Init
    
    convenience init(uid: Int? = nil, date: Date, toPerson: String, soup: FoodItem?, salad: FoodItem?, mainDish: FoodItem?, dessert: FoodItem?) {
        self.init()
        
        if let uid = uid { self.uid = uid }
        self.date = date
        self.toPerson = toPerson
        
        if let soup = soup { self.menu.append("🍜 " + soup.name) }
        if let salad = salad { self.menu.append("🥬 " + salad.name) }
        if let mainDish = mainDish { self.menu.append("🥩 " + mainDish.name) }
        if let dessert = dessert { self.menu.append("🍰 " + dessert.name) }
    }
    
    convenience init(properties: DeliveryPropertyList) {
        self.init(uid: properties.uid, date: properties.date, toPerson: properties.toPerson, soup: properties.soup, salad: properties.salad, mainDish: properties.mainDish, dessert: properties.dessert)
    }
}

extension Delivery: IdentifiableType {
    var identity: Int {
        return self.isInvalidated ? 0 : uid
    }
}
