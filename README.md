Welcome on the GitLab page of my second app - **CookBook**!

![app-screenshot](CookBook_Screenshot.png "App screenshot")


[[_TOC_]]

## Overview
The CookBook is my second ever app which I made myself as a showcase for my skills/abilities in iOS Development.
It is by no means perfect or in a production state, some additional improvements are possible in the future, depending on my mood.
As far as I know, this is a bug-free semi-stable functioning version.\
The functionality was tested on the `iOS 13` OS.\
The layout was tested on an `iPhone 11 Pro Max`.

## Background
My mom has a ginormous old leather-covered black recipe book inherited from her great-grandma, which hides a lot of treasures in itself. 
To ensure, that my mom's great granddaughters can still hold this family relic in their hands, the contents of the book must digitalized, 
which is done in the **Recipes** section.
My mom also likes to share with other people, so when she cooks, she cooks a lot (and when I mean a *lot*, it is actually *a ton*), so she sends 
some to other families, which do not have the time to cook so often. The **Delivery** section of this app tries to keep track of all the food deliveries 
made to other people.

## App logic
The App should be divided into two sections:\
\
**Recipes** `Tab Bar`:
- The `root view` is the *Recipe overview* list. The user can search for a specific recipe in the search bar, the given string does not need 
to match the recipe name 100%, it filters based on whether the recipe name contains the string or not. Below the search bar is a *food-type filter*.
It filters the recipes based on their "type", so whether the recipe is a soup, a salad, main dish or dessert. The *A* means *All*, so if that is selected,
everything is shown in the list. The filter supports multiple selection, so the user can filter for example the soups & salads, or even soups & salads & dessert.
The user can add new recipes with the **+** button on the right in the navigation bar. When clicked on a recipe in the list, the detail view shows up.
On the right in the navigation bar is a button, which represents the edit mode for that given recipe. All changes are reflected accordingly, if the *Save* 
button was pressed.

**Deliveries** `Tab Bar`:
- The `root view` is the *Delivery overview* list. The user can see the deliveries with the date, person and the menu. It can be filtered based on the person
(with the `person` button) or based on a choosen time interval (with the `calendar` button). The filters can be combined. If a filter is applied, a label appears
next to it indicating the choosen conditions. The filter can be removed using the yellow `x` button next to the applied-filters label. The user can add
new recipes with the `+` button on the right in the navigation bar. With the `contact book` button a list of already saved contacts pops up, from which a person
can be choosen. the `+` button on the left displays an alert, which requests the new person's name to be typed in. Below the person row, the user can put the menu
together. Clicking on the food-type shows a little `+` button, with which a food can be choosen from a list. If the user clicks on the food-types but `does not select`
specific foods, using the `Get Random` button the system selects random foods for the selected types. With the `calendar` button at the bottom the user can 
choose a delivery date.

## Used frameworks/libraries
- Reactive Framework (core):
    - [x] `RxSwift` (~> 5)
    - [x] `RxCocoa` (~> 5)
- Supplementary reactive libraries:
    - [x] `RxDataSources` (~> 4.0)
    - [x] `Action` (~> 4.0)
- Database libraries:
    - [x] `RealmSwift`
    - [x] `RxRealm`
- For testing:
    - [x] `RxBlocking` (~> 5)
    - [x] `RxTest` (~> 5)
- Others:
    - [x] `Then` (~> 2)

## Learned stuff (at least partially)
- First experience with the `reactive` approach
- First experience with the `MVVM` architecture
- First experience with the `localization` :hu: :gb:
- First experience with `Unit Testing`
- Deepening my `Realm` understanding
- Deepening my `UIKit` & `AutoLayout` understanding
- Supporting multiple `OS` versions (`iOS 12` & `iOS 13`)
- Creating my own mini-library -> `CalendarView` with reactive outlets

& *many more*...

## Side notes
1. **Managing image saving on disk**

  The custom images for recipes choosen by the user are copied from the camera roll to the `Documents` directory in the app's sandbox.
  The image's name is then saved to the `realm` together with the recipe. On the app's start, the system compares a list from all the image names
  in `realm` with the images in the `Documents` directory and deletes the ones, which are no longer needed (= there is no longer a reference to that image
  in the `realm`). So the unnecesary images are deleted, thus freeing up memory and minimizing the app's memory footprint.

2. **Pre-bundled default JSON recipes**

  Simulating the download and installing process of the app from the App Store, the app has a built in `JSON` file in the bundle, which provides some default
  built in recipes if the recipe `realm` is empty. The app converts the `JSON` objects to objects managed by the realm, so that the user can see some starting values
  upon "installing" the app for the first time.

3. **Person list - Deliveries**

  A pop-up shows all the persons, who got at least one delivery. This persons-list is not managed by `realm`, it is a simple `[String]` in the `UserDefaults`.


## Goals for next project
- Reactive networking
- To get know `Firebase`
- More specifyied `AutoLayout` for other devices & orientations
- Cleaner architecture and coding practices
- More animations
- App Icon :see_no_evil:


## GIFs
The GIFs demonstrate the functionality of the app.\
Hover to see the functionality. \
(Sorry for the - partially - `potato` quality)

![foodlist-filter-search](FoodList_Search_Filter.gif "Searching and filtering the recipes")

***

![food-detailscreen-editing](FoodDetail_Page_Editing.gif "Recipe detail screen and editing an existing recipe")

***

![foodlist-delete-new-recipe](CreateFood_Delete.gif "Creating new recipes and deleting recipes from list")

***

![deliverylist-filter](FilterDeliveryList.gif "Filtering the delivery list")

***

![create-new-delivery](CreateDelivery_Person.gif "Creating a new delivery")

***
