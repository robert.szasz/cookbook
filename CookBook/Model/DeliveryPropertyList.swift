//
//  DeliveryPropertyList.swift
//  CookBook
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation

typealias DeliveryPropertyList = (uid: Int?, date: Date, toPerson: String, soup: FoodItem?, salad: FoodItem?, mainDish: FoodItem?, dessert: FoodItem?)
