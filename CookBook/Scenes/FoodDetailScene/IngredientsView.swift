//
//  IngredientsView.swift
//  CookBook
//
//  Created by Robert Szasz on 22/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import Then
import RxSwift

class IngredientsView: UIView {

    private let ingredients: [String]
    let maxWidth: CGFloat = UIScreen.main.bounds.width * 0.3
    
    // MARK: - View properties
    
    private let ingredientsLabel = UILabel().then {
        $0.text = NSLocalizedString("Ingredients", comment: "Ingredients title in ingredients view")
        $0.font = .systemFont(ofSize: 18, weight: .black)
        $0.textColor = .white
    }
    
    override class var requiresConstraintBasedLayout: Bool { true }
    
    // MARK: - Init
    convenience init(ingredients: [String]) {
        self.init(frame: .zero, ingredients: ingredients)
        setUpView()
    }
    
    init(frame: CGRect, ingredients: [String]) {
        self.ingredients = ingredients
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Set up view
    
    func setUpView() {
        backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        layer.cornerRadius = 10
        layer.masksToBounds = true
        
        addSubview(ingredientsLabel)
        ingredientsLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        ingredients.enumerated().forEach { (index, ingredient) in
            addIngredientToView(ingredient, afterLabel: index == 0 ? nil : subviews[index], isLast: index == (ingredients.count - 1))
        }
    }
    
    // MARK: - Helper methods
    
    private func addIngredientToView(_ str: String, afterLabel label: UIView?, isLast: Bool) {

        let view = makeIngredientLabelWithBulletPoint(forString: str, width: maxWidth - 20)
        addSubview(view)
        view.anchor(top: label?.bottomAnchor ?? ingredientsLabel.bottomAnchor, left: ingredientsLabel.leftAnchor, bottom: isLast ? self.bottomAnchor : nil, right: self.rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: -10, paddingRight: -10, width: view.frame.width, height: 0)
    }
    
    private func makeIngredientLabelWithBulletPoint(forString str: String, width: CGFloat) -> UIView {
        let view = UIView()
        
        let bulletPointLabel = UILabel().then {
            $0.text = "\u{2022}  "
            $0.font = .systemFont(ofSize: 16, weight: .heavy)
            $0.textColor = .white
        }
        
        let ingredientLabel = UILabel().then {
            $0.text = str
            $0.font = .systemFont(ofSize: 14, weight: .regular)
            $0.numberOfLines = 0
            $0.textColor = .white
        }
        
        view.addSubview(bulletPointLabel)
        bulletPointLabel.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: ceil(bulletPointLabel.intrinsicContentSize.width), height: ceil(bulletPointLabel.intrinsicContentSize.height))
        
        view.addSubview(ingredientLabel)
        ingredientLabel.anchor(top: view.topAnchor, left: bulletPointLabel.rightAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: width, height: ingredientLabel.height(withConstrainedWidth: width))
        
        return view
    }
}
