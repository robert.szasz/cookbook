//
//  GlobalFunctions.swift
//  CookBook
//
//  Created by Robert Szasz on 28/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import UIKit

fileprivate let personListArray = "PersonList"

func imageFromFileName(_ filename: String) -> UIImage {
    let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let documentDir = URL(fileURLWithPath: documentDirectory)
    let imageURL = documentDir.appendingPathComponent(filename)
    guard let data = try? Data(contentsOf: imageURL) else { fatalError() }

    let image = UIImage(data: data)!
    return image
}


func getDefaultImageForFood(_ int: Int) -> UIImage {
    switch int {
    case 1: return UIImage(named: "soup")!
    case 2: return UIImage(named: "salad")!
    case 3: return UIImage(named: "mainDish")!
    case 4: return UIImage(named: "dessert")!
    default: fatalError("Invalid food kind integer")
    }
}

func getNumberForFoodKind(_ foodKind: FoodKind) -> Int {
    switch foodKind {
    case .Soup: return 1
    case .Salad: return 2
    case .MainDish: return 3
    case .Dessert: return 4
    default: fatalError("Invalid food kind")
    }
}

func getFoodKindForNumber(_ int: Int) -> FoodKind {
    switch int {
    case 1: return .Soup
    case 2: return .Salad
    case 3: return .MainDish
    case 4: return .Dessert
    default: fatalError("Invalid number")
    }
}

func getNameForFoodKind(_ foodKind: FoodKind) -> String {
    switch foodKind {
    case .Soup: return NSLocalizedString("soup", comment: "soup")
    case .Salad: return NSLocalizedString("salad", comment: "salad")
    case .MainDish: return NSLocalizedString("main dish", comment: "main dish")
    case .Dessert: return NSLocalizedString("dessert", comment: "dessert")
    default: fatalError("Invalid food kind")
    }
}

func getPersonListArrayFromUserDefaults() -> [String] {
    let defaults = UserDefaults.standard
    return defaults.stringArray(forKey: personListArray) ?? [String]()
}

func setPersonListArrayToUserDefaults(_ array: [String]) {
    let defaults = UserDefaults.standard
    defaults.set(array, forKey: personListArray)
    defaults.synchronize()
}
