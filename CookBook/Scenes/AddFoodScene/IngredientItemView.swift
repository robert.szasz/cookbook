//
//  IngredientItemView.swift
//  CookBook
//
//  Created by Robert Szasz on 28/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Then
import Action

class IngredientItem: UIView {
    
    let name: String
    
    private var labelHeightAnchor: NSLayoutConstraint!
    
    // MARK: - View properties
    
    var xButton = UIButton(type: .custom).then {
        $0.setTitle("x", for: .normal)
        $0.titleLabel?.textAlignment = .center
        $0.titleLabel?.font = .systemFont(ofSize: 15, weight: .heavy)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0.3), for: .highlighted)
        $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.borderWidth = 2
        $0.layer.cornerRadius = 10
        $0.layer.masksToBounds = true
    }
    
    lazy var container = UIView().then {
        $0.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.cornerRadius = 2
        
        let label = UILabel().then {
            $0.text = name
            $0.font = .systemFont(ofSize: 14, weight: .regular)
            $0.numberOfLines = 0
            $0.textColor = .white
            $0.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
            $0.layer.cornerRadius = 2
            $0.layer.masksToBounds = true
        }
        
        $0.addSubview(label)
        label.anchor(top: $0.topAnchor, left: $0.leftAnchor, bottom: $0.bottomAnchor, right: $0.rightAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: -5, paddingRight: -5, width: 0, height: 0)
    }
    
    // MARK: - Init
    
    convenience init(name: String) {
        self.init(frame: .zero, name: name)
        setUpView()
    }
    
    init(frame: CGRect, name: String) {
        self.name = name
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Set up view
    
    private func setUpView() {
        addSubview(xButton)
        xButton.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 20, height: 20)
        
        addSubview(container)
        container.anchor(top: topAnchor, left: xButton.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 5, paddingBottom: -5, paddingRight: 0, width: 0, height: 0)
    }
    
    func updateLayout() {
        let remainingSpace = frame.width - xButton.frame.width - 5
        let label = container.subviews.first! as! UILabel

        if labelHeightAnchor == nil { labelHeightAnchor = label.heightAnchor.constraint(equalToConstant: label.height(withConstrainedWidth: remainingSpace)) }
        if !labelHeightAnchor.isActive { labelHeightAnchor.isActive = true }
        labelHeightAnchor.constant = label.height(withConstrainedWidth: remainingSpace)
    }
}
