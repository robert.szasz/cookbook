//
//  MockAddFoodViewModel.swift
//  CookBookTests
//
//  Created by Robert Szasz on 28/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

@testable import CookBook

extension AddFoodViewModel {
    var ffpl: FullFoodPropertyList {
        FullFoodPropertyList(uid: -1,
                             name: name.value,
                             overview: overview.value,
                             recipe: recipe.value,
                             prepTime: prepTime.value,
                             ingredients: ingredients.value,
                             photo: photo.value,
                             isVegetarian: restrictions.value.contains("isVegetarian"),
                             isVegan: restrictions.value.contains("isVegan"),
                             isLactosefree: restrictions.value.contains("isLactosefree"),
                             isGlutenfree: restrictions.value.contains("isGlutenfree"),
                             isSugarfree: restrictions.value.contains("isSugarfree"),
                             foodKind: foodKind.value,
                             difficulty: difficulty.value)
    }
}
