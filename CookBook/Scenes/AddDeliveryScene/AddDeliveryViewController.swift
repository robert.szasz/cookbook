//
//  AddDeliveryViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 30/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Action
import Then
import RealmSwift

class AddDeliveryViewController: UIViewController, BindableType, ConfigurableType, UIAdaptivePresentationControllerDelegate, UIPopoverPresentationControllerDelegate {
    
    var viewModel: AddDeliveryViewModel!
    private var bag = DisposeBag()
        
    private let formatter = DateFormatter().then { $0.dateFormat = "dd/MM/yy" }
    private let calendar = Calendar.current
    
    // MARK: - View properties
    
    let toLabel: UILabel = UILabel().then {
        $0.text = NSLocalizedString("To:", comment: "To label")
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .heavy)
    }
    
    let personLabel: UILabel = UILabel().then {
        $0.font = .systemFont(ofSize: 22, weight: .semibold)
        $0.textColor = .darkGray
        $0.text = NSLocalizedString("Person", comment: "Person label")
    }
    
    var contactsButton: UIButton = UIButton(type: .custom).then {
        $0.setImage(UIImage(named: "contacts_icon")!.withRenderingMode(.alwaysTemplate), for: .normal)
        $0.tintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.imageView?.contentMode = .scaleAspectFit
        $0.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.borderWidth = 4
        $0.layer.cornerRadius = 18
        $0.layer.masksToBounds = true
    }
    
    let foodLabel: UILabel = UILabel().then {
        $0.text = NSLocalizedString("Choose a menu:", comment: "Choose menu label")
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .heavy)
    }
    
    var plusButton = UIButton(type: .custom).then {
        $0.setTitle("+", for: .normal)
        $0.titleLabel?.textAlignment = .center
        $0.titleLabel?.font = .systemFont(ofSize: 30, weight: .heavy)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0.3), for: .highlighted)
        $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.borderWidth = 4
        $0.layer.cornerRadius = 18
        $0.layer.masksToBounds = true
    }
    
    var soupButton = UIButton(type: .custom).then {
        $0.isSelected = false
        $0.setTitle("🍜", for: .normal)
        $0.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        $0.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0).cgColor
        $0.layer.cornerRadius = 25
        $0.clipsToBounds = true
        $0.tag = 1
    }
    
    var saladButton = UIButton(type: .custom).then {
        $0.isSelected = false
        $0.setTitle("🥬", for: .normal)
        $0.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        $0.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0).cgColor
        $0.layer.cornerRadius = 25
        $0.clipsToBounds = true
        $0.tag = 2
    }
    
    var mainDishButton = UIButton(type: .custom).then {
        $0.isSelected = false
        $0.setTitle("🥩", for: .normal)
        $0.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        $0.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0).cgColor
        $0.layer.cornerRadius = 25
        $0.clipsToBounds = true
        $0.tag = 3
    }
    
    var dessertButton = UIButton(type: .custom).then {
        $0.isSelected = false
        $0.setTitle("🍰", for: .normal)
        $0.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        $0.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0).cgColor
        $0.layer.cornerRadius = 25
        $0.clipsToBounds = true
        $0.tag = 4
    }
    
    var getRandomButton = UIButton(type: .custom).then {
        $0.setTitle(NSLocalizedString("Get Random", comment: "Get random button label"), for: .normal)
        $0.tintColor = .white
        $0.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
        $0.backgroundColor = #colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 1)
        $0.contentVerticalAlignment = .center
        $0.titleLabel?.textAlignment = .center
    }
    
    let dateLabel = UILabel().then {
        $0.text = NSLocalizedString("Date:", comment: "Date label")
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .heavy)
    }
    
    lazy var pickedDateLabel = UILabel().then {
        $0.font = .systemFont(ofSize: 22, weight: .semibold)
        $0.textColor = .darkGray
        $0.text = formatter.string(from: Date())
    }
    
    var calendarButton = UIButton(type: .custom).then {
        $0.setImage(UIImage(named: "calendar_icon")!.withRenderingMode(.alwaysTemplate), for: .normal)
        $0.tintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.imageView?.contentMode = .scaleAspectFit
        $0.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.borderWidth = 4
        $0.layer.cornerRadius = 18
        $0.layer.masksToBounds = true
    }
    
    lazy var contactAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        
        let names = self.viewModel.personList.value
        let contactPopover = PersonListViewController(names: names)
        contactPopover.modalPresentationStyle = .popover
        contactPopover.preferredContentSize = CGSize(width: 200, height: names.count * 30 + 15)
        
        let selectedName = contactPopover.selectedName
            .asObservable()
            .map { $0.trimmingCharacters(in: .whitespacesAndNewlines) }
            .share()
            
        selectedName
            .bind(to: self.viewModel.person)
            .disposed(by: contactPopover.bag)
        
        selectedName
            .asDriver(onErrorJustReturn: NSLocalizedString("Error", comment: "On error just return"))
            .drive(self.personLabel.rx.text)
            .disposed(by: self.bag)
        
        if let popoverPresentationController = contactPopover.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = self.contactsButton.frame
            popoverPresentationController.delegate = self
            self.present(contactPopover, animated: true, completion: nil)
        }
        return .just(())
    }
    
    lazy var addContactAction = CocoaAction { [weak self] in
        guard let self = self else { return .just(()) }
        let mockObject = PublishSubject<Void>()
                
        let alert = self.createAddNameAlert()
        self.present(alert, animated: true) {
            mockObject.onCompleted()
        }
        return mockObject.asObservable()
    }
    
    lazy var getRandomAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        
        var choosenArray = [FoodKind]()
        for tag in 1...4 {
            let foodButton = self.view.viewWithTag(tag) as! UIButton
            if foodButton.backgroundColor == #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) { choosenArray.append(getFoodKindForNumber(tag)) }
        }
        
        return self.getRandomsForArray(choosenArray).asObservable()
    }
    
    lazy var addDateAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        
        let datePopover = CalendarViewController(singleDayPick: true)
        datePopover.modalPresentationStyle = .popover
        datePopover.preferredContentSize = CGSize(width: 280, height: 350)
        
        let selectedDate = datePopover.calendar.choosenDay
            .asObservable()
            .share()
            
        selectedDate
            .bind(to: self.viewModel.date)
            .disposed(by: self.bag)
        
        selectedDate
            .map { self.formatter.string(from: $0) }
            .asDriver(onErrorJustReturn: NSLocalizedString("Error", comment: "On error just return"))
            .drive(self.pickedDateLabel.rx.text)
            .disposed(by: self.bag)
        
        if let popoverPresentationController = datePopover.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = self.calendarButton.frame
            popoverPresentationController.delegate = self
            self.present(datePopover, animated: true, completion: nil)
        }
        return .just(())
    }
        
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        // Do any additional setup after loading the view.
        addSubviews()
        addLayout()
        setUpFoodButtons()
    }
    
    // MARK: - Subviews & Layout
    
    private func addSubviews() {
        view.addSubview(toLabel)
        view.addSubview(personLabel)
        view.addSubview(contactsButton)
        view.addSubview(plusButton)
        view.addSubview(foodLabel)
        view.addSubview(soupButton)
        view.addSubview(saladButton)
        view.addSubview(mainDishButton)
        view.addSubview(dessertButton)
        view.addSubview(getRandomButton)
        view.addSubview(dateLabel)
        view.addSubview(pickedDateLabel)
        view.addSubview(calendarButton)
    }
    
    private func addLayout() {
        toLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        personLabel.anchor(top: toLabel.bottomAnchor, left: toLabel.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        contactsButton.anchor(top: nil, left: personLabel.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: contactsButton.layer.cornerRadius * 2, height: contactsButton.layer.cornerRadius * 2)
        contactsButton.centerYAnchor.constraint(equalTo: personLabel.centerYAnchor).isActive = true
        
        plusButton.anchor(top: nil, left: contactsButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, width: plusButton.layer.cornerRadius * 2, height: plusButton.layer.cornerRadius * 2)
        plusButton.centerYAnchor.constraint(equalTo: contactsButton.centerYAnchor).isActive = true
        
        foodLabel.anchor(top: personLabel.bottomAnchor, left: toLabel.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        let spaceBetweenButtons: CGFloat = (UIScreen.main.bounds.width - 4 * 50) / 5
        soupButton.anchor(top: foodLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: spaceBetweenButtons, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        saladButton.anchor(top: soupButton.topAnchor, left: soupButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: spaceBetweenButtons, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        mainDishButton.anchor(top: soupButton.topAnchor, left: saladButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: spaceBetweenButtons, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        dessertButton.anchor(top: soupButton.topAnchor, left: mainDishButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: spaceBetweenButtons, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        getRandomButton.anchor(top: soupButton.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 60, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: getRandomButton.intrinsicContentSize.width + 30, height: getRandomButton.intrinsicContentSize.height + 10)
        getRandomButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        getRandomButton.roundCorners()
        
        dateLabel.anchor(top: getRandomButton.bottomAnchor, left: toLabel.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        pickedDateLabel.anchor(top: dateLabel.bottomAnchor, left: dateLabel.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        calendarButton.anchor(top: nil, left: pickedDateLabel.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: calendarButton.layer.cornerRadius * 2, height: calendarButton.layer.cornerRadius * 2)
        calendarButton.centerYAnchor.constraint(equalTo: pickedDateLabel.centerYAnchor).isActive = true
    }
    
    private func setUpFoodButtons() {
        for tag in 1...4 {
            guard var foodButton = view.viewWithTag(tag) as? UIButton else { return }
            
            let foodPlusButton = makePlusButton(forFoodButton: foodButton, withTag: 10 + tag)
            view.addSubview(foodPlusButton)
            foodButton.rx.action = viewModel.onFoodButtonTapped(withPlusButton: foodPlusButton, foodButton)
        }
        
        contactsButton.rx.action = contactAction
        plusButton.rx.action = addContactAction
        getRandomButton.rx.action = getRandomAction
        calendarButton.rx.action = addDateAction

    }
    
    
    
    // MARK: - Bind ViewModel
    
    func bindViewModel() {
        // TODO: PersonList
        
        viewModel.soup.asObservable()
            .skip(1)
            .subscribe(onNext: { [unowned self] _ in
                let soupPlusButton = self.view.viewWithTag(11) as! UIButton
                soupPlusButton.setTitle("", for: .normal)
                soupPlusButton.setImage(UIImage(named: "checkmark")!.withRenderingMode(.alwaysTemplate), for: .normal)
                soupPlusButton.tintColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
                soupPlusButton.layer.borderColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
            })
            .disposed(by: bag)
        
        viewModel.salad.asObservable()
            .skip(1)
            .subscribe(onNext: { [unowned self] _ in
                let saladPlusButton = self.view.viewWithTag(12) as! UIButton
                saladPlusButton.setTitle("", for: .normal)
                saladPlusButton.setImage(UIImage(named: "checkmark")!.withRenderingMode(.alwaysTemplate), for: .normal)
                saladPlusButton.tintColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
                saladPlusButton.layer.borderColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
            })
            .disposed(by: bag)
        
        viewModel.mainDish.asObservable()
            .skip(1)
            .subscribe(onNext: { [unowned self] _ in
                let mainDishPlusButton = self.view.viewWithTag(13) as! UIButton
                mainDishPlusButton.setTitle("", for: .normal)
                mainDishPlusButton.setImage(UIImage(named: "checkmark")!.withRenderingMode(.alwaysTemplate), for: .normal)
                mainDishPlusButton.tintColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
                mainDishPlusButton.layer.borderColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
            })
            .disposed(by: bag)
        
        viewModel.dessert.asObservable()
            .skip(1)
            .subscribe(onNext: { [unowned self] _ in
                let dessertPlusButton = self.view.viewWithTag(14) as! UIButton
                dessertPlusButton.setTitle("", for: .normal)
                dessertPlusButton.setImage(UIImage(named: "checkmark")!.withRenderingMode(.alwaysTemplate), for: .normal)
                dessertPlusButton.tintColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
                dessertPlusButton.layer.borderColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
            })
            .disposed(by: bag)
    }
    
    // MARK: - Configurable Type
    
    func configureSelfAfterPushedOnNavStack() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: "Nav bar back button"), style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Save", comment: "Nav bar save button"), style: .done, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .black
        navigationItem.title = NSLocalizedString("Add Delivery", comment: "Nav bar title add delivery")
        
        navigationItem.leftBarButtonItem!.rx.action = CocoaAction { [unowned self] _ in
            return self.viewModel.sceneCoordinator
                .pop(animated: true)
                .asObservable()
                .map { _ in }
        }
        
        navigationItem.rightBarButtonItem!.rx.action = viewModel.onSaveDelivery()
        
        if #available(iOS 13.0, *) {
            navigationController?.presentationController?.rx.didDismissController
                .subscribe(onNext: { [weak self] _ in
                    guard let self = self else { return }
                    self.viewModel.sceneCoordinator
                        .swipeDownDismiss(self)
                    })
                .disposed(by: bag)
        }
    }
    
    // MARK: - Helper methods

    private func makePlusButton(forFoodButton foodButton: UIButton, withTag tagNum: Int) -> UIButton {
        var plusButton = UIButton(type: .custom).then {
            $0.setTitle("+", for: .normal)
            $0.contentVerticalAlignment = .center
            $0.titleLabel?.textAlignment = .center
            $0.titleLabel?.font = .systemFont(ofSize: 24, weight: .heavy)
            $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
            $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0.3), for: .highlighted)
            $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
            $0.layer.borderWidth = 3
            $0.layer.cornerRadius = 15
            $0.layer.masksToBounds = true
            $0.isHidden = true
            $0.alpha = 0
            $0.tag = tagNum
        }
        plusButton.rx.action = viewModel.onFoodPlusButtonTapped(foodKindForButton(foodButton))
        
        return plusButton
    }
    
    private func foodKindForButton(_ button: UIButton) -> FoodKind {
        switch button.titleLabel?.text! {
        case "🍜": return .Soup
        case "🥬": return .Salad
        case "🥩": return .MainDish
        case "🍰": return .Dessert
        default: fatalError("Button not recognized!")
        }
    }
        
    private func createAddNameAlert() -> UIAlertController {
        let alert = UIAlertController(title: NSLocalizedString("Add Person", comment: "Alert title"), message: NSLocalizedString("Write the person's name in the textfield and press OK", comment: "Alert message"), preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: NSLocalizedString("Add", comment: "Alert action"), style: UIAlertAction.Style.default, handler: { [weak self] _ in
            guard let name = alert.textFields?[0].text, let self = self else { return }
            self.personLabel.text = name
            self.viewModel.personList.accept(self.viewModel.personList.value + [name])
            self.viewModel.person.accept(name)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Alert action"), style: UIAlertAction.Style.cancel, handler: nil))
        alert.addTextField { textField in textField.placeholder = NSLocalizedString("Name", comment: "Person name") }
                
        return alert
    }
    
    private func getRandomsForArray(_ array: [FoodKind]) -> Observable<Void> {
        return Observable<Void>.create { observer in
            let localFoodService = FoodService()
            _ = localFoodService.foods()
                .take(1)
                .do(onNext: { results in
                    for food in array {
                        let foodInt = getNumberForFoodKind(food)
                        print(foodInt)
                        let arr = results.toArray().filter { $0.enumFoodKind == food }
                        guard let randomFood = arr.randomElement() else { continue }
                        switch foodInt {
                        case 1: self.viewModel.soup.accept(randomFood)
                        case 2: self.viewModel.salad.accept(randomFood)
                        case 3: self.viewModel.mainDish.accept(randomFood)
                        case 4: self.viewModel.dessert.accept(randomFood)
                        default: break
                        }
                        observer.onNext(())
                    }
                })
                .subscribe(onCompleted: { observer.onCompleted() })
            return Disposables.create()
        }
    }
}

extension AddDeliveryViewController {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
