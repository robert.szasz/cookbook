//
//  PickFoodViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 30/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Action

fileprivate let cellIdentifier = "SubtitleCell"

class PickFoodViewController: UIViewController, BindableType, ConfigurableType {

    var viewModel: PickFoodViewModel!
    var dataSource: RxTableViewSectionedAnimatedDataSource<FoodSection>!
    var bag = DisposeBag()

    // MARK: - View properties
    
    lazy var searchBar = UISearchBar().then {
        $0.isTranslucent = false
        $0.placeholder = NSLocalizedString("Search for ", comment: "Search bar placeholder") + getNameForFoodKind(viewModel.foodType) + "..."
        $0.showsCancelButton = true
        
        var searchTextField = $0.value(forKey: "searchField") as! UITextField
        searchTextField.backgroundColor = UIColor.init(white: 1, alpha: 0.5)
        
        let attributes:[NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 17)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    }
    
    let tableView = UITableView().then {
        $0.backgroundColor = .white
        $0.separatorStyle = .none
        $0.register(DefaultSubtitleCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        addLayout()
        
        configureDataSource()
        
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    
    }
    
    // MARK: - Subviews & Layout
    
    private func addSubviews() {
        view.addSubview(searchBar)
        view.addSubview(tableView)
    }
    
    private func addLayout() {
        searchBar.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 60)
        
        tableView.anchor(top: searchBar.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    // MARK: - Bind ViewModel
    
    func bindViewModel() {
        viewModel.sectionedItems
            .map { [$0] }
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        searchBar.rx.searchButtonClicked
            .map { [weak self] _ in self?.searchBar.text }
            .filter { $0 != nil}
            .map { str -> String in str! }
            .filter { !$0.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty }
            .bind(to: viewModel.textFilter)
            .disposed(by: bag)
        
        searchBar.rx.searchButtonClicked
            .subscribe(onNext: { [weak self] _ in
                self?.searchBar.resignFirstResponder()
            })
            .disposed(by: bag)
        
        searchBar.rx.textDidEndEditing
            .map { [weak self] _ -> String in (self?.searchBar.text)! }
            .filter { $0 == "" }
            .bind(to: viewModel.textFilter)
            .disposed(by: bag)
        
        searchBar.rx.cancelButtonClicked
            .do(onNext: { [weak self] _ in
                self?.searchBar.resignFirstResponder()
                self?.searchBar.text = ""
            })
            .map { _ -> String in "" }
            .bind(to: viewModel.textFilter)
            .disposed(by: bag)
        
        tableView.rx.itemSelected
            .do(onNext: { [unowned self] indexPath in
                self.tableView.deselectRow(at: indexPath, animated: true)
            })
            .map { [unowned self] indexPath in
                try! self.dataSource.model(at: indexPath) as! FoodItem
            }
            .bind(to: viewModel.pickedFood)
            .disposed(by: bag)
        
        viewModel.pickedFood.asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.onDismissSelf().execute()
            })
            .disposed(by: bag)
    }
    
    // MARK: - Configurable Type

    func configureSelfAfterPushedOnNavStack() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "Alert action"), style: .plain, target: nil, action: nil)

        navigationItem.leftBarButtonItem!.rx.action = CocoaAction { _ in
            return self.viewModel.sceneCoordinator.pop()
                .asObservable()
                .map { _ in }
        }
    }
    
    // MARK: - Helper methods
    
    private func configureDataSource() {
        dataSource = RxTableViewSectionedAnimatedDataSource<FoodSection>(configureCell: { dataSource, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DefaultSubtitleCell
            
            cell.imageView?.contentMode = .scaleAspectFill
            cell.imageView?.clipsToBounds = true
            if item.pictureURL != "" { cell.imageView?.image = imageFromFileName(item.pictureURL) }
            else { cell.imageView?.image = getDefaultImageForFood(getNumberForFoodKind(item.enumFoodKind)) }
            
            cell.textLabel?.text = item.name
            cell.detailTextLabel?.text = item.overview
            
            return cell
        }, titleForHeaderInSection: { dataSource, index in
            dataSource.sectionModels[index].model
        })
    }
}


class DefaultSubtitleCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
