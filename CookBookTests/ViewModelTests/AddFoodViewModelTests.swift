//
//  AddFoodViewModelTests.swift
//  CookBookTests
//
//  Created by Robert Szasz on 28/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm
import RxTest
import RxBlocking

@testable import CookBook

class AddFoodViewModelTests: XCTestCase {
    
    private var bag = DisposeBag()
    private let foodRealm: Realm = TestFoodRealmProvider.itemsOnDisk.realm

    private var p1: FullFoodPropertyList = (-1, "TestName", "TestOverview", "TestRecipe", "TestPrepTime", ["Test1", "Test2", "Test3"], photo: "TestPhoto.jpeg", isVegetarian: true, isVegan: false, isLactosefree: true, isGlutenfree: false, isSugarfree: true, FoodKind.MainDish, DifficultyLevel.Middle)
    

    private func createViewModelAdd() -> AddFoodViewModel {
        return AddFoodViewModel(coordinator: SceneCoordinator(window: UIWindow()), service: FoodService(TestFoodRealmProvider.self), foodToEdit: nil)
    }
    
    private func createViewModelEdit() throws -> AddFoodViewModel {
        let food = FoodItem(properties: p1)
        try foodRealm.write {
            foodRealm.add(food)
        }
        return AddFoodViewModel(coordinator: SceneCoordinator(window: UIWindow()), service: FoodService(TestFoodRealmProvider.self), foodToEdit: food)
    }
    
    override func setUp() {
        super.setUp()
        
        bag = DisposeBag()
    }
    
    override func tearDown() {
        guard let food = foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1) else { return }
        do {
            try foodRealm.write {
                foodRealm.delete(food)
            }
        } catch {
            fatalError()
        }
        
        super.tearDown()
    }

    // MARK: - Tests
    func testInitializedAddViewModel() {
        let viewModel = createViewModelAdd()
        XCTAssertNotNil(viewModel.sceneCoordinator)
        XCTAssertNotNil(viewModel.foodService)
        XCTAssertNil(viewModel.foodToEdit)
    }
    
    func testInputsAddViewModel() throws {
        let viewModel = createViewModelAdd()
        
        let photo = viewModel.photo
        XCTAssertEqual(try photo.asObservable().toBlocking().first(), "")
        
        let name = viewModel.name
        XCTAssertEqual(try name.asObservable().toBlocking().first(), "")
        
        let overview = viewModel.overview
        XCTAssertEqual(try overview.asObservable().toBlocking().first(), "")
        
        let foodKind = viewModel.foodKind
        XCTAssertEqual(try foodKind.asObservable().toBlocking().first(), FoodKind.MainDish)
        
        let recipe = viewModel.recipe
        XCTAssertEqual(try recipe.asObservable().toBlocking().first(), "")
        
        let ingredients = viewModel.ingredients
        XCTAssertEqual(try ingredients.asObservable().toBlocking().first(), [])
        
        let prepTime = viewModel.prepTime
        XCTAssertEqual(try prepTime.asObservable().toBlocking().first(), "")
        
        let difficulty = viewModel.difficulty
        XCTAssertEqual(try difficulty.asObservable().toBlocking().first(), DifficultyLevel.MiddleLow)
        
        let restrictions = viewModel.restrictions
        XCTAssertEqual(try restrictions.asObservable().toBlocking().first(), [])
    }
    
    func testFoodSavingAddViewModel() throws {
        let viewModel = createViewModelAdd()
        
        viewModel.photo.accept(p1.photo)
        viewModel.name.accept(p1.name)
        viewModel.overview.accept(p1.overview)
        viewModel.foodKind.accept(p1.foodKind)
        viewModel.recipe.accept(p1.recipe)
        viewModel.ingredients.accept(p1.ingredients)
        viewModel.prepTime.accept(p1.prepTime)
        viewModel.difficulty.accept(p1.difficulty)
        let restrArr = FoodItem(properties: p1).getFoodRestrictionArray()
        viewModel.restrictions.accept(restrArr)
        
        viewModel.foodService.createFood(properties: viewModel.ffpl).asObservable().subscribe(onCompleted: {
                XCTAssertNotNil(self.foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1))
                let food = self.foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!
                
                XCTAssertEqual(food.name, viewModel.name.value)
                XCTAssertEqual(food.pictureURL, viewModel.photo.value)
                XCTAssertEqual(food.overview, viewModel.overview.value)
                XCTAssertEqual(food.enumFoodKind, viewModel.foodKind.value)
                XCTAssertEqual(food.recipe, viewModel.recipe.value)
                XCTAssertEqual(food.ingredients.toArray(), viewModel.ingredients.value)
                XCTAssertEqual(food.prepTime, viewModel.prepTime.value)
                XCTAssertEqual(food.enumDifficultyLevel, viewModel.difficulty.value)
                XCTAssertEqual(food.getFoodRestrictionArray(), viewModel.restrictions.value)
            })
            .disposed(by: bag)
    }
    
    func testInitializedEditViewModel() throws {
        let viewModel = try createViewModelEdit()
        XCTAssertNotNil(viewModel.sceneCoordinator)
        XCTAssertNotNil(viewModel.foodService)
        XCTAssertNotNil(viewModel.foodToEdit)
    }
    
    func testInputsEditViewModel() throws {
        let viewModel = try createViewModelEdit()
        
        let photo = viewModel.photo
        XCTAssertEqual(try photo.asObservable().take(1).toBlocking().last(), "TestPhoto.jpeg" )
        
        let name = viewModel.name
        XCTAssertEqual(try name.asObservable().take(1).toBlocking().last(), "TestName" )
        
        let overview = viewModel.overview
        XCTAssertEqual(try overview.asObservable().take(1).toBlocking().last(), "TestOverview" )
        
        let foodKind = viewModel.foodKind
        XCTAssertEqual(try foodKind.asObservable().take(1).toBlocking().last(), FoodKind.MainDish )
        
        let recipe = viewModel.recipe
        XCTAssertEqual(try recipe.asObservable().take(1).toBlocking().last(), "TestRecipe" )
        
        let ingredients = viewModel.ingredients
        XCTAssertEqual(try ingredients.asObservable().take(1).toBlocking().last(), ["Test1", "Test2", "Test3"] )
        
        let prepTime = viewModel.prepTime
        XCTAssertEqual(try prepTime.asObservable().take(1).toBlocking().last(), "TestPrepTime" )
        
        let difficulty = viewModel.difficulty
        XCTAssertEqual(try difficulty.asObservable().take(1).toBlocking().last(), DifficultyLevel.Middle )
        
        let restrictions = viewModel.restrictions
        XCTAssertEqual(try restrictions.asObservable().take(1).toBlocking().last(), ["isVegetarian", "isLactosefree", "isSugarfree"] )
    }
    
    func testFoodUpdatingEditViewModel() throws {
        let viewModel = try createViewModelEdit()
        
        viewModel.photo.accept("New")
        viewModel.name.accept("New")
        viewModel.overview.accept("New")
        viewModel.foodKind.accept(.Dessert)
        viewModel.recipe.accept("New")
        viewModel.ingredients.accept(["New", "New"])
        viewModel.prepTime.accept("New")
        viewModel.difficulty.accept(.Easy)
        viewModel.restrictions.accept(["isGlutenfree"])
        
        viewModel.foodService.update(food: viewModel.foodToEdit!, properties: viewModel.ffpl)
            .asObservable()
            .take(1)
            .subscribe(onCompleted: {
                XCTAssertNotNil(self.foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1))
                let updatedFood = self.foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!
                
                XCTAssertEqual(updatedFood.name, viewModel.name.value)
                XCTAssertEqual(updatedFood.pictureURL, viewModel.photo.value)
                XCTAssertEqual(updatedFood.overview, viewModel.overview.value)
                XCTAssertEqual(updatedFood.enumFoodKind, viewModel.foodKind.value)
                XCTAssertEqual(updatedFood.recipe, viewModel.recipe.value)
                XCTAssertEqual(updatedFood.ingredients.toArray(), viewModel.ingredients.value)
                XCTAssertEqual(updatedFood.prepTime, viewModel.prepTime.value)
                XCTAssertEqual(updatedFood.enumDifficultyLevel, viewModel.difficulty.value)
                XCTAssertEqual(updatedFood.getFoodRestrictionArray(), viewModel.restrictions.value)
            })
            .disposed(by: bag)
    }
}
