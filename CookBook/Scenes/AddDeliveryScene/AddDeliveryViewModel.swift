//
//  AddDeliveryViewModel.swift
//  CookBook
//
//  Created by Robert Szasz on 30/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action
import Then


struct AddDeliveryViewModel {
    let sceneCoordinator: SceneCoordinatorType
    let deliveryService: DeliveryServiceType
    
    private let bag = DisposeBag()
    
    init(coordinator: SceneCoordinatorType, deliveryService: DeliveryServiceType) {
        self.sceneCoordinator = coordinator
        self.deliveryService = deliveryService
                
        personList.asObservable()
            .skip(1)
            .subscribe(onNext: { list in
                setPersonListArrayToUserDefaults(list)
            })
            .disposed(by: bag)
        
        // TESTING
        soup.asObservable().subscribe(onNext: { print($0) })
        salad.asObservable().subscribe(onNext: { print($0) })
        mainDish.asObservable().subscribe(onNext: { print($0) })
        dessert.asObservable().subscribe(onNext: { print($0) })
    }
    
    // MARK: - Inputs
    
    let person = BehaviorRelay<String>(value: "")
    
    let soup = BehaviorRelay<FoodItem?>(value: nil)
    
    let salad = BehaviorRelay<FoodItem?>(value: nil)
    
    let mainDish = BehaviorRelay<FoodItem?>(value: nil)
    
    let dessert = BehaviorRelay<FoodItem?>(value: nil)
    
    let date = BehaviorRelay<Date>(value: Date())
    
    // MARK: - Outputs
    
    let personList = BehaviorRelay<[String]>(value: getPersonListArrayFromUserDefaults())
    
    private var dpl: DeliveryPropertyList {
        DeliveryPropertyList(uid: nil,
                             date: date.value,
                             toPerson: person.value,
                             soup: soup.value,
                             salad: salad.value,
                             mainDish: mainDish.value,
                             dessert: dessert.value)
    }
    
    func onFoodButtonTapped(withPlusButton plusButton: UIButton, _ foodButton: UIButton) -> CocoaAction {
        return CocoaAction { _ in
            if plusButton.frame == CGRect.zero {
                plusButton.frame = CGRect(origin: CGPoint(x: foodButton.center.x - plusButton.layer.cornerRadius,
                                                      y: foodButton.center.y - plusButton.layer.cornerRadius),
                                      size: CGSize(width: plusButton.layer.cornerRadius * 2,
                                                   height: plusButton.layer.cornerRadius * 2))}
            
            let finishedSubject = PublishSubject<Void>()
            
            if plusButton.alpha == 0 {
                plusButton.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    foodButton.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
                    plusButton.alpha = 1
                    plusButton.transform = CGAffineTransform(translationX: 0, y: foodButton.frame.height / 2 + plusButton.frame.height / 2 + 10)
                }) { (_) in
                    finishedSubject.onCompleted()
                }
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    foodButton.backgroundColor = .white
                    plusButton.alpha = 0
                    plusButton.transform = CGAffineTransform(translationX: 0, y: 0)
                }) { (_) in
                    switch foodButton.titleLabel!.text! {
                    case "🍜": self.soup.accept(nil)
                    case "🥬": self.salad.accept(nil)
                    case "🥩": self.mainDish.accept(nil)
                    case "🍰": self.dessert.accept(nil)
                    default: break
                    }
                    
                    plusButton.isHidden = true
                    plusButton.setTitle("+", for: .normal)
                    plusButton.setImage(nil, for: .normal)
                    plusButton.tintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
                    plusButton.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
                    
                    finishedSubject.onCompleted()
                }
            }

            return finishedSubject.asObservable()
        }
    }
    
    func onFoodPlusButtonTapped(_ foodButton: FoodKind) -> CocoaAction {
        return CocoaAction { _ in
            let mockObject = PublishSubject<Void>()
            let pickFoodViewModel = PickFoodViewModel(coordinator: self.sceneCoordinator, foodService: FoodService(), foodType: foodButton)
            
            pickFoodViewModel.pickedFood
                .subscribe(onNext: {food in
                    switch foodButton {
                    case .Soup: self.soup.accept(food)
                    case .Salad: self.salad.accept(food)
                    case .MainDish: self.mainDish.accept(food)
                    case .Dessert: self.mainDish.accept(food)
                    }
                })
                .disposed(by: pickFoodViewModel.bag)
            
            self.sceneCoordinator
                .transition(to: DeliveryScene.pickFood(pickFoodViewModel), type: .push)
            
            mockObject.onCompleted()
            return mockObject.asObservable()
        }
    }
    
    func onSaveDelivery() -> CocoaAction {
        return CocoaAction { _ in
            return self.deliveryService
                .createDelivery(properties: self.dpl)
                .flatMap { _ -> Observable<Void> in
                    return self.sceneCoordinator
                        .pop(animated: true)
                        .asObservable()
                        .map { _ in }
                }
        }
    }
    
}
