//
//  DifficultyView.swift
//  CookBook
//
//  Created by Robert Szasz on 22/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import Then
import RxSwift

class DifficultyView: UIView {
    
    private let difficultyLevel: DifficultyLevel
    
    // MARK: - View properties
    
    lazy var c1 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10)).then {
        $0.layer.addSublayer(makeCircleSublayer(withColor: #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)))
        $0.translatesAutoresizingMaskIntoConstraints = false
    }
    
    lazy var c2 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10)).then {
        $0.layer.addSublayer(makeCircleSublayer(withColor: #colorLiteral(red: 0.1803921569, green: 0.8, blue: 0.4431372549, alpha: 1)))
        $0.translatesAutoresizingMaskIntoConstraints = false
    }
     
    lazy var c3 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10)).then {
        $0.layer.addSublayer(makeCircleSublayer(withColor: #colorLiteral(red: 0.9450980392, green: 0.768627451, blue: 0.05882352941, alpha: 1)))
        $0.translatesAutoresizingMaskIntoConstraints = false
    }
    
    lazy var c4 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10)).then {
        $0.layer.addSublayer(makeCircleSublayer(withColor: #colorLiteral(red: 0.9529411765, green: 0.6117647059, blue: 0.07058823529, alpha: 1)))
        $0.translatesAutoresizingMaskIntoConstraints = false
    }
    
    lazy var c5 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10)).then {
        $0.layer.addSublayer(makeCircleSublayer(withColor: #colorLiteral(red: 0.8274509804, green: 0.3294117647, blue: 0, alpha: 1)))
        $0.translatesAutoresizingMaskIntoConstraints = false
    }
    
    lazy var circleArr = [c1, c2, c3, c4, c5]
    
    override class var requiresConstraintBasedLayout: Bool { true }
    
    // MARK: - Init
    
    convenience init(difficulty: DifficultyLevel) {
        self.init(frame: .zero, difficulty: difficulty)
        setUpView()
    }
    
    init(frame: CGRect, difficulty: DifficultyLevel) {
        self.difficultyLevel = difficulty
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Set up view
    
    private func setUpView() {
        backgroundColor = .white
        
        for i in 1...circleArr.count {
            if i > difficultyLevel.rawValue {
                circleArr[i - 1].alpha = 0.1
            }
        }
                
        let stackView = UIStackView(arrangedSubviews: circleArr)
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        addSubview(stackView)
        stackView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    // MARK: - Helper methods
    
    private func makeCircleSublayer(withColor color: UIColor) -> CAShapeLayer {
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 10, height: 10)).cgPath
        circle.fillColor = color.cgColor
        return circle
    }
}
