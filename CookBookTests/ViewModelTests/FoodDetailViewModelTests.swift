//
//  FoodDetailViewModelTests.swift
//  CookBookTests
//
//  Created by Robert Szasz on 23/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm
import RxTest
import RxBlocking

@testable import CookBook
 
class FoodDetailViewModelTests: XCTestCase {
    var disposeBag: DisposeBag!
    
    private var p1: FoodPropertyList = (-1, "TestName", "TestOverview", "TestRecipe", "TestPrepTime", ["Test1", "Test2", "Test3"], FoodKind.MainDish, DifficultyLevel.Middle)
    
    private let foodRealm: Realm = TestFoodRealmProvider.itemsOnDisk.realm

    private func createViewModel() throws -> FoodDetailViewModel {
        let food = FoodItem(properties: p1)
        try foodRealm.write {
            foodRealm.add(food)
        }
        return FoodDetailViewModel(coordinator: SceneCoordinator(window: UIWindow()), foodService: FoodService(TestFoodRealmProvider.self), food: food)
    }

    private func changeTestObject(withViewModel viewModel: FoodDetailViewModel) {
        
        let ffpl1: FullFoodPropertyList = (nil, "New", "NewOverview", "NewRecipe", "NewPreptime", ["New", "New", "New"], photo: "", isVegetarian: false, isVegan: true, isLactosefree: false, isGlutenfree: false, isSugarfree: true, FoodKind.Dessert, DifficultyLevel.Easy)
        let ffpl2: FullFoodPropertyList = (nil, "New2", "NewOverview2", "NewRecipe2", "NewPreptime2", ["New2", "New2"], photo: "", isVegetarian: false, isVegan: false, isLactosefree: true, isGlutenfree: false, isSugarfree: false, FoodKind.Salad, DifficultyLevel.High)
        
        createFoodUpdatingDispatchingBlock(forTime: .now() + 0.5, withViewModel: viewModel, fullPropertyList: ffpl1)
        createFoodUpdatingDispatchingBlock(forTime: .now() + 0.8, withViewModel: viewModel, fullPropertyList: ffpl2)
    }
    
    private func createFoodUpdatingDispatchingBlock(forTime time: DispatchTime, withViewModel viewModel: FoodDetailViewModel, fullPropertyList ffpl: FullFoodPropertyList) {
        DispatchQueue.main.asyncAfter(deadline: time) {
            let food = self.foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!
            viewModel.foodService.update(food: food, properties: ffpl)
        }
    }
    
    override func tearDown() {
        guard let food = foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1) else { return }
        do {
            try foodRealm.write {
                foodRealm.delete(food)
            }
        } catch {
            fatalError()
        }
        
        super.tearDown()
    }
    
    // MARK: - Tests
    func testNameOutput() throws {
        var viewModel = try createViewModel()
        let result = viewModel.name
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), ["TestName","New","New2"])
    }
    
    func testOverviewOutput() throws {
        var viewModel = try createViewModel()
        let result = viewModel.overview
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), ["TestOverview","NewOverview","NewOverview2"])
    }
    
    func testRecipeOutput() throws {
        var viewModel = try createViewModel()
        let result = viewModel.recipe
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), ["TestRecipe","NewRecipe","NewRecipe2"])
    }
    
    func testPreptimeOutput() throws {
        var viewModel = try createViewModel()
        let result = viewModel.prepTime
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), ["TestPrepTime","NewPreptime","NewPreptime2"])
    }
    
    func testIngredientsOutput() throws {
        var viewModel = try createViewModel()
        let result = viewModel.ingredients
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), [["Test1", "Test2", "Test3"],
                                                                   ["New", "New", "New"],
                                                                   ["New2", "New2"]])
    }
    
    func testEnumFoodKindOutput() throws {
        var viewModel = try createViewModel()
        let _ = viewModel.foodKindObserver // --> must initialize because lazy var
        let result = viewModel.foodKind.asObservable()
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), [.MainDish, .Dessert, .Salad])
    }
    
    func testDifficultyLevelOutput() throws {
        var viewModel = try createViewModel()
        let result = viewModel.difficulty
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), [.Middle, .Easy, .High])
    }
    
    func testFoodRestrictionOutput() throws {
        var viewModel = try createViewModel()
        let result = viewModel.foodRestriction
        changeTestObject(withViewModel: viewModel)
        XCTAssertEqual(try result.take(3).toBlocking().toArray(), [[],
                                                                   ["isVegan", "isSugarfree"],
                                                                   ["isLactosefree"]])
    }
    
}
