//
//  DeliveryListCell.swift
//  CookBook
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RealmSwift
import RxCocoa

class DeliveryListCell: UITableViewCell {
    
    private var bag = DisposeBag()
    private lazy var formatter = DateFormatter().then { $0.dateFormat = "dd/MM/yy" }
    private let calendar = Calendar.current
    
    // MARK: - View properties

    let dateLabel = UILabel().then {
        $0.textColor = .white
        $0.font = UIFont.systemFont(ofSize: 18, weight: .black)
        $0.setContentCompressionResistancePriority(UILayoutPriority.defaultHigh, for: .horizontal)
        $0.backgroundColor = #colorLiteral(red: 1, green: 0.1764705882, blue: 0.3333333333, alpha: 1)
        $0.layer.cornerRadius = 3
        $0.layer.masksToBounds = true
    }
    
    let personLabel = UILabel().then {
        $0.textColor = .white
        $0.font = UIFont.systemFont(ofSize: 18, weight: .black)
        $0.setContentCompressionResistancePriority(UILayoutPriority.defaultHigh, for: .horizontal)
        $0.backgroundColor = #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1)
        $0.layer.cornerRadius = 3
        $0.layer.masksToBounds = true
    }
    
    let foodLabel = UILabel().then {
        $0.textColor = .darkGray
        $0.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        $0.setContentCompressionResistancePriority(UILayoutPriority.defaultLow, for: .horizontal)
        $0.numberOfLines = 0
    }
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        addLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        bag = DisposeBag()
    }
    
    // MARK: - Set up view
    
    private func addSubviews() {
        contentView.addSubview(dateLabel)
        contentView.addSubview(personLabel)
        contentView.addSubview(foodLabel)
    }
    
    private func addLayout() {
        dateLabel.anchor(top: nil, left: contentView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        dateLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        personLabel.anchor(top: nil, left: dateLabel.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        personLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        foodLabel.anchor(top: contentView.topAnchor, left: personLabel.rightAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 8, paddingLeft: 12, paddingBottom: -8, paddingRight: -12, width: 0, height: 0)
        foodLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
    
    // MARK: - Helper methods

    func configure(with item: Delivery) {
        
        item.rx.observe(Date.self, "date")
            .asDriver(onErrorJustReturn: Date(timeIntervalSince1970: 0))
            .filter { $0 != nil }
            .map { $0! }
            .map { [weak self] in self?.formatter.string(from: $0) }
            .drive(dateLabel.rx.text)
            .disposed(by: bag)
        
        item.rx.observe(String.self, "toPerson")
            .asDriver(onErrorJustReturn: NSLocalizedString("Unknown", comment: "On error just return"))
            .drive(personLabel.rx.text)
            .disposed(by: bag)
        
        var text = ""
        for food in item.menu.toArray() {
            text += food + "\n"
        }
        foodLabel.text = String(text.dropLast(1))
    }

}
