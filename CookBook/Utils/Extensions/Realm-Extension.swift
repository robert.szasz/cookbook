//
//  Realm-Extension.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxRealm

extension Realm {
    
    enum PopulatingError: Swift.Error, LocalizedError {
        case invalidURL(URL)
        case decodingFailed
        
        public var errorDescription: String? {
              switch self {
              case .invalidURL(let url): return NSLocalizedString("Couldn't get data from the given URL: \(url).", comment: "invalidURL")
              case .decodingFailed: return NSLocalizedString("The decoding failed.", comment: "decodingFailed")
              }
          }
    }
          
    private static func createObjectsForRealm(withJSONDataAtUrl url: URL) -> Maybe<[FoodItem]> {
        return Maybe<[FoodItem]>.create { maybe in
            
            guard let jsonData = try? Data(contentsOf: url) else {
                maybe(.error(PopulatingError.invalidURL(url)))
                return Disposables.create {}
            }
            
            let jsonDecoder = JSONDecoder()
            
            guard let foodArray = try? jsonDecoder.decode([FoodItem].self, from: jsonData) else {
                maybe(.error(PopulatingError.decodingFailed))
                return Disposables.create {}
            }
            
            maybe(.success(foodArray))
            return Disposables.create {}
        }
    }
    
    @discardableResult
    static func populateRealm(withFilesAtUrl url: URL) -> Completable {
        let subject = PublishSubject<Void>()
        
        guard FoodRealmProvider.itemsOnDisk.realm.objects(FoodItem.self).isEmpty else {
            subject.onCompleted()
            return subject.asObservable()
                       .take(1)
                       .ignoreElements()
        }
        
        Realm.createObjectsForRealm(withJSONDataAtUrl: url)
            .asObservable()
            .do(afterCompleted: {
                subject.onCompleted()
            })
            .subscribe(FoodRealmProvider.itemsOnDisk.realm.rx.add(update: .modified, onError: { elements, error in
                if let _ = elements {
                    subject.onError(error)
                } else {
                  print("Error \(error.localizedDescription) while opening realm.")
                }
            }))
        
        return subject.asObservable()
            .take(1)
            .ignoreElements()
    }

    
}
