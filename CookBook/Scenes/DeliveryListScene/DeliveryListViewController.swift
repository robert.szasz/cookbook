//
//  DeliveryListViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Action
import RxDataSources
import Then

fileprivate let reuseIdentifier = "DeliveryCell"

class DeliveryListViewController: UIViewController, BindableType {

    var viewModel: DeliveryListViewModel!
    var dataSource: RxTableViewSectionedAnimatedDataSource<DeliverySection>!
    let bag = DisposeBag()
    
    // MARK: - View properties
    
    var personButton = UIButton(type: .custom).then {
        $0.setImage(UIImage(named: "user_icon")!.withRenderingMode(.alwaysTemplate), for: .normal)
        $0.tintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.imageView?.contentMode = .scaleAspectFit
        $0.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        $0.layer.cornerRadius = 25
        $0.layer.backgroundColor = UIColor.white.cgColor
    }
    
    var calendarButton = UIButton(type: .custom).then {
        $0.setImage(UIImage(named: "calendar_icon")!.withRenderingMode(.alwaysTemplate), for: .normal)
        $0.tintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.imageView?.contentMode = .scaleAspectFit
        $0.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        $0.layer.cornerRadius = 25
        $0.layer.backgroundColor = UIColor.white.cgColor
    }
    
    let buttonContainer = UIView().then {
        $0.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.cornerRadius = 35
        $0.clipsToBounds = true
        $0.layer.shadowColor = UIColor.black.cgColor
        $0.layer.shadowOpacity = 0.5
        $0.layer.shadowOffset = .zero
        $0.layer.shadowRadius = 5
        $0.layer.shadowPath = UIBezierPath(roundedRect: $0.bounds, cornerRadius: 30).cgPath
    }
    
    let tableView = UITableView().then {
        $0.backgroundColor = .white
        $0.separatorStyle = .none
        $0.allowsSelection = false
        $0.rowHeight = UITableView.automaticDimension
        $0.estimatedRowHeight = 200
        $0.register(DeliveryListCell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    let personFilterLabel = UILabel().then {
        $0.numberOfLines = 0
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .bold)
        $0.textAlignment = .center
    }
    
    var cancelPersonFilterButton = UIButton(type: .custom).then {
        $0.setTitle("x", for: .normal)
        $0.titleLabel?.textAlignment = .center
        $0.titleLabel?.font = .systemFont(ofSize: 20, weight: .heavy)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0.3), for: .highlighted)
        $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.borderWidth = 2
        $0.isHidden = true
    }
    
    let dateFilterLabel = UILabel().then {
        $0.numberOfLines = 0
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 10, weight: .bold)
        $0.textAlignment = .center
    }
    
    var cancelDateFilterButton = UIButton(type: .custom).then {
        $0.setTitle("x", for: .normal)
        $0.titleLabel?.textAlignment = .center
        $0.titleLabel?.font = .systemFont(ofSize: 20, weight: .heavy)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0.3), for: .highlighted)
        $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.borderWidth = 2
        $0.isHidden = true
    }
    
    lazy var contactAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        
        let names = getPersonListArrayFromUserDefaults()
        let contactPopover = PersonListViewController(names: names)
        contactPopover.modalPresentationStyle = .popover
        contactPopover.preferredContentSize = CGSize(width: 200, height: names.count * 30 + 15)
        
        let contactObservable = contactPopover.selectedName
            .asObservable()
            .map { self.viewModel.personFilter.value + [$0] }
            .share()
            
        contactObservable
            .bind(to: self.viewModel.personFilter)
            .disposed(by: self.bag)
        
        contactObservable
            .filter { !$0.isEmpty }
            .map { $0.reduce("") { $0 + $1 + ", " } }
            .map { String($0.dropLast(2)) }
            .bind(to: self.personFilterLabel.rx.text)
            .disposed(by: self.bag)
        
        contactObservable
            .map { _ in false }
            .bind(to: self.cancelPersonFilterButton.rx.isHidden)
            .disposed(by: self.bag)
        
        if let popoverPresentationController = contactPopover.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = self.personButton.frame
            popoverPresentationController.delegate = self
            self.present(contactPopover, animated: true, completion: nil)
        }
        return .just(())
    }
    
    lazy var filterAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        
        let formatter = DateFormatter().then { $0.dateFormat = "dd/MM/yy" }
        
        let datePopover = CalendarViewController(singleDayPick: false)
        datePopover.modalPresentationStyle = .popover
        datePopover.preferredContentSize = CGSize(width: 280, height: 350)
        
        let selectedDate = datePopover.calendar.choosenInterval
            .asObservable()
            .share()
            
        selectedDate
            .bind(to: self.viewModel.calendarFilter)
            .disposed(by: self.bag)
        
        selectedDate
            .map { "\(formatter.string(from: $0.start))\n-\n\(formatter.string(from: $0.end))" }
            .bind(to: self.dateFilterLabel.rx.text)
            .disposed(by: self.bag)
        
        selectedDate
            .map { _ in false }
            .bind(to: self.cancelDateFilterButton.rx.isHidden)
            .disposed(by: self.bag)
        
        if let popoverPresentationController = datePopover.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = self.calendarButton.frame
            popoverPresentationController.delegate = self
            self.present(datePopover, animated: true, completion: nil)
        }
        return .just(())
    }
    
    lazy var cancelPersonFilterAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        
        self.personFilterLabel.text = ""
        self.viewModel.personFilter.accept([])
        self.cancelPersonFilterButton.isHidden = true
        
        return .just(())
    }
    
    lazy var cancelDateFilterAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        
        self.dateFilterLabel.text = ""
        self.viewModel.calendarFilter.accept(nil)
        self.cancelDateFilterButton.isHidden = true
        
        return .just(())
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setUpView()
        
        configureDataSource()
    }
    
    // MARK: - Subviews & Layout
    
    private func setUpView() {
        addSubviews()
        addLayout()
        addActions()
        
        setUpNavBar()
    }
    
    private func addSubviews() {
        view.addSubview(buttonContainer)
        view.addSubview(personButton)
        view.addSubview(calendarButton)
        view.addSubview(tableView)
        view.addSubview(personFilterLabel)
        view.addSubview(cancelPersonFilterButton)
        view.addSubview(dateFilterLabel)
        view.addSubview(cancelDateFilterButton)
    }
    
    private func addLayout() {
        buttonContainer.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 2 * 50 + 10 + 20, height: 70)
        buttonContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        personButton.anchor(top: buttonContainer.topAnchor, left: buttonContainer.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        calendarButton.anchor(top: personButton.topAnchor, left: personButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        tableView.anchor(top: buttonContainer.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        cancelPersonFilterButton.anchor(top: nil, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        cancelPersonFilterButton.centerYAnchor.constraint(equalTo: buttonContainer.centerYAnchor).isActive = true
        cancelPersonFilterButton.roundCorners()
        
        personFilterLabel.anchor(top: nil, left: cancelPersonFilterButton.rightAnchor, bottom: nil, right: buttonContainer.leftAnchor, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: -8, width: 0, height: 0)
        personFilterLabel.centerYAnchor.constraint(equalTo: buttonContainer.centerYAnchor).isActive = true
        
        dateFilterLabel.anchor(top: nil, left: buttonContainer.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        dateFilterLabel.centerYAnchor.constraint(equalTo: buttonContainer.centerYAnchor).isActive = true
        
        cancelDateFilterButton.anchor(top: nil, left: dateFilterLabel.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        cancelDateFilterButton.centerYAnchor.constraint(equalTo: dateFilterLabel.centerYAnchor).isActive = true
        cancelDateFilterButton.roundCorners()
    }
    
    private func addActions() {
        calendarButton.rx.action = filterAction
        cancelDateFilterButton.rx.action = cancelDateFilterAction
        personButton.rx.action = contactAction
        cancelPersonFilterButton.rx.action = cancelPersonFilterAction
    }

    // MARK: - Bind ViewModel

    func bindViewModel() {
        viewModel.sectionedItems
            .map { [$0] }
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        navigationItem.rightBarButtonItem?.rx.action = viewModel.onCreateDelivery()
    }
    
    // MARK: - Helper methods
 
    private func configureDataSource() {
        dataSource = RxTableViewSectionedAnimatedDataSource<DeliverySection>(animationConfiguration: AnimationConfiguration(insertAnimation: .automatic, reloadAnimation: .automatic, deleteAnimation: .left), configureCell: { dataSource, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! DeliveryListCell
            cell.configure(with: item)
            return cell
        }, titleForHeaderInSection: { dataSource, index in
            dataSource.sectionModels[index].model
        }, canEditRowAtIndexPath: { _, _ in
            true
        })
    }
    
    private func setUpNavBar() {
        navigationItem.title = NSLocalizedString("Deliveries", comment: "Deliveries scene")
        navigationController?.navigationBar.prefersLargeTitles = true
        
        if #available(iOS 13.0, *) {
            let navigationBar = navigationController?.navigationBar
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
            appearance.shadowColor = nil
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.titleTextAttributes = [.foregroundColor : UIColor.white]
            let btnAppearance = UIBarButtonItemAppearance(style: .plain)
            btnAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.white]
            appearance.buttonAppearance = btnAppearance
            UINavigationBar.appearance().tintColor = .white
            
            navigationBar?.standardAppearance = appearance
            navigationBar?.scrollEdgeAppearance = appearance
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus.app.fill"), style: .plain, target: nil, action: nil)
            return
        }
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
}

extension DeliveryListViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
