//
//  FoodServiceTest.swift
//  CookBookTests
//
//  Created by Robert Szasz on 17/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm
import RxTest
import RxBlocking

@testable import CookBook

class FoodServiceTest: XCTestCase {
    var foodService: FoodService!
    var foodPropertyList: FoodPropertyList!
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    var subscription: Disposable!
    
    private let foodRealm: Realm = TestFoodRealmProvider.itemsOnDisk.realm
    
    override func setUp() {
        super.setUp()
        
        foodService = FoodService(TestFoodRealmProvider.self)
        foodPropertyList = (-1, "TestName", "TestOverview", "TestRecipe", "TestPrepTime", ["Test1", "Test2", "Test3"], FoodKind.MainDish, DifficultyLevel.Middle)
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
        
        try! foodRealm.write {
            foodRealm.deleteAll()
        }
        
    }

    override func tearDown() {
        try! foodRealm.write {
            foodRealm.deleteAll()
        }
        
        super.tearDown()
    }

    func testFoodIsAddedToRealm() throws {
        
        let scheduler = ConcurrentDispatchQueueScheduler(qos: .default)
        
        let foodObservable = foodService.createFood(properties: foodPropertyList).subscribeOn(scheduler)
        
        XCTAssertEqual(try foodObservable.toBlocking(timeout: 1.0).last()?.uid, -1)
    }
    
    func testFoodIsDeletedFromRealm() throws {
        
        // Add test object to Realm
        let food = FoodItem(properties: foodPropertyList)
        try foodRealm.write {
            foodRealm.add(food)
        }
        
        let scheduler = ConcurrentDispatchQueueScheduler(qos: .default)
        
        let foodObservable = foodService.delete(food: food).subscribeOn(scheduler)
        
        let result = foodObservable
            .toBlocking()
            .materialize()
                
        switch result {
        case .completed(let elements):
            XCTAssert(elements.count >= 0)
        case .failed(_, let error):
            XCTFail("Expected result to complete without error, but received \(error).")
        }
    }
    
    func testFoodIsUpdatedInRealm() throws {
        
        // Add test object to Realm
        let food = FoodItem(properties: foodPropertyList)
        try foodRealm.write {
            foodRealm.add(food)
        }
        
        // Test against initial properties
        XCTAssertEqual(food.name, "TestName")
        XCTAssertEqual(food.recipe, "TestRecipe")
        XCTAssertEqual(food.prepTime, "TestPrepTime")
        XCTAssertEqual(food.ingredients.count, 3)
        XCTAssertEqual(food.enumFoodKind, .MainDish)
        XCTAssertEqual(food.enumDifficultyLevel, .Middle)
        
        let scheduler = ConcurrentDispatchQueueScheduler(qos: .default)
        
        let foodObservable = foodService.update(food: food, properties: (nil, "TestName1", "TestOverview1", "TestRecipe1", "TestPrepTime1", ["Test1", "Test2", "Test3", "Test4"], FoodKind.Soup, DifficultyLevel.Easy)).observeOn(scheduler)
        
        let result = foodObservable
            .toBlocking()
            .materialize()
        
        switch result {
        case .completed(let food):
            XCTAssertEqual(food.count, 1)
            XCTAssertEqual(food[0].name, "TestName1")
            XCTAssertEqual(food[0].recipe, "TestRecipe1")
            XCTAssertEqual(food[0].prepTime, "TestPrepTime1")
            XCTAssertEqual(food[0].ingredients.count, 4)
            XCTAssertEqual(food[0].ingredients.last, "Test4")
            XCTAssertEqual(food[0].enumFoodKind, .Soup)
            XCTAssertEqual(food[0].enumDifficultyLevel, .Easy)
        case .failed(_, let error):
            XCTFail("Expected result to complete without error, but received \(error).")
        }
    }
    
    func testFoodListIsReturned() {
        // create a few test tasks
        var properties2 = foodPropertyList!
        properties2.uid = -2
        var properties3 = foodPropertyList!
        properties3.uid = -3

        let propertiesArray: [FoodPropertyList] = [foodPropertyList, properties2, properties3]
        
        propertiesArray.forEach { foodService.createFood(properties: $0) }
        
        let testObs = scheduler.createObserver(Results<FoodItem>.self)
        
        foodService.foods().bind(to: testObs).disposed(by: disposeBag)
        
        scheduler.start()
        
        let results = testObs.events.count
        
        XCTAssertEqual(results, 1)
    }
}
