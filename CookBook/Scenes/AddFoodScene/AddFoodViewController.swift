//
//  AddFoodViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 21/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Action
import Then

class AddFoodViewController: UIViewController, BindableType, ConfigurableType, UIAdaptivePresentationControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var viewModel: AddFoodViewModel!
    private var bag = DisposeBag()
    
    // MARK: - View properties
    lazy var photo = UIImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
        $0.backgroundColor = .darkGray
        $0.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        $0.addGestureRecognizer(tapGesture)
        tapGesture.rx.event
            .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .bind(onNext: { [weak self] _ in self?.photoViewTapped() })
            .disposed(by: bag)
    }
    let imageNotifier = PublishSubject<String>()
    
    let nameField = EntryFieldView(entryfieldName: NSLocalizedString("Name", comment: "Food name"))
    let overviewField = EntryFieldView(entryfieldName: NSLocalizedString("Overview", comment: "Food Detail View overview button"))
    let kindPicker = UISegmentedControl(items: ["🍜", "🥬", "🥩", "🍰"]).then {
        $0.selectedSegmentIndex = 2
        if #available(iOS 13.0, *) { $0.selectedSegmentTintColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) }
    }
    let recipeView = EntryFieldView(entryTextviewName: NSLocalizedString("Recipe", comment: "Food Detail View recipe button"))
    
    let preptimeField = EntryFieldView(entryfieldName: NSLocalizedString("Prep Time", comment: "Preparation time"))
    
    let difficultyTitle = UILabel().then {
        $0.text = NSLocalizedString("Difficulty", comment: "Difficulty")
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .heavy)
    }
    
    let difficultyPicker = DifficultySelectorView(initialValue: 1)
    
    let restrictionTitle = UILabel().then {
        $0.text = NSLocalizedString("Restrictions", comment: "Restrictions")
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .heavy)
    }
    
    var isVegetarianButton = UIButton.foodRestrictionButton(ofType: "isVegetarian", withTag: 1)
    var isVeganButton = UIButton.foodRestrictionButton(ofType: "isVegan", withTag: 2)
    var isLactosefreeButton = UIButton.foodRestrictionButton(ofType: "isLactosefree", withTag: 3)
    var isGlutenfreeButton = UIButton.foodRestrictionButton(ofType: "isGlutenfree", withTag: 4)
    var isSugarfreeButton = UIButton.foodRestrictionButton(ofType: "isSugarfree", withTag: 5)
    var restrictionArr = BehaviorRelay<[String]>(value: [])
    
    lazy var restrictionAction = Action<UIButton, Void> { [weak self] button in
        guard let self = self else { return .just(()) }
        let object = PublishSubject<Void>()
        
        if button.layer.borderColor?.alpha == 1 {
            UIView.animate(withDuration: 1, animations: {
                button.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0).cgColor
            }) { _ in
                var oldArray = self.restrictionArr.value
                oldArray.remove(at: oldArray.firstIndex(of: self.getStringForButtonTag(button.tag))!)
                self.restrictionArr.accept(oldArray)
                object.onCompleted()
            }
        } else {
            UIView.animate(withDuration: 1, animations: {
                button.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(1).cgColor
            }) { _ in
                var oldArray = self.restrictionArr.value
                oldArray.append(self.getStringForButtonTag(button.tag))
                self.restrictionArr.accept(oldArray)
                object.onCompleted()
            }
        }
        
        return object.asObservable()
    }
    
    let ingredientsView = IngredientsListView()

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        isVegetarianButton.rx.bind(to: restrictionAction, input: isVegetarianButton)
        isVeganButton.rx.bind(to: restrictionAction, input: isVeganButton)
        isLactosefreeButton.rx.bind(to: restrictionAction, input: isLactosefreeButton)
        isGlutenfreeButton.rx.bind(to: restrictionAction, input: isGlutenfreeButton)
        isSugarfreeButton.rx.bind(to: restrictionAction, input: isSugarfreeButton)
        
        imageNotifier.asObservable()
            .map { imageFromFileName($0) }
            .bind(to: photo.rx.image)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addSubviews()
        addLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        ingredientsView.ingredientsViews.forEach { $0.layoutIfNeeded() }
        ingredientsView.ingredientsViews.forEach { $0.updateLayout() }
    }
    
    // MARK: - Subviews & Layout
    
    private func addSubviews() {
        view.addSubview(photo)
        view.addSubview(nameField)
        view.addSubview(overviewField)
        view.addSubview(kindPicker)
        view.addSubview(recipeView)
        view.addSubview(preptimeField)
        view.addSubview(difficultyTitle)
        view.addSubview(difficultyPicker)
        view.addSubview(restrictionTitle)
        view.addSubview(isVegetarianButton)
        view.addSubview(isVeganButton)
        view.addSubview(isLactosefreeButton)
        view.addSubview(isGlutenfreeButton)
        view.addSubview(isSugarfreeButton)
        view.addSubview(ingredientsView)

    }
    
    private func addLayout() {
        photo.anchor(top: nil, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 120, height: 120)
        
        nameField.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: photo.rightAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        overviewField.anchor(top: nameField.bottomAnchor, left: nameField.leftAnchor, bottom: nil, right: nameField.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        photo.bottomAnchor.constraint(equalTo: overviewField.textfield.bottomAnchor).isActive = true
        
        kindPicker.anchor(top: overviewField.bottomAnchor, left: photo.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: -20, width: 0, height: 40)
        
        recipeView.anchor(top: kindPicker.bottomAnchor, left: overviewField.leftAnchor, bottom: nil, right: overviewField.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: -10, width: 0, height: 200)
        
        preptimeField.anchor(top: recipeView.bottomAnchor, left: recipeView.leftAnchor, bottom: nil, right: nameField.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        difficultyTitle.anchor(top: preptimeField.bottomAnchor, left: preptimeField.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        difficultyPicker.anchor(top: difficultyTitle.bottomAnchor, left: preptimeField.leftAnchor, bottom: nil, right: preptimeField.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: -10, width: 0, height: 30)
        difficultyPicker.subviews.first?.layoutIfNeeded()
        
        restrictionTitle.anchor(top: difficultyPicker.bottomAnchor, left: difficultyPicker.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        let buttonSizing = makeEstimateSize(difficultyPicker.subviews.first!.frame.width)

        isVegetarianButton.anchor(top: restrictionTitle.bottomAnchor, left: difficultyPicker.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: buttonSizing.padding, paddingBottom: 0, paddingRight: 0, width: buttonSizing.buttonWidth, height: buttonSizing.buttonWidth)
        isVegetarianButton.layer.cornerRadius = buttonSizing.buttonWidth / 2
        
        isVeganButton.anchor(top: isVegetarianButton.topAnchor, left: isVegetarianButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: buttonSizing.padding, paddingBottom: 0, paddingRight: 0, width: buttonSizing.buttonWidth, height: buttonSizing.buttonWidth)
        isVeganButton.layer.cornerRadius = buttonSizing.buttonWidth / 2

        isLactosefreeButton.anchor(top: isVegetarianButton.topAnchor, left: isVeganButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: buttonSizing.padding, paddingBottom: 0, paddingRight: 0, width: buttonSizing.buttonWidth, height: buttonSizing.buttonWidth)
        isLactosefreeButton.layer.cornerRadius = buttonSizing.buttonWidth / 2

        isGlutenfreeButton.anchor(top: isVegetarianButton.topAnchor, left: isLactosefreeButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: buttonSizing.padding, paddingBottom: 0, paddingRight: 0, width: buttonSizing.buttonWidth, height: buttonSizing.buttonWidth)
        isGlutenfreeButton.layer.cornerRadius = buttonSizing.buttonWidth / 2

        isSugarfreeButton.anchor(top: isVegetarianButton.topAnchor, left: isGlutenfreeButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: buttonSizing.padding, paddingBottom: 0, paddingRight: 0, width: buttonSizing.buttonWidth, height: buttonSizing.buttonWidth)
        isSugarfreeButton.layer.cornerRadius = buttonSizing.buttonWidth / 2
        
        ingredientsView.anchor(top: recipeView.topAnchor, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: recipeView.leftAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: -10, paddingRight: -10, width: 0, height: 0)
    }
    
    // MARK: - Bind ViewModel
    
    func bindViewModel() {
        
        viewModel.photo.asObservable()
            .take(1)
            .flatMap { [unowned self] photoName -> Observable<UIImage> in
                if photoName == "" { return .just(getDefaultImageForFood(self.viewModel.foodKind.value.rawValue)) }
                else { return .just(imageFromFileName(photoName)) }
            }
            .bind(to: photo.rx.image)
            .disposed(by: bag)
        
        imageNotifier.asObservable()
            .bind(to: viewModel.photo)
            .disposed(by: bag)
        
        viewModel.name.asObservable()
            .take(1)
            .filter { $0.trimmingCharacters(in: .whitespacesAndNewlines) != "" }
            .bind(to: nameField.textfield.rx.text)
            .disposed(by: bag)
        
        nameField.textfield.rx.controlEvent([.editingChanged]).asObservable()
            .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .map { [weak self] _ in self?.nameField.textfield.text }
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: viewModel.name)
            .disposed(by: bag)
        
        viewModel.overview.asObservable()
            .take(1)
            .filter { $0.trimmingCharacters(in: .whitespacesAndNewlines) != "" }
            .bind(to: overviewField.textfield.rx.text)
            .disposed(by: bag)
        
        overviewField.textfield.rx.controlEvent([.editingChanged]).asObservable()
            .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .map { [weak self] _ in self?.overviewField.textfield.text }
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: viewModel.overview)
            .disposed(by: bag)
        
        viewModel.foodKind.asObservable()
            .take(1)
            .map { $0.rawValue - 1 }
            .bind(to: kindPicker.rx.selectedSegmentIndex)
            .disposed(by: bag)
        
        kindPicker.rx.selectedSegmentIndex.asObservable()
            .map { FoodKind(rawValue: $0 + 1)! }
            .bind(to: viewModel.foodKind)
            .disposed(by: bag)
        
        kindPicker.rx.selectedSegmentIndex.asObservable()
            .filter { [weak self] _ in self?.viewModel.photo.value == "" }
            .map { $0 + 1 }
            .map { [weak self] in getDefaultImageForFood($0) }
            .bind(to: photo.rx.image)
            .disposed(by: bag)
        
        viewModel.recipe.asObservable()
            .take(1)
            .filter { $0.trimmingCharacters(in: .whitespacesAndNewlines) != "" }
            .bind(to: recipeView.textview.rx.text)
            .disposed(by: bag)
        
        recipeView.textview.rx.didEndEditing.asObservable()
            .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .map { [weak self] _ in self!.recipeView.textview.text }
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: viewModel.recipe)
            .disposed(by: bag)
        
        viewModel.ingredients.asObservable()
            .take(1)
            .bind(to: ingredientsView.ingredientsArray)
            .disposed(by: bag)
        
        ingredientsView.ingredientsArray.asObservable()
            .bind(to: viewModel.ingredients)
            .disposed(by: bag)
        
        viewModel.prepTime.asObservable()
            .take(1)
            .filter { $0.trimmingCharacters(in: .whitespacesAndNewlines) != "" }
            .bind(to: preptimeField.textfield.rx.text)
            .disposed(by: bag)
        
        preptimeField.textfield.rx.controlEvent([.editingChanged]).asObservable()
            .throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .map { [weak self] _ in self?.preptimeField.textfield.text }
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: viewModel.prepTime)
            .disposed(by: bag)
        
        viewModel.difficulty.asObservable()
            .take(1)
            .map { $0.rawValue }
            .bind(to: difficultyPicker.selectedButton)
            .disposed(by: bag)
        
        difficultyPicker.selectedButton.asObservable()
            .skip(1)
            .map { DifficultyLevel(rawValue: $0)! }
            .bind(to: viewModel.difficulty)
            .disposed(by: bag)
        
        viewModel.restrictions.asObservable()
            .take(1)
            .do(onNext: { [weak self] in if $0.contains("isVegetarian") { self?.isVegetarianButton.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(1).cgColor } })
            .do(onNext: { [weak self] in if $0.contains("isVegan") { self?.isVeganButton.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(1).cgColor } })
            .do(onNext: { [weak self] in if $0.contains("isLactosefree") { self?.isLactosefreeButton.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(1).cgColor } })
            .do(onNext: { [weak self] in if $0.contains("isGlutenfree") { self?.isGlutenfreeButton.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(1).cgColor } })
            .do(onNext: { [weak self] in if $0.contains("isSugarfree") { self?.isSugarfreeButton.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(1).cgColor } })
            .bind(to: restrictionArr)
            .disposed(by: bag)
        
        restrictionArr.asObservable()
            .skip(1)
            .bind(to: viewModel.restrictions)
            .disposed(by: bag)

    }
    
    // MARK: - Configurable Type
    
    func configureSelfAfterPushedOnNavStack() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: "Nav bar back button"), style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: viewModel.foodToEdit == nil ? NSLocalizedString("Save", comment: "Nav bar save button") : NSLocalizedString("Done", comment: "Nav bar done button"), style: .done, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .black
        
        navigationItem.leftBarButtonItem!.rx.action = CocoaAction { [unowned self] _ in
            return self.viewModel.sceneCoordinator
                .pop(animated: true)
                .asObservable()
                .map { _ in }
        }
        
        navigationItem.rightBarButtonItem!.rx.action = viewModel.foodToEdit == nil ? viewModel.onSaveTask() : viewModel.onEditTask()
        
        if #available(iOS 13.0, *) {
            navigationController?.presentationController?.rx.didDismissController
                .subscribe(onNext: { [weak self] _ in
                    guard let self = self else { return }
                    self.viewModel.sceneCoordinator
                        .swipeDownDismiss(self)
                    })
                .disposed(by: bag)
        }
    }
    
    // MARK: - Helper methods
    
    private func photoViewTapped() {
        guard UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) else { return }
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        
        present(imagePicker, animated: true, completion: nil)
    }
        
    private func makeEstimateSize(_ width: CGFloat) -> (padding: CGFloat, buttonWidth: CGFloat) {
        let minPadding: CGFloat = 10
        let remainingSpace = width - 6 * minPadding
        let estimatedButtonWidth = CGFloat(Int(Int(remainingSpace / 5) / 5) * 5)
        
        return (padding: minPadding, buttonWidth: estimatedButtonWidth)
    }
    
    private func getStringForButtonTag(_ tag: Int) -> String {
        let restrictionStrings = ["isVegetarian", "isVegan", "isLactosefree", "isGlutenfree", "isSugarfree"]
        return restrictionStrings[tag - 1]
    }
}

// MARK: - Image Picker Delegate

extension AddFoodViewController {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) { [weak self] in
            guard let url = info[.imageURL] as? URL, let image = info[.originalImage] as? UIImage else { return }
            
            let imageName = url.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!

            let documentDir = URL(fileURLWithPath: documentDirectory)
            let imageURL = documentDir.appendingPathComponent(imageName)
            
            try! image.jpegData(compressionQuality: 0.5)?.write(to: imageURL, options: .atomicWrite)
            self?.imageNotifier.onNext(imageName)
        }
    }
}

