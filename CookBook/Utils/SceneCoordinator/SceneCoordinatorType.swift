//
//  SceneCoordinatorType.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol SceneCoordinatorType: AnyObject {
    
    // transition to another scene
    @discardableResult
    func transition(to scene: Scene, type: SceneTransitionType) -> Maybe<UINavigationController>
    
    // pop scene from navigation stack or dismiss current modal
    @discardableResult
    func pop(animated: Bool) -> Completable
    
    // pop scene from navigation stack when modal is swiped down
    @discardableResult
    func swipeDownDismiss(_ modalVC: UIViewController) -> Completable
}

extension SceneCoordinatorType {
    @discardableResult
    func pop() -> Completable {
        return pop(animated: true)
    }
}


protocol Scene {
    func viewController() -> UIViewController
}

enum FoodScene: Scene {
    case foodList(FoodListViewModel)
    case addFood(AddFoodViewModel)
    case foodDetail(FoodDetailViewModel)
    
    func viewController() -> UIViewController {
        
        switch self {
        case .foodList(let viewModel):
          let nc = UINavigationController(rootViewController: FoodListViewController())
          var vc = nc.viewControllers.first as! FoodListViewController
          vc.bindViewModel(to: viewModel)
          return nc

        case .foodDetail(let viewModel):
            var vc = FoodDetailViewController()
            vc.bindViewModel(to: viewModel)
            return vc
        
        case .addFood(let viewModel):
            var vc = AddFoodViewController()
            vc.bindViewModel(to: viewModel)
            return vc
        }
    }
}

enum DeliveryScene: Scene {
    case deliveryList(DeliveryListViewModel)
    case addDelivery(AddDeliveryViewModel)
    case pickFood(PickFoodViewModel)
    
        func viewController() -> UIViewController {
        
        switch self {
        case .deliveryList(let viewModel):
          let nc = UINavigationController(rootViewController: DeliveryListViewController())
          var vc = nc.viewControllers.first as! DeliveryListViewController
          vc.bindViewModel(to: viewModel)
          return nc

        case .addDelivery(let viewModel):
            var vc = AddDeliveryViewController()
            vc.bindViewModel(to: viewModel)
            return vc
            
        case .pickFood(let viewModel):
            var vc = PickFoodViewController()
            vc.bindViewModel(to: viewModel)
            return vc
        }
    }
}

enum SceneTransitionType {
    case root       // make view controller the root view controller
    case push       // push view controller to navigation stack
    case modal      // present view controller modally
}

