//
//  FoodListViewModelTests.swift
//  CookBookTests
//
//  Created by Robert Szasz on 19/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm
import RxTest
import RxBlocking

@testable import CookBook

class FoodListViewModelTests: XCTestCase {
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    
    private var p1: FoodPropertyList!
    private var p2: FoodPropertyList!
    private var p3: FoodPropertyList!
    
    private let foodRealm: Realm = TestFoodRealmProvider.itemsOnDisk.realm

    private func createViewModel() -> FoodListViewModel {
        FoodListViewModel(coordinator: SceneCoordinator(window: UIWindow()), foodService: FoodService(TestFoodRealmProvider.self))
    }
    
    private func addTestObjectsToRealm(withFoodService foodService: FoodServiceType) -> Bool {
        p1 = (-1, "TestName", "TestOverview", "TestRecipe", "TestPrepTime", ["Test1", "Test2", "Test3"], FoodKind.MainDish, DifficultyLevel.Middle)
        p2 = (-2, "TestName2", "TestOverview2", "TestRecipe2", "TestPrepTime2", ["Test1", "Test2", "Test3"], FoodKind.Dessert, DifficultyLevel.Middle)
        p3 = (-3, "Testing", "TestOverview", "TestRecipe", "TestPrepTime", ["Test1", "Test2", "Test3"], FoodKind.MainDish, DifficultyLevel.Middle)
        
        let propertiesArray: [FoodPropertyList] = [p1, p2, p3]
        
        propertiesArray.forEach { foodService.createFood(properties: $0)
            .subscribe(onError: { _ in
                return false
            })
            .disposed(by: disposeBag)
        }
        return true
    }
    
    override func setUp() {
        super.setUp()
        
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }
    
    func test_whenInitiated_storesInitParams() {
        let viewModel = createViewModel()
        
        XCTAssertNotNil(viewModel.foodService)
        XCTAssertNotNil(viewModel.sceneCoordinator)
    }
    
    func test_sectionedItemsEmit_whenInitialized() throws {
        let viewModel = createViewModel()
        
        let result = viewModel.sectionedItems
        
        XCTAssertNotNil(try? result.toBlocking(timeout: 1.0).first())
    }
    
    func test_sectionedItemsEmit_whenFilteringApplied() {
        let viewModel = createViewModel()
        
        guard addTestObjectsToRealm(withFoodService: viewModel.foodService) else {
            XCTFail("Couldn't add test objects to Realm.")
            return
        }
        
        let filteredResults = scheduler.createObserver([FoodSection].self)
        
        viewModel.sectionedItems
            .bind(to: filteredResults)
            .disposed(by: disposeBag)
        
        scheduler.createColdObservable([.next(10, "test"),
                                        .next(20, "testn"),
                                        .next(30, "testi"),
                                        .next(40, "")])
            .bind(to: viewModel.searchBarInput)
            .disposed(by: disposeBag)
        
        scheduler.createColdObservable([.next(15, [4]),
                                        .next(25, [3, 4]),
                                        .next(45, [5])])
            .bind(to: viewModel.filterInput)
            .disposed(by: disposeBag)
        
        scheduler.start()
        
        XCTAssertEqual(filteredResults.events, [.next(0, [FoodSection(model: "All", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -3)!,
                                                                                            foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -2)!,
                                                                                            foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!])]),
                                                .next(10, [FoodSection(model: "All", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -3)!,
                                                                                             foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -2)!,
                                                                                             foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!])]),
                                                .next(15, [FoodSection(model: "Desserts", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -2)!])]),
                                                .next(20, [FoodSection(model: "Desserts", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -2)!])]),
                                                .next(25, [FoodSection(model: "Main Dishes & Desserts", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -2)!,
                                                                                                                foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!])]),
                                                .next(30, [FoodSection(model: "Main Dishes & Desserts", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -3)!])]),
                                                .next(40, [FoodSection(model: "Main Dishes & Desserts", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -3)!,
                                                                                                                foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -2)!,
                                                                                                                foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!])]),
                                                .next(45, [FoodSection(model: "All", items: [foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -3)!,
                                                                                             foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -2)!,
                                                                                             foodRealm.object(ofType: FoodItem.self, forPrimaryKey: -1)!])])
        ])

    }
    
    
    
    
    
}
