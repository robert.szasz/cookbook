//
//  DeliveryServiceType.swift
//  CookBook
//
//  Created by Robert Szasz on 29/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

enum DeliveryServiceError: Error {
  case creationFailed
  case updateFailed(Delivery)
  case deletionFailed(Delivery)
}

protocol DeliveryServiceType {
    @discardableResult
    func createDelivery(properties: DeliveryPropertyList) -> Observable<Delivery>

    @discardableResult
    func delete(delivery: Delivery) -> Observable<Void>
    
    // Still to decide
    //@discardableResult
    //func update(food: FoodItem, properties: FullFoodPropertyList) -> Observable<FoodItem>

    func deliveries() -> Observable<Results<Delivery>>
}
