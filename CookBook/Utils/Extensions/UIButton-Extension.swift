//
//  UIButton-Extension.swift
//  CookBook
//
//  Created by Robert Szasz on 26/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit

extension UIButton {
    
    static func foodRestrictionButton(ofType type: String, withTag tag: Int) -> Self {
        return self.init(type: .custom).then {
            $0.setImage(UIImage(named: type), for: .normal)
            $0.setImage(UIImage(named: type), for: .highlighted)
            $0.tag = tag
            $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0).cgColor
            $0.layer.borderWidth = 5
            $0.backgroundColor = .white
        }
    }
    
    func roundCorners() {
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
    }
    
}

