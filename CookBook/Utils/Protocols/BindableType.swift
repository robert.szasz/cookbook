//
//  BindableType.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift

protocol BindableType {
  associatedtype ViewModelType
  
  var viewModel: ViewModelType! { get set }

  func bindViewModel()
}

extension BindableType where Self: UIViewController {
  mutating func bindViewModel(to model: Self.ViewModelType) {
    viewModel = model
    loadViewIfNeeded()
    bindViewModel()
  }
}
