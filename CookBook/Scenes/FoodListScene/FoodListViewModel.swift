//
//  FoodListViewModel.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay
import RealmSwift
import RxDataSources
import RxCocoa
import Action
import RxRealm

typealias FoodSection = AnimatableSectionModel<String, FoodItem>

struct FoodListViewModel {
    let sceneCoordinator: SceneCoordinatorType
    let foodService: FoodServiceType
    
    private let bag = DisposeBag()
    
    // MARK: - Input
    let searchBarInput = BehaviorRelay<String>(value: "")
    let filterInput = BehaviorRelay<[Int]>(value: [5])
    
    init(coordinator: SceneCoordinatorType, foodService: FoodServiceType) {
        self.sceneCoordinator = coordinator
        self.foodService = foodService
    }
    
    // MARK: - Output
    var sectionedItems: Observable<[FoodSection]> {
        return Observable.combineLatest(Observable.collection(from: FoodRealmProvider.itemsOnDisk.realm.objects(FoodItem.self)), filterInput.asObservable(), searchBarInput.asObservable()) { (foods, filters, textFilter) -> [FoodSection] in
            if filters.contains(5) {
                
                if textFilter.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                    return [FoodSection(model: NSLocalizedString("All", comment: "Food List section header"), items: foods
                        .sorted(byKeyPath: "added", ascending: false)
                        .toArray())]
                }
                
                let sortedFoods = foods
                    .filter(NSPredicate(format: "name CONTAINS[c] %@", textFilter))
                    .sorted(byKeyPath: "added", ascending: false)
                return [FoodSection(model: NSLocalizedString("All", comment: "Food List section header"), items: sortedFoods.toArray())]
            } else {
                
                var filteredText: String = ""
                filters.forEach { filter in
                    switch filter {
                    case 1: filteredText += NSLocalizedString("Soups & ", comment: "Food List section header")
                    case 2: filteredText += NSLocalizedString("Salads & ", comment: "Food List section header")
                    case 3: filteredText += NSLocalizedString("Main Dishes & ", comment: "Food List section header")
                    case 4: filteredText += NSLocalizedString("Desserts & ", comment: "Food List section header")
                    default: fatalError("Should not reach default case in the switch statement")
                    }
                }
                filteredText = String(filteredText.dropLast(3))
                
                if textFilter.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                    let filteredFoods = foods
                        .filter(NSPredicate(format: "realmFoodKind IN %@", filters))
                    
                    return [FoodSection(model: filteredText, items: filteredFoods
                        .sorted(byKeyPath: "added", ascending: false)
                        .toArray())]
                }
                
                let filteredFoods = foods
                    .filter(NSPredicate(format: "realmFoodKind IN %@", filters))
                    .filter(NSPredicate(format: "name CONTAINS[c] %@", textFilter))
                    .sorted(byKeyPath: "added", ascending: false)

                return [FoodSection(model: filteredText, items: filteredFoods.toArray())]
            }
        }
    }
    
    func onCreateFood() -> CocoaAction {
        return CocoaAction { _ in
            let mockSubject = PublishSubject<Void>()
            let addFoodViewModel = AddFoodViewModel(coordinator: self.sceneCoordinator, service: self.foodService, foodToEdit: nil)
            
            self.sceneCoordinator
                .transition(to: FoodScene.addFood(addFoodViewModel), type: .modal)
            
            mockSubject.onCompleted()
            
            return mockSubject.asObservable()
        }
    }
    
    lazy var foodSelected: Action<FoodItem, Void> = { me in
        return Action { foodItem in
            let mockSubject = PublishSubject<Void>()
            let foodDetailViewModel = FoodDetailViewModel(coordinator: me.sceneCoordinator, foodService: me.foodService, food: foodItem)
            
            me.sceneCoordinator
                .transition(to: FoodScene.foodDetail(foodDetailViewModel), type: .push)
            
            // Not an elegant solution, but the Maybe from transition did not complete for some reason...
            mockSubject.onCompleted()
            
            return mockSubject.asObservable()
        }
    }(self)
    
    func onDeleteAction() -> Action<FoodItem, Void> {
        Action { food in
            self.foodService.delete(food: food)
        }
    }
}
