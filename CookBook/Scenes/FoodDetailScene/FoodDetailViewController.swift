//
//  FoodDetailViewController.swift
//  CookBook
//
//  Created by Robert Szasz on 21/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Action
import Then

class FoodDetailViewController: UIViewController, BindableType, ConfigurableType, UIScrollViewDelegate {
    
    var viewModel: FoodDetailViewModel!
    private var bag = DisposeBag()
    private let stateSubject = PublishSubject<Bool>()

    // MARK: - View properties
    
    lazy var photo = UIImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    
    lazy var iVC: BehaviorRelay<IngredientsView> = BehaviorRelay(value: IngredientsView(ingredients: [NSLocalizedString("Default", comment: "Default ingredient in ingredients view")]))

    let iv1 = UIImageView(image: UIImage()).then {
        $0.contentMode = .scaleAspectFit
        $0.tag = 1
    }
    let iv2 = UIImageView(image: UIImage()).then {
        $0.contentMode = .scaleAspectFit
        $0.tag = 2
    }
    let iv3 = UIImageView(image: UIImage()).then {
        $0.contentMode = .scaleAspectFit
        $0.tag = 3
    }
    let iv4 = UIImageView(image: UIImage()).then {
        $0.contentMode = .scaleAspectFit
        $0.tag = 4
    }
    let iv5 = UIImageView(image: UIImage()).then {
        $0.contentMode = .scaleAspectFit
        $0.tag = 5
    }
    lazy var ivArray = [iv1, iv2, iv3, iv4, iv5]
    
    let prepTimeLabel = UILabel().then {
        $0.font = .systemFont(ofSize: 18, weight: .semibold)
    }
    
    lazy var difView: BehaviorRelay<DifficultyView> = BehaviorRelay(value: DifficultyView(difficulty: DifficultyLevel.Middle))
    
    var recipeButtonView = UIButton(type: .custom).then {
        $0.setTitle(NSLocalizedString("Recipe", comment: "Food Detail View recipe button"), for: .normal)
        $0.titleLabel?.font = .systemFont(ofSize: 16, weight: .semibold)
        $0.isSelected = true
        $0.setTitleColor(.white, for: .selected)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
    }
    
    var overviewButtonView = UIButton(type: .custom).then {
        $0.setTitle(NSLocalizedString("Overview", comment: "Food Detail View overview button"), for: .normal)
        $0.titleLabel?.font = .systemFont(ofSize: 16, weight: .semibold)
        $0.isSelected = false
        $0.setTitleColor(.white, for: .selected)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
    }
    
    let btnSelectedRect = UIView().then {
        $0.layer.cornerRadius = 5
        $0.layer.backgroundColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.masksToBounds = true
    }
    
    let scrollView = UIScrollView().then {
        $0.backgroundColor = .white
        $0.isPagingEnabled = true
        $0.showsHorizontalScrollIndicator = false
    }
    
    let recipeTextView = UITextView().then {
        $0.font = .systemFont(ofSize: 18, weight: .medium)
        $0.textColor = .darkGray
        $0.isEditable = false
    }
    
    let overviewTextView = UITextView().then {
        $0.font = .systemFont(ofSize: 18, weight: .medium)
        $0.textColor = .darkGray
        $0.isEditable = false
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        iVC.asObservable()
            .subscribe { [weak self] (_) in
                self?.viewWillLayoutSubviews()
            }
            .disposed(by: bag)
        
        difView.asObservable()
            .subscribe { [weak self] (_) in
                self?.viewWillLayoutSubviews()
            }
            .disposed(by: bag)
        
        scrollView.rx.setDelegate(self)
            .disposed(by: bag)
        
        scrollView.rx.didEndDecelerating
            .map { [weak self] _ -> Bool in
                guard let self = self else { return false }
                let width = self.scrollView.bounds.size.width
                let page = Int((self.scrollView.contentOffset.x + width / 2) / width)
                return page == 0
            }
            .bind(to: stateSubject)
            .disposed(by: bag)
        
        scrollView.rx.didScroll
            .map { [weak self] _ -> CGFloat in
                guard let self = self else { return 0 }
                
                let percentage = self.scrollView.contentOffset.x / self.scrollView.frame.width
                return percentage >= 1 ? CGFloat(1) : percentage
            }.subscribe(onNext: { [weak self] percentage in
                guard let self = self else { return }
                let recipeBtnWidth = (self.view.frame.width - self.iVC.value.frame.width - 30 - 20) / 2

                self.btnSelectedRect.transform = CGAffineTransform(translationX: (recipeBtnWidth + 10) * percentage, y: 0)
            })
            .disposed(by: bag)
        
        stateSubject.asObservable()
            .bind(to: recipeButtonView.rx.isSelected)
            .disposed(by: bag)
        
        stateSubject.asObservable()
            .map { !$0 }
            .bind(to: overviewButtonView.rx.isSelected)
            .disposed(by: bag)
        
        stateSubject.asObservable()
            .subscribe(onNext: { [weak self] isFirstPage in
                guard let self = self else { return }
                
                let recipeBtnWidth = (self.view.frame.width - self.iVC.value.frame.width - 30 - 20) / 2

                self.view.isUserInteractionEnabled = false
                UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut], animations: {
                    self.btnSelectedRect.transform = isFirstPage ? .identity : CGAffineTransform(translationX: recipeBtnWidth + 10, y: 0)
                    self.scrollView.contentOffset = CGPoint(x: isFirstPage ? 0 : self.scrollView.frame.width, y: 0)
                }) { finished in
                    self.view.isUserInteractionEnabled = finished
                }
            })
            .disposed(by: bag)
        
        recipeButtonView.rx.action = CocoaAction { [weak self] _ in
            guard let self = self else { return Observable<Void>.empty() }

            self.stateSubject.onNext(true)
            return .empty()
        }
        
        overviewButtonView.rx.action = CocoaAction { [weak self] _ in
            guard let self = self else { return Observable<Void>.empty() }

            self.stateSubject.onNext(false)
            return .empty()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        setUpView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let recipeBtnWidth = (view.frame.width - iVC.value.frame.width - 30 - 20) / 2

        btnSelectedRect.frame = CGRect(x: 30 + iVC.value.frame.width + 5, y: view.safeAreaInsets.top + photo.frame.height + 5, width: recipeBtnWidth - 10, height: 30)
        
        recipeButtonView.frame = CGRect(x: iVC.value.frame.maxX + 5, y: photo.frame.maxY + 5, width: recipeBtnWidth, height: 30)
        
        overviewButtonView.frame = CGRect(x: recipeButtonView.frame.maxX + 10, y: photo.frame.maxY + 5, width: recipeBtnWidth, height: 30)
        
        scrollView.frame = CGRect(x: iVC.value.frame.maxX + 5, y: recipeButtonView.frame.maxY + 5, width: view.frame.width - iVC.value.frame.width - 30 - 10, height: view.frame.maxY - view.safeAreaInsets.bottom - 5 - (recipeButtonView.frame.maxY + 5))
        scrollView.contentSize = CGSize(width: scrollView.frame.width * 2, height: scrollView.frame.height)
        modifyScrollView()
    }
    
    // MARK: - Layout
    
    private func setUpView() {
        addSubviews()
        addConstraints()
    }
    
    private func addSubviews() {
        view.subviews.forEach({ $0.removeFromSuperview() })
        view.addSubview(photo)
        view.addSubview(iVC.value)
        ivArray.forEach { imageView in
            view.addSubview(imageView)
        }
        view.addSubview(prepTimeLabel)
        view.addSubview(difView.value)
        view.addSubview(btnSelectedRect)
        view.addSubview(recipeButtonView)
        view.addSubview(overviewButtonView)
        view.addSubview(scrollView)
    }
    
    private func addConstraints() {
        photo.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: view.frame.height / 3)

        iVC.value.anchor(top: photo.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: nil, paddingTop: -50, paddingLeft: 30, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
                
        iv1.anchor(top: iVC.value.bottomAnchor, left: iVC.value.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        
        iv2.anchor(top: iv1.topAnchor, left: iv1.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        
        iv3.anchor(top: iv2.topAnchor, left: iv2.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        
        iv4.anchor(top: iv1.bottomAnchor, left: iv1.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        
        iv5.anchor(top: iv4.topAnchor, left: iv4.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        
        prepTimeLabel.anchor(top: iv4.bottomAnchor, left: iv4.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        difView.value.anchor(top: prepTimeLabel.bottomAnchor, left: prepTimeLabel.leftAnchor, bottom: nil, right: iVC.value.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: -30, width: 0, height: 20)
    }
    
    // MARK: - Bind ViewModel
    
    func bindViewModel() {
        
        viewModel.photo
            .flatMap { [unowned self] photoString -> Observable<UIImage> in
                return Observable.create { observer in
                    if photoString == "" { observer.onNext(getDefaultImageForFood(self.viewModel.foodKind.value.rawValue)) }
                    else {
                        let filename = URL(string: photoString)!.lastPathComponent
                        observer.onNext(imageFromFileName(filename))
                    }
                    return Disposables.create()
                }
            }
            .bind(to: photo.rx.image)
            .disposed(by: bag)
        
        
        viewModel.name
            .bind(to: navigationItem.rx.title)
            .disposed(by: bag)
        
        viewModel.prepTime
            .bind(to: prepTimeLabel.rx.text)
            .disposed(by: bag)
        
        viewModel.ingredients
            .map { IngredientsView (ingredients: $0) }
            .bind(to: iVC)
            .disposed(by: bag)
        
        viewModel.difficulty
            .map { DifficultyView(difficulty: $0) }
            .bind(to: difView)
            .disposed(by: bag)

        viewModel.foodRestriction
            .do(onNext: { [weak self] _ in
                for i in 1...5 {
                    let iv = self?.view.viewWithTag(i) as! UIImageView
                    iv.image = UIImage()
                }
            })
            .subscribe(onNext: { [weak self] restrictions in
                for (index, rest) in restrictions.enumerated() {
                    let iv = self?.view.viewWithTag(index + 1) as! UIImageView
                    iv.image = UIImage(named: rest)
                }
            })
            .disposed(by: bag)
        
        viewModel.recipe
            .bind(to: recipeTextView.rx.text)
            .disposed(by: bag)
        
        viewModel.overview
            .bind(to: overviewTextView.rx.text)
            .disposed(by: bag)
    }
    
    // MARK: - Configurable Type

    func configureSelfAfterPushedOnNavStack() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: "Nav bar back button"), style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: nil, action: nil)

        navigationItem.leftBarButtonItem!.rx.action = CocoaAction { _ in
            return self.viewModel.sceneCoordinator.pop()
                .asObservable()
                .map { _ in }
        }
        
        navigationItem.rightBarButtonItem!.rx.tap
            .filter{ self.overviewButtonView.isSelected }
            .subscribe(onNext: { self.stateSubject.onNext(true) })
            .disposed(by: bag)
        
        navigationItem.rightBarButtonItem!.rx.action = viewModel.onEditFood()
                
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    // MARK: - Helper methods
    
    private func modifyScrollView() {

        scrollView.addSubview(recipeTextView)
        recipeTextView.anchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: scrollView.frame.width, height: scrollView.frame.height)

        scrollView.addSubview(overviewTextView)
        overviewTextView.anchor(top: scrollView.topAnchor, left: recipeTextView.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: scrollView.frame.width, height: scrollView.frame.height)
    }
}
