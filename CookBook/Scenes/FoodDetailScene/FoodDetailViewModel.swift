//
//  FoodDetailViewModel.swift
//  CookBook
//
//  Created by Robert Szasz on 21/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxRealm
import Action

struct FoodDetailViewModel {
    let sceneCoordinator: SceneCoordinatorType
    let foodService: FoodServiceType
    
    private let bag = DisposeBag()
    
    lazy var observedObject = Observable.from(object: foodItem, emitInitialValue: true, properties: ["name", "overview", "recipe", "prepTime", "pictureURL", "isVegetarian", "isVegan", "isLactosefree", "isGlutenfree", "isSugarfree", "ingredients", "pictureURL", "enumFoodKind", "enumDifficultyLevel"])
        .share(replay: 1)
    
    init(coordinator: SceneCoordinatorType, foodService: FoodServiceType, food: FoodItem) {
        self.sceneCoordinator = coordinator
        self.foodService = foodService
        self.foodItem = food
    }
    
    // MARK: - Input
    private let foodItem: FoodItem
    
    
    // MARK: - Output
    lazy var photo = observedObject.asObservable()
        .map { $0.pictureURL }
        .distinctUntilChanged()
        
    lazy var name = observedObject
        .asObservable()
        .map {$0.name}
        .distinctUntilChanged()

    lazy var overview = observedObject
        .asObservable()
        .map {$0.overview}
        .distinctUntilChanged()
    
    lazy var recipe = observedObject
        .asObservable()
        .map {$0.recipe}
        .distinctUntilChanged()
    
    lazy var prepTime = observedObject
        .asObservable()
        .map {$0.prepTime}
        .distinctUntilChanged()
    
    private lazy var isVegan = observedObject.map { $0.isVegan }
    private lazy var isVegetarian = observedObject.map { $0.isVegetarian }
    private lazy var isLactosefree = observedObject.map { $0.isLactosefree }
    private lazy var isGlutenfree = observedObject.map { $0.isGlutenfree }
    private lazy var isSugarfree = observedObject.map { $0.isSugarfree }
    
    lazy var foodRestriction = Observable.combineLatest(isVegan, isVegetarian, isLactosefree, isGlutenfree, isSugarfree)
        .debounce(RxTimeInterval.milliseconds(100), scheduler: MainScheduler.instance)
        .map { (isVegan, isVegetarian, isLactosefree, isGlutenfree, isSugarfree) -> [String] in
            let restrictionArr = [isVegan, isVegetarian, isLactosefree, isGlutenfree, isSugarfree]
            .enumerated()
            .map { (index, element) -> String? in
                if element {
                    switch index {
                    case 0: return "isVegan"
                    case 1: return "isVegetarian"
                    case 2: return "isLactosefree"
                    case 3: return "isGlutenfree"
                    case 4: return "isSugarfree"
                    default: fatalError("Index out of range!")
                    }
                } else {
                    return nil
                }
            }
            .compactMap { $0 }

            return restrictionArr
        }
    
    lazy var ingredients = observedObject
        .asObservable()
        .map { $0.ingredients.toArray() }
        .distinctUntilChanged()
    
    lazy var foodKindObserver = observedObject
        .asObservable()
        .map {$0.enumFoodKind}
        .distinctUntilChanged()
        .bind(to: foodKind)
        .disposed(by: bag)
    
    lazy var foodKind = BehaviorRelay<FoodKind>(value: foodItem.enumFoodKind)
    
    lazy var difficulty = observedObject
        .asObservable()
        .map {$0.enumDifficultyLevel}
        .distinctUntilChanged()
    
    func onEditFood() -> CocoaAction {
        return CocoaAction { _ in
            let mockSubject = PublishSubject<Void>()
            let addFoodViewModel = AddFoodViewModel(coordinator: self.sceneCoordinator, service: self.foodService, foodToEdit: self.foodItem)
            
            self.sceneCoordinator
                .transition(to: FoodScene.addFood(addFoodViewModel), type: .modal)
            
            mockSubject.onCompleted()
            
            return mockSubject.asObservable()
        }
    }

}
