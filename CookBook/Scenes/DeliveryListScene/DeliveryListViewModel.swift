//
//  DeliveryListViewModel.swift
//  CookBook
//
//  Created by Robert Szasz on 18/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay
import RealmSwift
import RxDataSources
import Action

typealias DeliverySection = AnimatableSectionModel<String, Delivery>

struct DeliveryListViewModel {
    let sceneCoordinator: SceneCoordinatorType
    let deliveryService: DeliveryServiceType
    
    private let bag = DisposeBag()
    
    init(coordinator: SceneCoordinatorType, deliveryService: DeliveryServiceType) {
        self.sceneCoordinator = coordinator
        self.deliveryService = deliveryService
    }
    
    // MARK: - Inputs
    
    let personFilter = BehaviorRelay<[String]>(value: [])
    
    let calendarFilter = BehaviorRelay<DateInterval?>(value: nil)
    
    // MARK: - Outputs
    
    var sectionedItems: Observable<DeliverySection> {
        return Observable.combineLatest(self.deliveryService.deliveries(), personFilter.asObservable(), calendarFilter.asObservable()) { (deliveries, persons, dateIntervals) -> DeliverySection in
            if persons == [] && dateIntervals == nil { return DeliverySection(model: NSLocalizedString("Deliveries", comment: "Deliveries scene"), items: deliveries.sorted(byKeyPath: "date", ascending: false).toArray()) }
            else {
                var filteredText: String = persons == [] ? NSLocalizedString("Deliveries", comment: "Deliveries scene") : NSLocalizedString("Deliveries to: ", comment: "Deliveries scene with to")
                for name in persons { filteredText += name + ", " }
                if persons != [] { filteredText = String(filteredText.dropLast(2)) }
                
                guard let dateIntervals = dateIntervals else {
                    return DeliverySection(model: filteredText, items: deliveries
                        .filter({ (delivery) -> Bool in
                            persons.contains(where: {$0.caseInsensitiveCompare(delivery.toPerson) == .orderedSame})
                        })
                        .sorted(by: { $0.date > $1.date }))
                }
                
                let filteredDeliveries = deliveries
                    .filter("date <= %@ AND date >= %@", dateIntervals.end, dateIntervals.start)
                    .filter({ (delivery) -> Bool in
                        if !persons.isEmpty { return persons.contains(where: {$0.caseInsensitiveCompare(delivery.toPerson) == .orderedSame}) }
                        return true
                    })
                    .sorted(by: { $0.date > $1.date })
                
                return DeliverySection(model: filteredText, items: filteredDeliveries)
            }
        }
    }
    
    func onCreateDelivery() -> CocoaAction {
        return CocoaAction { _ in
            let mockSubject = PublishSubject<Void>()
            let addDeliveryViewModel = AddDeliveryViewModel(coordinator: self.sceneCoordinator, deliveryService: self.deliveryService)
            
            self.sceneCoordinator
                .transition(to: DeliveryScene.addDelivery(addDeliveryViewModel), type: .modal)
            
            mockSubject.onCompleted()
            
            return mockSubject.asObservable()
        }
    }
    
}
