//
//  FoodService.swift
//  CookBook
//
//  Created by Robert Szasz on 16/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxRealm


struct FoodService: FoodServiceType {
    private var realmProviderType: RealmProvider.Type
    
    init(_ realmProvider: RealmProvider.Type = FoodRealmProvider.self) {
        self.realmProviderType = realmProvider
    }
    
    private func withFoodRealm<T>(_ operation: String, action: (Realm) throws -> T) -> T? {
      do {
        let realm = realmProviderType.itemsOnDisk.realm
        return try action(realm)
      } catch let err {
        print("Failed \(operation) realm with error: \(err)")
        return nil
      }
    }
    
    /**
        This function creates, saves to Realm and returns an observable food object with the given properties.
     
        The `createFood` function takes a `FoodPropertyList` argument, which the food object is constructed from. The object gets automatically saved to the food `Realm` instance. At creation, the newly created object gets assigned an automatically increment unique integer id (to be uniquely identifiable). The `id`s of the objects are positive integer values, negative integer `id`s are used by testing objects.
     
        - Parameter properties: A `FoodPropertyList`, which is a **typealias** for a `tuple`
        - Returns: A `Observable<FoodItem>` if the action succeeds or a `FoodServiceError.creationFailed` otherwise.
        - Warning: This function writes the generated object to a Realm specified by the `RealmProvider` of the `struct`
        - Note: This function has `@discardableResult`
     */
    @discardableResult
    func createFood(properties: FullFoodPropertyList) -> Observable<FoodItem> {
        let result = withFoodRealm("creating") { realm -> Observable<FoodItem> in
            let food = FoodItem(properties: properties)
            try realm.write {
                if properties.uid == nil {food.uid = (realm.objects(FoodItem.self).max(ofProperty: "uid") ?? 0) + 1 }
                realm.add(food)
            }
            return .just(food)
        }
        return result ?? .error(FoodServiceError.creationFailed)
    }
    
    @discardableResult
    func createFood(properties: FoodPropertyList) -> Observable<FoodItem> {
        let ffpl = fullFoodPropertyList(from: properties)
        
        return createFood(properties: ffpl)
    }
    
    /**
        This function deletes the specified object from the Realm and returns `Void` observable.
     
        The `delete` function takes a `FoodItem` argument, which defines the to be deleted object. The object gets deleted from the `Realm`.
     
        - Parameter properties: A `FoodItem` object
        - Returns: A `Observable<Void>` if the action succeeds or a `FoodServiceError.deletionFailed(food)` with the associated object otherwise.
        - Warning: This function writes the generated object to a Realm specified by the `RealmProvider` of the `struct`
        - Note: This function has `@discardableResult`
        - Todo: Change return type to Completable (or Maybe), which fits the purpose more
     */
    @discardableResult
    func delete(food: FoodItem) -> Observable<Void> {
        let result = withFoodRealm("deleting") { realm -> Observable<Void> in
            try realm.write {
                realm.delete(food)
            }
            return .empty()
        }
        return result ?? .error(FoodServiceError.deletionFailed(food))
    }
    
    /**
        This function updates a food object with the given properties in the `Realm`.
     
        The `update` function takes a `FoodItem` and a `FoodPropertyList` argument. The object gets updated based on the given property list.
     
        - Parameters:
            - food: The object to be updated
            - properties: A `FoodPropertyList`, which is a **typealias** for a `tuple`
        - Returns: A `Observable<FoodItem>` if the action succeeds or a `FoodServiceError.creationFailed` otherwise.
        - Warning: This function writes the generated object to a Realm specified by the `RealmProvider` of the `struct`.
        - Important: The `properties` argument muss contain the objects desired state after the update, not just the changed properties.
        - Note: This function has `@discardableResult`
     */
    @discardableResult
    func update(food: FoodItem, properties: FullFoodPropertyList) -> Observable<FoodItem> {
        var newFPL = properties
        newFPL.uid = food.uid
        let newFood = FoodItem(properties: newFPL)
        
        let result = withFoodRealm("updating") { realm -> Observable<FoodItem> in
            try realm.write {
                realm.add(newFood, update: .modified)
            }
            return .just(newFood)
        }
        return result ?? .error(FoodServiceError.updateFailed(newFood))
    }
    
    @discardableResult
    func update(food: FoodItem, properties: FoodPropertyList) -> Observable<FoodItem> {
        let ffpl = fullFoodPropertyList(from: properties)
        
        return update(food: food, properties: ffpl)
    }
    
    /**
        This function fetches the food objects from the `Realm`.
     
        The `foods` doesn not take an argument and returns the whole content of the food `Realm`.
     
        - Returns: A `Observable<Results<FoodItem>>` - a observable of the Realm `Results` wrapper with the foods - if the action succeeds or a `empty` Observable otherwise.
        - Warning: This function fetches the objects from a Realm specified by the `RealmProvider` of the `struct`
     */
    func foods() -> Observable<Results<FoodItem>> {
        let result = withFoodRealm("getting foods") { realm -> Observable<Results<FoodItem>> in
            let foods = realm.objects(FoodItem.self)
            return .collection(from: foods)
        }
        return result ?? .empty()
    }
    
    func foodsOfType(_ type: FoodKind) -> Observable<Results<FoodItem>> {
        let foodKindNumber = getNumberForFoodKind(type)
        return foods()
            .asObservable()
            .flatMap({ foods -> Observable<Results<FoodItem>> in
                Observable.just(foods.filter(NSPredicate(format: "realmFoodKind == \(foodKindNumber)")).sorted(byKeyPath: "added", ascending: false))
            })
    }
}


struct FoodRealmProvider: RealmProvider {
    
    private static let libraryDir = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first ?? ""
    let configuration: Realm.Configuration
    var realm: Realm {
        return try! Realm(configuration: configuration)
    }
    
    private init(config: Realm.Configuration) {
        configuration = config
    }
    
    // MARK: - FOOD Realm on the disk
    private static func foodItemsOnDiskConfig() -> Realm.Configuration {
        let libraryUrl = URL(fileURLWithPath: libraryDir)
        let libraryUrlWithRealm = libraryUrl.appendingPathComponent("foodItems.realm")
        
        return Realm.Configuration(fileURL: libraryUrlWithRealm, schemaVersion: 0, deleteRealmIfMigrationNeeded: true, objectTypes: [FoodItem.self])
    }
    
    static var itemsOnDisk: RealmProvider = {
        return FoodRealmProvider(config: FoodRealmProvider.foodItemsOnDiskConfig())
    }()
}

protocol RealmProvider {
    var realm: Realm { get }
    static var itemsOnDisk: RealmProvider { get }
}
