//
//  IngredientsView.swift
//  CookBook
//
//  Created by Robert Szasz on 26/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Then
import Action

class IngredientsListView: UIView {

    let ingredientsArray: BehaviorRelay<[String]>
    private let bag = DisposeBag()
    
    var ingredientsViews = [IngredientItem]()
    var ingredientsTextfieldTopanchor: NSLayoutYAxisAnchor?
    
    lazy var plusAction: CocoaAction = CocoaAction { [weak self] _ in
        guard let self = self else { return .just(()) }
        guard self.textfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "", let text = self.textfield.text else { return .just(()) }
        
        var oldArray = self.ingredientsArray.value
        
        self.ingredientsArray.accept(oldArray + [text])
  
        return .just(())
    }
    
    lazy var deleteAction: Action<Int, Void> = Action { [weak self] index in
        guard let self = self else { return .just(()) }
        guard self.ingredientsArray.value.count >= index else { return .just(()) }
          
        var oldArray = self.ingredientsArray.value
        oldArray.remove(at: index)
        self.ingredientsArray.accept(oldArray)
    
        return .just(())
      }
    
    // MARK: - View properties
    
    let ingredientsTitle = UILabel().then {
        $0.text = NSLocalizedString("Ingredients", comment: "Ingredients title in ingredients view")
        $0.textColor = .darkGray
        $0.font = .systemFont(ofSize: 14, weight: .heavy)
    }
    
    let textfield: UITextField = UITextField().then {
        $0.placeholder = NSLocalizedString("Ingredient", comment: "Add single ingredient in ingredients view")
        $0.borderStyle = .roundedRect
    }
    
    var plusButton = UIButton(type: .custom).then {
        $0.setTitle("+", for: .normal)
        $0.titleLabel?.font = .systemFont(ofSize: 30, weight: .heavy)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1), for: .normal)
        $0.setTitleColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1).withAlphaComponent(0.3), for: .highlighted)
        $0.layer.borderColor = #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1)
        $0.layer.borderWidth = 5
        $0.layer.cornerRadius = 20
        $0.layer.masksToBounds = true
    }
 
    // MARK: - Init
    
    convenience init(ingredientsArray: [String] = []) {
        self.init(frame: .zero, array: ingredientsArray)
        translatesAutoresizingMaskIntoConstraints = false
        setUpButtons()
        setUpView()
        
        self.ingredientsArray.asObservable()
            .do(onNext: { [weak self] newArr in
                if newArr.count == 0 { self?.ingredientsTextfieldTopanchor = nil }
            })
            .subscribe(onNext: { [weak self] newArr in
                self?.setUpView()
                self?.textfield.text = ""
            })
            .disposed(by: bag)
    }
    
    init(frame: CGRect, array: [String]) {
        self.ingredientsArray = BehaviorRelay(value: array)
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Set up view
    
    private func setUpView() {
        subviews.forEach { $0.removeFromSuperview() }
        
        addSubview(ingredientsTitle)
        ingredientsTitle.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addIngredients(withArray: ingredientsArray.value)
        
        addSubview(textfield)
        textfield.anchor(top: ingredientsTextfieldTopanchor == nil ? ingredientsTitle.bottomAnchor : ingredientsTextfieldTopanchor!, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addSubview(plusButton)
        plusButton.anchor(top: textfield.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 40, height: 40)
        plusButton.centerXAnchor.constraint(equalTo: textfield.centerXAnchor).isActive = true
        
    }

    private func setUpButtons() {
        plusButton.rx.action = plusAction
    }
    
    // MARK: - Helper methods
    
    private func addIngredients(withArray array: [String]) {
        guard array.count != 0 else { return }
        ingredientsViews = []
        
        let startingTag = 10
        let transformedArray = array.enumerated().map { (index, name) in
            IngredientItem(name: name).then { $0.xButton.rx.bind(to: deleteAction, input: index) }
        }
        
        for (index, ingredient) in transformedArray.enumerated() {
            ingredient.tag = startingTag + index
            ingredientsViews.append(ingredient)
        }
        
        for (index, ingredientView) in ingredientsViews.enumerated() {
            addSubview(ingredientView)
            ingredientView.anchor(top: index == 0 ? ingredientsTitle.bottomAnchor : viewWithTag(startingTag + index - 1)!.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
            ingredientsTextfieldTopanchor = ingredientView.bottomAnchor
        }
    }
}
