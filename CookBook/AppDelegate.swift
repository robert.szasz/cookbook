//
//  AppDelegate.swift
//  CookBook
//
//  Created by Robert Szasz on 16/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        clearMemoryFromImages()
        if #available(iOS 13, *) { return true }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        RootTabBarViewController.initRootTabBar(window: window!)
        
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func clearMemoryFromImages() {
        let obs = Observable<[String]>.create { observer in
            let ages = Array(FoodRealmProvider.itemsOnDisk.realm.objects(FoodItem.self)).map{ $0.pictureURL }.filter { $0 != "" }
            let uniqueULRs = Set(ages)
            
            let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

            do {
                let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)

                let imageFiles = directoryContents.filter{ $0.pathExtension == "jpeg" }
                let imageNames = Set(imageFiles.map { $0.lastPathComponent })
                
                let toBeDeletedImages = imageNames.subtracting(uniqueULRs)
                for filename in toBeDeletedImages {
                    let fileUrl = documentsUrl.appendingPathComponent(filename)
                    try FileManager.default.removeItem(at: fileUrl)
                }
                observer.onCompleted()

            } catch {
                observer.onError(error)
                print(error)
            }
            
            return Disposables.create()
        }
        
        obs.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .default))
            .subscribe(onCompleted: {
                print("COMPLETED")
            }) {
                print("DISPOSED")
            }
    }

}

